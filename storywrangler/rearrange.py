
import logging
import shutil
import tarfile
import time
from pathlib import Path

from storywrangler import cli


def move_ngrams(ipath, opath):

    lang, fname = ipath.name.split('_')
    if lang != '':
        out = Path(f'{opath}/{lang}/{ipath.parent.stem}')
        out.mkdir(parents=True, exist_ok=True)
        opath = Path(f'{out}/{fname}')

        if not opath.exists():
            shutil.move(ipath, opath)


def decompress(ipath, opath):

    logging.info(f"Decompressing {ipath}...")
    with tarfile.open(ipath) as tar:
        try:
            tar.extractall(f'{opath}')
        except Exception as e:
            logging.warning(f'An error has occurred during the decompressing process: {opath}')
            logging.warning(e)


def process_day(ipath, opath):

    tmp = opath / 'tmp'
    decompress(ipath, tmp)
    ipath = opath / 'tmp' / ipath.name.split(".")[0]

    logging.info(f"Copying:")
    for d in ipath.rglob('*grams'):
        for f in d.iterdir():
            logging.info(f)
            move_ngrams(f, opath)

    shutil.rmtree(ipath, ignore_errors=True)


def parse_args(args):

    parser = cli.parser()
    parser.add_argument(
        'infile',
        help='absolute Path to a daily compressed tarball'
    )

    parser.add_argument(
        '-o', '--outdir',
        help='absolute Path to save ngrams'
    )
    return parser.parse_args(args)


def main(args=None):

    timeit = time.time()
    args = parse_args(args)

    data = Path(args.infile) if args.infile.endswith('.tar.gz') else Path(f"{args.infile}.tar.gz")
    out = Path(args.outdir)

    Path(f'{out}/logs/{data.parent.stem}').mkdir(parents=True, exist_ok=True)
    logfile = f'{out}/logs/{data.parent.stem}/{data.name.split(".")[0]}.log'
    logging.basicConfig(
        format='%(asctime)s - %(message)s', level=logging.INFO, filename=logfile
    )
    logging.info('Initialized logging...')
    logging.info(args)

    process_day(data, out)

    logging.info(f'Total time elapsed: {time.time() - timeit:.2f} secs.')


if __name__ == "__main__":
    main()
