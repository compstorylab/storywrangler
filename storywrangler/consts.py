
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors


def cmap(n, base='tab20c'):
    base = plt.cm.get_cmap(base)
    cmaplist = base(np.linspace(0, 1, n))
    return mcolors.LinearSegmentedColormap.from_list(None, cmaplist, n)


tweet_types = {
    "tweet": 0,
    "retweet": 1,
    "comment": 2
}

top10 = [
    'en', 'ja', 'es', 'pt', 'th',
    'ar', 'ko', 'fr', 'id', 'tr',
]

reachedtop10 = [
    'en', 'ja', 'und', 'es', 'pt', 'ar', 'th',
    'ko', 'fr', 'id', 'tr', 'de', 'nl', 'ru'
]

reachedtop15 = [
    "en", "ja", "und", "es",
    "pt", "ar", "th", "ko",
    "fr", "tr", "id", "de",
    "it", "zh", "ru", "nl",
]

langsOfInterest = [
    "en", "ja", "es", "und", "pt",
    "th", "ar", "ko", "fr", "id",
    "tr", "de", "zh", "it", "ru",
    "tl", "hi", "fa", "ur", "pl",
    "ca", "nl", "ta", "el", "sv",
    "sr", "fi", "ceb", "uk", "eo",
]

defaultLangs = [
    "en",
    "ja",
    "es",
    "pt",
    "ar",
    "th",
    "ko",
    "fr",
    "id",
    "tr",
    "und",
    'other',
    'unknown',
]

colors = {
    'en': plt.cm.tab20(0),
    'ja': plt.cm.tab20(1),
    'es': plt.cm.tab20(2),
    'und': plt.cm.tab20(10),
    'pt': plt.cm.tab20(3),
    'ar': plt.cm.tab20(4),
    'th': plt.cm.tab20(5),
    'ko': plt.cm.tab20(6),
    'fr': plt.cm.tab20(7),
    'tr': plt.cm.tab20(8),
    'id': plt.cm.tab20(9),
    'de': plt.cm.tab20(11),
    'it': plt.cm.tab20(12),
    'zh': plt.cm.tab20(13),
    'ru': plt.cm.tab20(16),
    'sv': plt.cm.tab20(17),
    'nl': plt.cm.tab20(18),
    'hi': plt.cm.tab20(19),
    'tl': plt.cm.tab20(20),
    'other': (0, 0, 0, 1),
    'unknown': (1, 1, 1, 1)
}

other_colors = {
    'retweets': '#165c0f',#'seagreen',
    'tweets': '#7a5195', #dimgrey',
    'emojis': '#ff491c',
}

corrupted_days = [
    pd.to_datetime('2009-05-09'),
    pd.to_datetime('2009-05-10'),
    pd.to_datetime('2009-05-14'),
    pd.to_datetime('2009-05-15'),
    pd.to_datetime('2009-05-16'),
    pd.to_datetime('2009-05-17'),
    pd.to_datetime('2009-05-18'),
    pd.to_datetime('2009-05-19'),
    pd.to_datetime('2009-05-20'),
    pd.to_datetime('2009-05-21'),
    pd.to_datetime('2009-05-22'),
    pd.to_datetime('2009-05-23'),
    pd.to_datetime('2009-05-24'),
]

ngrams = [
    '1grams',
    '2grams',
    '3grams',
]
colormap = cmap(len(ngrams), base='tab20b')
ngrams_colors = {t: colormap(i) for i, t in enumerate(ngrams)}


types_colors = {
    'AT': 'dimgrey',
    'RT': 'darkorange',
    'OT': 'steelblue',
}
