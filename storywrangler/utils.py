
import contextlib
import gzip
import multiprocessing as mp
import os
import re
import sys
import tarfile
import shutil
import time
import numpy as np
import pandas as pd
import ujson
import traceback
from itertools import chain
from pathlib import Path
from datetime import datetime
import fasttext

from storywrangler import consts
from storywrangler import counter
from storywrangler import fileio
from storywrangler import regexr
from storywrangler import tweet_utils

import logging
print = logging.info


def get_obj_size(obj):
    """Compute memory footprint of an object recursively"""
    dtypes = {
        tuple: iter,
        list: iter,
        dict: lambda d: chain.from_iterable(d.items()),
    }
    seen = set()

    def mem(obj):
        i = id(obj)
        if i in seen:
            return 0

        seen.add(i)
        total = sys.getsizeof(obj, sys.getsizeof(0))
        for tp, h in dtypes.items():
            if isinstance(obj, tp):
                total += sum(map(mem, h(obj)))
                break
        return total

    # convert to (GB)
    return round(mem(obj) / float(1 << 30), 2)


def df_mem_usage(d):
    """Get memory usage for a given dataframe

    Args:
      d: a pandas dataframe

    Returns:
        memory used
    """
    return round(d.memory_usage(deep=True).sum() / float(1 << 30), 2)


def batches(ll, n):
    """Yield successive n-sized batches from ll

    Args:
      ll: list of files
      n: number of batches

    Returns:
      np.array of [n-batches]

    """
    for i in range(0, len(ll), n):
        yield ll[i:i + n]


def multiprocess(func, jobs, cores):
    """Multiprocess a generic function

    Args:
      func: a python function
      jobs: a list of jobs for function `func`
      cores: number of cores to use

    Returns:
      an array of outputs for every function call

    """
    if cores == 1:
        logs = []
        for j in jobs:
            logs.append(func(j))

    elif cores == -1:
        with mp.Pool(mp.cpu_count()) as p:
            logs = list(p.map(func, jobs))

    elif cores > 1:
        with mp.Pool(cores) as p:
            logs = list(p.map(func, jobs))

    else:
        print('Error: jobs must be a positive integer')
        return False

    return logs


def parse_activity(json_obj):
    """Parse out some metadata from a tweet object

    Args:
      json_obj: a tweet JSON object

    Returns:
      a list of dicts [tweet-ID, user-ID, twitter-label, tweet-text, RT-flag]

    """
    new_txt, ref_txt = tweet_utils.get_text(json_obj, enrich_rt=True)

    # if we get two objects back (comment, retweeted-text)
    if new_txt is not None and ref_txt is not None:
        new_rt = consts.tweet_types['retweet'] \
            if tweet_utils.find_rt_text(new_txt) != '' else consts.tweet_types['comment']
        ref_rt = consts.tweet_types['retweet']
    else:
        new_rt = consts.tweet_types['retweet'] \
            if tweet_utils.find_rt_text(new_txt) != '' else consts.tweet_types['tweet']
        ref_rt = consts.tweet_types['retweet'] \
            if tweet_utils.find_rt_text(ref_txt) != '' else consts.tweet_types['tweet']

    new_twt_id, ref_twt_id = tweet_utils.get_tweet_id(json_obj)
    new_twt_id = int(new_twt_id.split(':')[-1]) if isinstance(new_twt_id, str) else new_twt_id
    ref_twt_id = int(ref_twt_id.split(':')[-1]) if isinstance(ref_twt_id, str) else ref_twt_id

    new_usr_id, ref_usr_id = tweet_utils.get_user_id(json_obj)
    new_usr_id = int(new_usr_id.split(':')[-1]) if isinstance(new_usr_id, str) else new_usr_id
    ref_usr_id = int(ref_usr_id.split(':')[-1]) if isinstance(ref_usr_id, str) else ref_usr_id

    new_lang, ref_lang = tweet_utils.get_lang(json_obj)

    return [
        dict(tid=new_twt_id, uid=new_usr_id, lang=new_lang, text=new_txt, rt=new_rt, length=len(new_txt))
        if new_txt is not None else None,
        dict(tid=ref_twt_id, uid=ref_usr_id, lang=ref_lang, text=ref_txt, rt=ref_rt, length=len(ref_txt))
        if ref_txt is not None else None,
    ]


def predict_lang(pred, threshold):
    """Parse out predicted language and confidence-score

    Args:
      pred: a list of predictions from FT model
      threshold: confidence score threshold for the language identifier

    Returns:
      a tuple (language, confidenceScore)

    """
    try:
        label, conf = pred
        if conf[0] > threshold:
            return re.sub('__label__', '', label[0]), round(conf[0], 2)
        else:
            return 'und', 1 - round(conf[0], 2)
    except Exception as e:
        return 'und', 1.00


def predict(text, model, threshold):
    """Predict language of the given text

    Args:
      text: an array of strings
      model: path to a fastText model
      threshold: confidence score threshold for the language identifier

    Returns:
      a tuple (language, confidenceScore)

    """
    if text.strip() != '':
        clf = fasttext.load_model(model)
        pred = clf.predict(text)
        return predict_lang(pred, threshold)
    else:
        return 'und', 1.00


def process_activity(
    act,
    clf,
    threshold,
    emoji_parser,
    ngrams_parser,
    scheme,
    target_langs,
    case_sensitive=False,
    anchor_regex=None
):
    """Parse out an activity object

    Args:
      act: dict: ['tid', 'uid', 'lang', 'text', 'rt']
      clf: path to a fastText model
      threshold: confidence score threshold for the language identifier
      emoji_parser: a compiled regular expression that matches emojis
      ngrams_parser: a compiled regular expression that matches ngrams
      scheme: degree of the ngrams
      target_langs: a list of languages to parse out
      case_sensitive: a toggle to get case sensitive ngrams (Default value = True)
      anchor_regex: a compiled regex to subsample tweets

    Returns:
        a tuple (ft_lang, NgramCounter(), PredictionDict()) (Default value = None)
    """
    pred, ngrams = None, None

    text = regexr.remove_whitespaces(act['text'])
    emoji_freezone = emoji_parser.sub(r'', regexr.filter_text(text))
    emoji_freezone = regexr.remove_whitespaces(emoji_freezone)
    ft_lang, ft_conf = predict_lang(clf.predict(emoji_freezone), threshold)

    if not case_sensitive:
        text = text.lower()

    if act['tid'] is not None:
        pred = dict(
            tid=act['tid'],
            uid=act['uid'],
            tw_lang=act['lang'],
            ft_lang=ft_lang,
            ft_conf=ft_conf,
            rt=act['rt'],
            length=act['length']
        )

    if ft_lang in target_langs:
        if (anchor_regex is not None) and (not any(re.findall(anchor_regex, text))):
            return ft_lang, pred, ngrams

        # get a Counter object for given tweet
        ngrams = regexr.ngrams(text, parser=ngrams_parser, n=scheme)

        if ngrams is not None:
            if act['rt'] == consts.tweet_types['retweet']:
                ngrams = counter.NgramCounter(
                    {
                        k: counter.Storyon(count=v, count_no_rt=0)
                        for k, v in ngrams.items()
                    }
                )
            else:
                ngrams = counter.NgramCounter(
                    {
                        k: counter.Storyon(count=v, count_no_rt=v)
                        for k, v in ngrams.items()
                     }
                )

    return ft_lang, pred, ngrams


def process_batch(
    path,
    clf,
    threshold,
    emoji_parser,
    ngrams_parser,
    language_codes,
    scheme,
    target_langs,
    case_sensitive,
    anchor_regex
):
    """Process a 15-minute chunk of tweets

    Args:
      path: path to a 15-minute chunk
      clf: path to a fastText model
      threshold: confidence score threshold for the language identifier
      emoji_parser: a compiled regular expression that matches emojis
      ngrams_parser: a compiled regular expression that matches ngrams
      language_codes: path to a dict of language codes
      scheme: degree of the ngrams
      target_langs: a list of languages to parse out
      case_sensitive: a toggle to get case sensitive ngrams
      anchor_regex: a compiled regex to subsample tweets

    Returns:
        total number of activities, number of corrupted activities, prediction (dict), ngrams (NgramCounter)
    """
    predictions = {}
    ngrams = counter.NgramCounter({})
    acts, corrupted = 0, 0

    with gzip.open(path, 'rb') as data:
        try:
            for line in data:
                acts += 1
                if line.isspace():
                    # filter out empty tweets
                    corrupted += 1
                    continue

                try:
                    activities = parse_activity(ujson.loads(line))
                except ValueError:
                    corrupted += 1
                    continue

                for activity in activities:
                    if activity is not None:
                        try:
                            ft_lang, pred, ngs = process_activity(
                                activity,
                                clf=clf,
                                threshold=threshold,
                                emoji_parser=emoji_parser,
                                ngrams_parser=ngrams_parser,
                                scheme=scheme,
                                target_langs=target_langs,
                                case_sensitive=case_sensitive,
                                anchor_regex=anchor_regex
                            )

                            if ngs is not None:
                                if ngrams.get(ft_lang) is not None:
                                    ngrams[ft_lang] += ngs
                                else:
                                    ngrams[ft_lang] = ngs

                            if pred is not None:
                                if language_codes.get(pred['tw_lang']) is None:
                                    language_codes[pred['tw_lang']] = len(language_codes)

                                if language_codes.get(pred['ft_lang']) is None:
                                    language_codes[pred['ft_lang']] = len(language_codes)

                                predictions.update({
                                    pred['tid']: [
                                        language_codes[pred['tw_lang']],
                                        language_codes[pred['ft_lang']],
                                        pred['ft_conf'],
                                        pred['uid'],
                                        pred['rt'],
                                        pred['length']
                                    ]
                                })

                        except Exception as e:
                            print(e)
                            traceback.print_exc()
                            corrupted += 1
                            continue

        except Exception:
            print('Warning: 15-minute batch is incomplete!')

    print(f'[{acts}] activities | [{corrupted}] corrupted activities')
    return acts, corrupted, predictions, ngrams


def agg_lang_stats(stats, pred_path, lang_path, language_codes):
    """Aggregate language statistics

    Args:
      pred_path: path to save predictions
      lang_path: path to save predictions
      language_codes: a dict of language ISO codes
      stats: a dict of running stats

    """
    df = pd.read_csv(
        pred_path,
        header=0,
        memory_map=True,
        low_memory=True,
        encoding='utf-8',
        sep=',',
        na_filter=False,
        names=['id', 'twitter_label', 'fastText_label', 'fastText_conf_score', 'user_id', 'rt', 'length']
    )
    df.set_index('id', inplace=True)
    rts = df.rt.astype(int).value_counts().to_dict()

    try:
        stats['num_tweets'] = rts[consts.tweet_types['tweet']]
    except KeyError:
        stats['num_tweets'] = 0

    try:
        stats['num_retweets'] = rts[consts.tweet_types['retweet']]
    except KeyError:
        stats['num_retweets'] = 0

    try:
        stats['num_comments'] = rts[consts.tweet_types['comment']]
    except KeyError:
        stats['num_comments'] = 0

    stats['num_activities'] = len(df)

    tw = df.twitter_label.value_counts().to_frame(name='count')
    tw['speakers'] = df.groupby(['user_id', 'twitter_label'], as_index=False) \
        .size().reset_index()['twitter_label'].value_counts()

    try:
        tw['tweets'] = df.groupby(['rt', 'twitter_label']).size().loc[consts.tweet_types['tweet']]
    except KeyError:
        tw['tweets'] = 0
    try:
        tw['retweets'] = df.groupby(['rt', 'twitter_label']).size().loc[consts.tweet_types['retweet']]
    except KeyError:
        tw['retweets'] = 0
    try:
        tw['comments'] = df.groupby(['rt', 'twitter_label']).size().loc[consts.tweet_types['comment']]
    except KeyError:
        tw['comments'] = 0

    tw['language'] = tw.index.map({v: k for k, v in language_codes.items()})
    tw = tw.set_index('language')

    ft = df.fastText_label.value_counts().to_frame(name='count')
    ft['speakers'] = df.groupby(['user_id', 'fastText_label'], as_index=False) \
        .size().reset_index()['fastText_label'].value_counts()
    try:
        ft['tweets'] = df.groupby(['rt', 'fastText_label']).size().loc[consts.tweet_types['tweet']]
    except KeyError:
        ft['tweets'] = 0
    try:
        ft['retweets'] = df.groupby(['rt', 'fastText_label']).size().loc[consts.tweet_types['retweet']]
    except KeyError:
        ft['retweets'] = 0
    try:
        ft['comments'] = df.groupby(['rt', 'fastText_label']).size().loc[consts.tweet_types['comment']]
    except KeyError:
        ft['comments'] = 0
    ft['language'] = ft.index.map({v: k for k, v in language_codes.items()})
    ft = ft.set_index('language')

    stats['languages'] = [code for code in language_codes if code in ft.index and ft.loc[code]['count'] != 0]

    if len(tw) > 0:
        tw['rank'] = tw['count'].rank(method='average', ascending=False)
        total = tw['count'].sum()
        tw['freq'] = tw['count'] / total
    else:
        print('Warning: no twitter labels!')

    if len(ft) > 0:
        ft['rank'] = ft['count'].rank(method='average', ascending=False)
        total = ft['count'].sum()
        ft['freq'] = ft['count'] / total
    else:
        print('Warning: no fastText labels!')

    ft = ft.add_prefix('ft_')
    tw = tw.add_prefix('tw_')
    df = pd.concat([ft, tw], axis=1, join='outer', sort=False).sort_values('ft_count', ascending=False)
    df.index.name = 'language'
    df.to_csv(lang_path)

    for lang in stats['languages']:
        try:
            stats[lang] = {'num_activities': int(df.loc[lang, 'ft_count'])}
        except ValueError:
            stats[lang] = {'num_activities': 0}

        try:
            stats[lang] = {'num_tweets': int(df.loc[lang, 'ft_tweets'])}
        except ValueError:
            stats[lang] = {'num_tweets': 0}

        try:
            stats[lang] = {'num_retweets': int(df.loc[lang, 'ft_retweets'])}
        except ValueError:
            stats[lang] = {'num_retweets': 0}

        try:
            stats[lang] = {'num_comments': int(df.loc[lang, 'ft_comments'])}
        except ValueError:
            stats[lang] = {'num_comments': 0}


def agg_langs(stats, path, scheme):
    """Aggregate ngrams for every language

    Args:
      stats: a dict of running stats
      path: path to .tsv file(s)
      scheme: degree of the ngrams

    """
    for f in sorted(path.rglob('*.json.gz'), key=os.path.getsize):
        f = Path(f)
        cc = counter.NgramCounter({})
        t = time.time()
        lang = f.stem.split('_')[0]

        with gzip.open(f, 'r') as data:
            for batch in data:
                d = counter.NgramCounter(ujson.loads(batch))
                if cc is not None:
                    cc += d
                else:
                    cc = d

        df = pd.DataFrame.from_dict(
            cc,
            orient='index',
            columns=['count', 'count_no_rt']
        )
        df['rank'] = df['count'].rank(method='average', ascending=False)
        df['rank_no_rt'] = df['count_no_rt'].rank(method='average', ascending=False)
        df['freq'] = df['count'] / df['count'].sum()
        df['freq_no_rt'] = df['count_no_rt'] / df['count_no_rt'].sum()
        df.index.name = 'ngram'
        df.sort_values(by='count', ascending=False, inplace=True)

        df.to_csv(path/Path(f'{f.stem.split(".")[0]}.tsv.gz'), sep='\t')
        os.remove(f)

        try:
            stats[lang][f'unique_{scheme}grams'] = int(df.shape[0])
            stats[lang][f'num_{scheme}grams'] = int(df['count'].sum())
            stats[lang][f'num_{scheme}grams_no_rt'] = int(df['count_no_rt'].sum())
        except KeyError:
            stats[lang] = {}
            stats[lang][f'unique_{scheme}grams'] = int(df.shape[0])
            stats[lang][f'num_{scheme}grams'] = int(df['count'].sum())
            stats[lang][f'num_{scheme}grams_no_rt'] = int(df['count_no_rt'].sum())

        print(
            f"{time.time() - t:.2f} sec. | ({lang}) "
            f"[{stats[lang][f'unique_{scheme}grams']}] unique {scheme}grams | "
            f"{df_mem_usage(df)} GB (DataFrame) ~ {get_obj_size(cc)} GB (NgramCounter)"
        )


def collect_ngrams(
    pth,
    scheme,
    model,
    emoji_parser,
    ngrams_parser,
    savepath,
    threshold,
    p_language_codes,
    target_langs,
    s_predictions,
    case_sensitive,
    anchor
):
    """Run fastText on a full day folder

    Args:
      pth: path to a raw compressed 15-minutes twitter file(s)
      scheme: degree of the ngrams
      model: path to a fastText model
      emoji_parser: a compiled regular expression that matches emojis
      ngrams_parser: a compiled regular expression that matches ngrams
      savepath: path to save predictions and ngrams
      threshold: confidence score threshold for the language identifier
      p_language_codes: path to a dict of language codes
      target_langs: a list of languages to parse out
      s_predictions: a toggle to keep track of network predictions
      case_sensitive: a toggle to get case sensitive ngrams
      anchor: path to a json file of anchor ngrams to subsample tweets

    """
    pth = Path(pth)
    savedir = Path(savepath)
    if not pth.exists() or not pth.is_dir():
        print(f'Error: {pth} - no such directory')
        return False

    if s_predictions:
        out = Path(f'{savedir.parent}/predictions')
        out.mkdir(parents=True, exist_ok=True)
        pred_path = Path(f'{out}/{pth.stem}_predictions.csv.gz')

    clf = fasttext.load_model(model)
    language_codes = fileio.load_json(p_language_codes)
    cols = ['twitter_label', 'fastText_label', 'fastText_conf_score', 'user_id', 'rt', 'length']
    todo = sorted([b.stem.split('.')[0] for b in pth.rglob('*.gz')])

    if list(savedir.parent.rglob(f'{pth.stem}*')):
        completed = set()
        for f in savedir.parent.iterdir():
            completed.add(
                frozenset(todo).symmetric_difference(
                    frozenset([b.stem.split('.')[0] for b in f.rglob(f'{pth.stem}*.tsv.gz')])
                )
            )
        todo = sorted(set().union(*completed))

    print(f"TODO: {todo}")

    if anchor is not None:
        anchor_regex = regexr.create_anchor_regex(anchor)
    else:
        anchor_regex = None

    # -------------------------------------------------------------------------------------------------- #
    print('Classifying tweets...')
    print('-' * 60)
    for i, b in enumerate(sorted(pth.rglob('*.gz'))):

        # check if already completed
        if b.stem not in todo:
            continue

        print(b.stem)
        t = time.time()
        acts_batch, corrupted_batch, predictions_batch, ngrams_batch = process_batch(
            b,
            clf=clf,
            threshold=threshold,
            emoji_parser=emoji_parser,
            ngrams_parser=ngrams_parser,
            language_codes=language_codes,
            scheme=scheme,
            target_langs=target_langs,
            case_sensitive=case_sensitive,
            anchor_regex=anchor_regex
        )

        if s_predictions:
            if pred_path.exists():
                fileio.write_gzip_csv(pred_path, predictions_batch, ['id'] + cols, mode='at')
            else:
                fileio.write_gzip_csv(pred_path, predictions_batch, ['id'] + cols, mode='wt')
            print(f'[{get_obj_size(predictions_batch)} GB] (PredictionsDict)')
            del predictions_batch

        for lang in ngrams_batch.keys():
            if ngrams_batch[lang] != counter.NgramCounter({}):

                df = pd.DataFrame.from_dict(ngrams_batch[lang], orient='index', columns=['count', 'count_no_rt'])
                df['rank'] = df['count'].rank(method='average', ascending=False)
                df['rank_no_rt'] = df['count_no_rt'].rank(method='average', ascending=False)
                df['freq'] = df['count'] / df['count'].sum()
                df['freq_no_rt'] = df['count_no_rt'] / df['count_no_rt'].sum()
                df.index.name = 'ngram'

                total_at = df['count'].sum()
                total_ot = df['count_no_rt'].sum()
                ratio = (df['count'] - df['count_no_rt']) / df['count']
                lang_ratio = (total_at - total_ot) / total_at
                df['r_rel'] = (ratio / lang_ratio).replace([np.inf, -np.inf, np.nan], 1)

                df = df.sort_values(by='count', ascending=False)

                print(
                    f'{lang} | '
                    f'MEM={round(df_mem_usage(df) + get_obj_size(ngrams_batch[lang]), 2)} GB | '
                    f'[{len(df)}] unique {scheme}grams')

                out = Path(f"{savedir.parent}/{lang}/{savedir.stem}")
                out.mkdir(parents=True, exist_ok=True)
                df.to_csv(f'{out}/{b.stem}.tsv.gz', sep='\t')

        print(f'Processing time: {time.time() - t:.2f} sec.')
        print('-' * 60)
    # -------------------------------------------------------------------------------------------------- #
    return True


def aggregate_ngrams(
    indir,
    outdir,
    resolution
):
    """Run fastText on a full day folder

    Args:
      indir: path to daily compressed tarballs
      outdir: path to save aggregated ngrams
      resolution: a time-scale to aggregate on (i.e. 'W', 'M', 'Y')

    """
    if not indir.exists() or not indir.is_dir():
        print(f'Error: {indir} - no such directory')
        return False

    files = list(indir.glob('*.tar.gz'))
    dates = [datetime.strptime(str(f.stem.split('.')[0]), '%Y-%m-%d') for f in files]
    df = pd.DataFrame(files, index=dates, columns=['path'])
    groups = df.resample(resolution)
    date_ranges = [[timestamp.to_pydatetime(), list(g.path.values)] for timestamp, g in groups.__iter__()]

    ngrams = {}
    for timestamp, files in date_ranges:
        ngrams[timestamp] = {}
        for path in files:
            dataframes = tweet_utils.read_tarball_multi(
                path,
                filenames=['tsv'],
                kwargs=dict(
                    header=0,
                    encoding='utf-8',
                    sep='\t',
                    na_filter=False,
                    compression='gzip',
                    index_col=0,
                    usecols=['ngram', 'count', 'count_no_rt']
                )
            )
            for k in dataframes.keys():
                lang = Path(k).stem.split('_')[0]

                if ngrams[timestamp].get(lang) is None:
                    ngrams[timestamp][lang] = dataframes[k].copy()
                else:
                    ngrams[timestamp][lang] = ngrams[timestamp][lang].add(dataframes[k], fill_value=0)

    for timestamp, dfs in ngrams.items():
        savedir = outdir / str(timestamp.date())
        savedir.mkdir(parents=True, exist_ok=True)
        print(savedir)

        for lang in dfs.keys():
            dfs[lang]['rank'] = dfs[lang]['count'].rank(method='average', ascending=False)
            dfs[lang]['rank_no_rt'] = dfs[lang]['count_no_rt'].rank(method='average', ascending=False)

            try:
                dfs[lang]['freq'] = dfs[lang]['count'] / dfs[lang]['count'].sum()
            except ZeroDivisionError:
                dfs[lang]['freq'] = 0

            try:
                dfs[lang]['freq_no_rt'] = dfs[lang]['count_no_rt'] / dfs[lang]['count_no_rt'].sum()
            except ZeroDivisionError:
                dfs[lang]['freq_no_rt'] = 0

            dfs[lang].sort_values(by='count', ascending=False, inplace=True)
            dfs[lang].to_csv(savedir / f'{lang}_{str(timestamp.date())}.tsv.gz', sep='\t')

            print(
                f"({lang}) [{dfs[lang].shape[0]}] ~ {df_mem_usage(dfs[lang])} GB"
            )

        print('Compressing files... ')
        with tarfile.open(f'{savedir}.tar.gz', "w:gz") as tar:
            tar.add(savedir, arcname=savedir.stem)

        print('Deleting temporary files... ')
        shutil.rmtree(savedir, ignore_errors=True)

    return True


@contextlib.contextmanager
def dummy_context_mgr():
    """For conditional context managers, and other purposes"""
    yield None


def predict_aggregate(
    pth,
    scheme,
    classifier,
    model,
    emoji_parser,
    ngrams_parser,
    savepath,
    threshold,
    p_language_codes,
    ignore,
    s_predictions,
    s_languages,
    config,
    anchor
):
    """Run fastText on a full day folder

    Args:
      pth: path to a raw compressed 15-minutes twitter file(s)
      scheme: degree of the ngrams
      classifier: label to use when parsing out n-grams (ft: FastText, tw: Twitter)
      model: path to a fastText model
      emoji_parser: a compiled regular expression that matches emojis
      ngrams_parser: a compiled regular expression that matches ngrams
      savepath: path to save predictions
      threshold: confidence score threshold for the language identifier
      p_language_codes: path to a dict of language codes
      ignore: a list of languages to ignore
      s_predictions: a toggle to keep track of network predictions
      s_languages: a toggle to collect language statistics
      config: a dict of package paths
      anchor: path to a json file of anchor ngrams to subsample tweets

    """

    langdf = None
    pth = Path(pth)
    savedir = Path(savepath)
    if not pth.exists():  # or not pth.is_dir():
        print(f'Error: {pth} - no such directory')
        return False

    lang_path = f"{config['languages']}/{pth.stem}.csv.gz" if "storyons" in str(savedir) \
        else f'{savedir.parent}/languages/{pth.stem}.csv.gz'

    Path(f'{savedir}/{scheme}grams').mkdir(parents=True, exist_ok=True)
    Path(lang_path).parent.mkdir(parents=True, exist_ok=True)

    corrupted = 0
    ngrams = counter.NgramCounter({})
    clf = fasttext.load_model(model)
    language_codes = fileio.load_json(p_language_codes)
    cols = ['twitter_label', 'fastText_label', 'fastText_conf_score', 'user_id', 'rt', 'length']

    if anchor is not None:
        anchor_regex = regexr.create_anchor_regex(anchor)
    else:
        anchor_regex = None

    def tweet_laundromat(act):
        """Parse out an activity object and update counters"""
        text = regexr.remove_whitespaces(act['text'])
        emoji_freezone = emoji_parser.sub(r'', regexr.filter_text(text))
        emoji_freezone = regexr.remove_whitespaces(emoji_freezone)
        ft_lang, ft_conf = predict_lang(clf.predict(emoji_freezone), threshold)

        if language_codes.get(act['lang']) is None:
            language_codes[act['lang']] = len(language_codes)

        if language_codes.get(ft_lang) is None:
            language_codes[ft_lang] = len(language_codes)

        if act['tid'] is not None and s_predictions:
            predictions[act['tid']] = [
                language_codes[act['lang']],
                language_codes[ft_lang],
                ft_conf,
                act['uid'],
                act['rt'],
                act['length']
            ]

        ########################################################################################
        # DISCLAIMER: Current Ngrams parser does NOT support continuous-script based languages #
        ########################################################################################
        if ft_lang not in ignore:
            if (anchor_regex is not None) and (not any(re.findall(anchor_regex, text))):
                ngs = None
            else:
                # get a Counter object for given tweet
                ngs = regexr.ngrams(text, parser=ngrams_parser, n=scheme)

            if ngs is not None:
                if act['rt'] == consts.tweet_types['retweet']:
                    ngs = counter.NgramCounter(
                        {k: counter.Storyon(count=v, count_no_rt=0) for k, v in ngs.items()}
                    )
                else:
                    ngs = counter.NgramCounter(
                        {k: counter.Storyon(count=v, count_no_rt=v) for k, v in ngs.items()}
                    )

                if classifier == 'ft':
                    if ngrams.get(ft_lang) is not None:
                        ngrams[ft_lang] += ngs
                    else:
                        ngrams[ft_lang] = ngs

                else:
                    if ngrams.get(act['lang']) is not None:
                        ngrams[act['lang']] += ngs
                    else:
                        ngrams[act['lang']] = ngs

    # Process 15-minute chunks for given day
    print('Classifying tweets...')
    predictions = {}
    # -------------------------------------------------------------------------------------------------- #
    # tar ball here
    is_tarball = bool(pth.suffix == '.gz')

    with tarfile.open(pth) if is_tarball else dummy_context_mgr() as cm:

        if is_tarball:
            f_iter = [x for x in cm.getnames() if x.endswith('.gz')]
        else:
            f_iter = pth.rglob('*.gz')

        for b in f_iter:
            start = time.time()
            if is_tarball:
                b = cm.extractfile(b)
            # -------------------------------------------------------------------------------------------------- #
            try:
                with gzip.open(b, 'rb') as data:
                    try:
                        acts = 0
                        for line in data:

                            acts += 1
                            if line.isspace():
                                # filter out empty tweets
                                corrupted += 1
                                continue

                            try:
                                activities = parse_activity(ujson.loads(line))
                            except ValueError:
                                corrupted += 1
                                continue

                            for activity in activities:
                                if activity is not None:
                                    try:
                                        tweet_laundromat(activity)
                                    except Exception:
                                        traceback.print_exc()
                                        corrupted += 1
                                        continue

                    except EOFError:
                        print('Warning: 15-minute batch is incomplete!')

            except Exception:
                traceback.print_exc()
                print('Warning: corrupted file: ', b)
                continue

            b = Path(str(b))
            # -------------------------------------------------------------------------------------------------- #
            print(f'{time.time() - start:.2f} sec. | {b.stem} | [{acts}] activities')

    del clf
    print('-' * 60)
    # -------------------------------------------------------------------------------------------------- #

    if len(ngrams.keys()) == 0:
        print(f'Warning: no n-grams were found! | {pth}')
        return False

    stats = {}

    if s_predictions:
        print('Aggregating predictions...')
        # -------------------------------------------------------------------------------------------------- #
        start = time.time()

        df = pd.DataFrame.from_dict(predictions, orient='index', columns=cols)
        del predictions

        rts = df.rt.value_counts().to_dict()
        try:
            stats['num_tweets'] = rts[consts.tweet_types['tweet']]
        except KeyError:
            stats['num_tweets'] = 0

        try:
            stats['num_retweets'] = rts[consts.tweet_types['retweet']]
        except KeyError:
            stats['num_retweets'] = 0

        try:
            stats['num_comments'] = rts[consts.tweet_types['comment']]
        except KeyError:
            stats['num_comments'] = 0

        stats['num_corrupted'] = int(corrupted)
        stats['num_activities'] = int(df.shape[0])

        print(f'Total number of tweets: {stats["num_tweets"]}')
        print(f'Total number of [re]tweets: {stats["num_retweets"]}')
        print(f'Total number of comments: {stats["num_comments"]}')
        print(f'Total number of corrupted tweets: {stats["num_corrupted"]}')
        print(f'Total number of valid activities: {stats["num_activities"]}')

        df.index.name = 'id'

        df.to_csv(f'{savedir}/{pth.stem}_predictions.csv.gz')
        print(f'{time.time() - start:.2f} sec. | {pth.stem}_predictions.csv.gz | MEM={df_mem_usage(df)} GB')

        if s_languages:
            print('-' * 60)
            print('Aggregating languages...')
            # -------------------------------------------------------------------------------------------------- #
            fileio.save_json(language_codes, p_language_codes)
            start = time.time()

            tw = df.twitter_label.value_counts().to_frame(name='count')
            tw['speakers'] = df.groupby(['user_id', 'twitter_label'], as_index=False) \
                .size().reset_index()['twitter_label'].value_counts()

            try:
                tw['tweets'] = df.groupby(['rt', 'twitter_label']).size().loc[consts.tweet_types['tweet']]
            except KeyError:
                tw['tweets'] = 0
            try:
                tw['retweets'] = df.groupby(['rt', 'twitter_label']).size().loc[consts.tweet_types['retweet']]
            except KeyError:
                tw['retweets'] = 0
            try:
                tw['comments'] = df.groupby(['rt', 'twitter_label']).size().loc[consts.tweet_types['comment']]
            except KeyError:
                tw['comments'] = 0

            tw['language'] = tw.index.map({v: k for k, v in language_codes.items()})
            tw = tw.set_index('language')

            ft = df.fastText_label.value_counts().to_frame(name='count')
            ft['speakers'] = df.groupby(['user_id', 'fastText_label'], as_index=False) \
                .size().reset_index()['fastText_label'].value_counts()
            try:
                ft['tweets'] = df.groupby(['rt', 'fastText_label']).size().loc[consts.tweet_types['tweet']]
            except KeyError:
                ft['tweets'] = 0
            try:
                ft['retweets'] = df.groupby(['rt', 'fastText_label']).size().loc[consts.tweet_types['retweet']]
            except KeyError:
                ft['retweets'] = 0
            try:
                ft['comments'] = df.groupby(['rt', 'fastText_label']).size().loc[consts.tweet_types['comment']]
            except KeyError:
                ft['comments'] = 0
            ft['language'] = ft.index.map({v: k for k, v in language_codes.items()})
            ft = ft.set_index('language')

            if classifier == 'ft':
                stats['languages'] = [code for code in language_codes
                                      if code in ft.index and ft.loc[code]['count'] != 0]
            else:
                stats['languages'] = [code for code in language_codes
                                      if code in tw.index and tw.loc[code]['count'] != 0]

            if len(tw) > 0:
                tw['rank'] = tw['count'].rank(method='average', ascending=False)
                total = tw['count'].sum()
                tw['freq'] = tw['count'] / total
            else:
                print('Warning: no twitter labels!')

            if len(ft) > 0:
                ft['rank'] = ft['count'].rank(method='average', ascending=False)
                total = ft['count'].sum()
                ft['freq'] = ft['count'] / total
            else:
                print('Warning: no fastText labels!')

            ft = ft.add_prefix('ft_')
            tw = tw.add_prefix('tw_')
            langdf = pd.concat([ft, tw], axis=1, join='outer', sort=False).sort_values('ft_count', ascending=False)
            langdf.index.name = 'language'
            langdf.to_csv(f'{savedir}/{pth.stem}_languages.csv.gz')
            langdf.to_csv(lang_path)

            for lang in stats['languages']:
                try:
                    stats[lang] = {'num_activities': int(langdf.loc[lang, 'ft_count'])}
                except ValueError:
                    stats[lang] = {'num_activities': 0}

                try:
                    stats[lang] = {'num_tweets': int(langdf.loc[lang, 'ft_tweets'])}
                except ValueError:
                    stats[lang] = {'num_tweets': 0}

                try:
                    stats[lang] = {'num_retweets': int(langdf.loc[lang, 'ft_retweets'])}
                except ValueError:
                    stats[lang] = {'num_retweets': 0}

                try:
                    stats[lang] = {'num_comments': int(langdf.loc[lang, 'ft_comments'])}
                except ValueError:
                    stats[lang] = {'num_comments': 0}

            print(f'{time.time() - start:.2f} sec. | {pth.stem}_languages.csv.gz | MEM={df_mem_usage(langdf)} GB')
            print('-' * 45)
            # -------------------------------------------------------------------------------------------------- #

    if scheme >= 1:
        print('Aggregate n-grams...')
        # -------------------------------------------------------------------------------------------------- #
        start = time.time()
        all_ngrams = counter.NgramCounter({})

        def ngram_writer(d, lang):
            df = pd.DataFrame.from_dict(d, orient='index', columns=['count', 'count_no_rt'])
            df['rank'] = df['count'].rank(method='average', ascending=False)
            df['rank_no_rt'] = df['count_no_rt'].rank(method='average', ascending=False)
            df['freq'] = df['count'] / df['count'].sum()
            df['freq_no_rt'] = df['count_no_rt'] / df['count_no_rt'].sum()
            df.index.name = 'ngram'
            df = df.sort_values(by='count', ascending=False)

            if lang == '':
                stats[f'unique_{scheme}grams'] = int(df.shape[0])
                stats[f'num_{scheme}grams'] = int(df['count'].sum())
                stats[f'num_{scheme}grams_no_rt'] = int(df['count_no_rt'].sum())
            else:
                try:
                    languages[lang][f'num_{scheme}grams'] = int(df['count'].sum())
                    languages[lang][f'num_{scheme}grams_no_rt'] = int(df['count_no_rt'].sum())
                    languages[lang][f'unique_{scheme}grams'] = int((df['count'] != 0).sum())
                    languages[lang][f'unique_{scheme}grams_no_rt'] = int((df['count_no_rt'] != 0).sum())

                except IndexError:
                    languages[lang][f'num_{scheme}grams'] = 0
                    languages[lang][f'num_{scheme}grams_no_rt'] = 0
                    languages[lang][f'unique_{scheme}grams'] = 0
                    languages[lang][f'unique_{scheme}grams_no_rt'] = 0

                try:
                    stats[lang][f'unique_{scheme}grams'] = int(df.shape[0])
                    stats[lang][f'num_{scheme}grams'] = int(df['count'].sum())
                    stats[lang][f'num_{scheme}grams_no_rt'] = int(df['count_no_rt'].sum())
                except KeyError:
                    stats[lang] = {}
                    stats[lang][f'unique_{scheme}grams'] = int(df.shape[0])
                    stats[lang][f'num_{scheme}grams'] = int(df['count'].sum())
                    stats[lang][f'num_{scheme}grams_no_rt'] = int(df['count_no_rt'].sum())

            print(f'{lang} | MEM={round(df_mem_usage(df) + get_obj_size(d), 2)} GB | [{len(df)}] unique {scheme}grams')
            df.to_csv(f'{savedir}/{scheme}grams/{lang}_{pth.stem}.tsv.gz', sep='\t')
            del df

        languages = {lang: {} for lang in list(ngrams.keys())}
        for lang in languages:
            if ngrams[lang] != counter.Storyon(0, 0):
                ngram_writer(ngrams[lang], lang)
                all_ngrams += ngrams[lang]
                del ngrams[lang]

        if s_languages:
            def save_langs(data):
                try:
                    on_disk = pd.read_csv(
                        lang_path,
                        header=0,
                        index_col='language',
                        compression='gzip',
                        encoding='utf-8',
                        sep=',',
                    )
                    data = on_disk.combine_first(data)

                except FileNotFoundError:
                    pass

                if int(scheme) == 3:
                    data.loc['_all'] = data.sum()

                data.index.name = 'language'
                data.to_csv(lang_path)

            d = pd.DataFrame.from_dict(
                languages,
                orient='index',
                columns=[
                    f'num_{scheme}grams',
                    f'num_{scheme}grams_no_rt',
                    f'unique_{scheme}grams',
                    f'unique_{scheme}grams_no_rt'
                ]
            )

            if langdf is None:
                save_langs(d)
            else:
                langdf = langdf.combine_first(d)
                save_langs(langdf)

        print('-' * 45)
        ngram_writer(all_ngrams, '')
        del all_ngrams
        # -------------------------------------------------------------------------------------------------- #
        fileio.save_json(stats, f'{savepath}/{pth.stem}_{scheme}grams_summary_fastText.json')
        print(f'{time.time() - start:.2f} sec. | {pth.stem}_{scheme}grams_summary_fastText.json')
        print('-' * 60)

        return True


def select_tweets(
        pth,
        model,
        emoji_parser,
        savepath,
        threshold,
        lang,
        anchor
):
    """Run fastText on a full day folder to select tweets for a given language

    Args:
      pth: path to a raw compressed 15-minutes twitter file(s)
      model: path to a fastText model
      emoji_parser: a compiled regular expression that matches emojis
      savepath: path to save predictions
      threshold: confidence score threshold for the language identifier
      lang: desired language for tweet selection
      anchor: path to a json file of anchor ngrams to subsample tweets

    """
    batch_size = 10**5

    if not pth.exists():
        print(f'Error: {pth} - no such directory')
        return False

    corrupted = 0
    clf = fasttext.load_model(model)

    if anchor is not None:
        anchor_regex = regexr.create_anchor_regex(anchor)
    else:
        anchor_regex = None


    # Process 15-minute chunks for given day
    print('Classifying tweets...')
    # -------------------------------------------------------------------------------------------------- #
    is_tarball = bool(pth.suffix == '.gz')

    with tarfile.open(pth) if is_tarball else dummy_context_mgr() as cm:

        if is_tarball:
            f_iter = [x for x in cm.getnames() if x.endswith('.gz')]
        else:
            f_iter = pth.rglob('*.gz')

        for b in sorted(f_iter):
            start = time.time()
            if is_tarball:
                b = cm.extractfile(b)
            # -------------------------------------------------------------------------------------------------- #
            try:
                with gzip.open(b, 'rb') as data:
                    msgs = 0
                    batch = []
                    batch_path = savepath / f'{b.stem}.gz'

                    for line in data:
                        if line.isspace():
                            # filter out empty tweets
                            corrupted += 1
                            continue

                        try:
                            obj = ujson.loads(line)
                            messages = parse_activity(obj)
                        except ValueError:
                            corrupted += 1
                            continue

                        for msg in messages:
                            if msg is not None:
                                try:
                                    text = regexr.remove_whitespaces(msg['text'])
                                    emoji_freezone = emoji_parser.sub(r'', regexr.filter_text(text))
                                    emoji_freezone = regexr.remove_whitespaces(emoji_freezone)
                                    ft_lang, ft_conf = predict_lang(clf.predict(emoji_freezone), threshold)

                                    if ft_lang == lang:
                                        if (anchor_regex is not None) and (not any(re.findall(anchor_regex, text))):
                                            msgs += 1
                                            batch.append(obj)

                                            if len(batch) >= batch_size:
                                                fileio.save_batch(batch_path, batch, mode="a+")
                                                batch = []  # reset batch

                                except Exception:
                                        traceback.print_exc()
                                        corrupted += 1
                                        continue

                    # write out remaining tweets
                    fileio.save_batch(batch_path, batch, mode="a+")

            except Exception:
                traceback.print_exc()
                print('Warning: corrupted file: ', b)
                continue

            b = Path(str(b))
            # -------------------------------------------------------------------------------------------------- #
            print(f'{time.time() - start:.2f} sec. | {b.stem} | [{msgs}] messages')

    del clf
    print('-' * 60)
    # -------------------------------------------------------------------------------------------------- #

    return True
