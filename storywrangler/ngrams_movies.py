
from datetime import timedelta
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def load_movies(movie_path):
    """Load the movies dataset and return datetime dictionaries"""
    movies = pd.read_csv(movie_path)
    movies = movies[movies.release_date > '2010-01-01']

    movies = movies[movies.revenue > movies.revenue.quantile(.95)]

    movies = movies.loc[~movies.set_index('original_title').index.duplicated(keep='first')]

    movies.release_date = pd.to_datetime(movies.release_date, infer_datetime_format=True)

    movies['year'] = movies.release_date.dt.year

    movie2year = movies[['original_title', 'year']].set_index('original_title').to_dict()['year']

    movie2date = movies[['original_title', 'release_date']].set_index('original_title').to_dict()['release_date']

    movie2shortnames = {k: ' '.join(k.split(':')[0].split()[:3]) for k in movies.original_title}

    return movie2year, movie2date, movie2shortnames, movies


def find_peak_year(df, year, measure):
    """find peak in a given year

    Args:
      df: dataframe with 'year' column
      year: target year
      measure: which column to use (e.g., freq, rank)

    Returns:
        max value in that year

    """
    temp = df[df.index.year == year]
    return temp[measure].idxmax()


def growth_decay_finder(ts_df, measure='freq', starting_val=0.01, max_loc=None, increasing=True):
    """Get the doubling time to peak

    Args:
      ts_df: time series DataFrame from storywrangler
      measure: column of df (Default value = 'freq')
      starting_val: lookback value for start of growth period (Default value = 0.01)
      max_loc: send a maximum value (Default value = None)
      increasing:  (Default value = True)

    Returns:
      jump_time: time from start of period to max
      period_start: period boundary
      max_loc: location of max value
      cutoff: the cutoff value for the region
      max_val: the maximum value for the TS

    """
    if not max_loc:
        max_loc = ts_df[measure].idxmax()
    max_val = ts_df.loc[max_loc][measure]

    if increasing:
        poi = ts_df.loc[:max_loc]
    else:
        poi = ts_df.loc[max_loc:]

    # find the recent contiguous block from .01 to 1.0
    cutoff = max_val * starting_val

    target_region = poi[measure].tail(-1)[np.diff(poi[measure] >= cutoff) != 0].index
    if increasing:
        period_start = target_region.max()
    else:
        period_start = target_region.min()

    jump_time = abs((period_start - max_loc).days)
    return jump_time, period_start, max_loc, cutoff, max_val


def calc_percent_change(movie2shortnames, movie2year, movie2date, movie2tsdf, savepath, plot=False):
    """Find regions of interest for a variety of cutoff values"""

    movie2dub = {}
    movie2half = {}
    movie2peakfreq = {}
    for i, (key, val) in enumerate(movie2shortnames.items()):

        print(i, key, end='\t \r')

        try:
            this_data = movie2tsdf[val].rolling(7, center=True).mean()
            peak_freq = find_peak_year(this_data, movie2year[key], 'freq')
            movie2peakfreq.update({key: peak_freq})

            if plot:
                f, a = plt.subplots(1, 1, figsize=(12, 8))

                a.plot(movie2tsdf[val]['freq'], marker='o', color='lightgrey', linewidth=0, label=val)

                a.plot(this_data['freq'], color='k')
                a.set_title(f'{key} \n {movie2year[key]}')
                a.semilogy()
                a.grid()

                a.axvline(movie2date[key], color='pink', label='release date')
                a.axvline(peak_freq, color='grey', linestyle='dashed')

                a.set_ylim(10 ** -8, 10 ** -3)
                a.legend()
                plt.xticks(rotation=70)

                plt.savefig(f'{savepath}/001_{key.strip(" ")}.pdf')
                plt.close()

            dtimes = []
            htimes = []
            for thres in [.01, .1, .25, .5, .75, .9]:
                dub = growth_decay_finder(this_data,
                                          starting_val=thres,
                                          max_loc=peak_freq,
                                          increasing=True)
                half = growth_decay_finder(this_data,
                                           starting_val=thres,
                                           max_loc=peak_freq,
                                           increasing=False)
                if thres == .5 and plot:
                    a.axvline(dub[1], color='green', linestyle=':', label='50% before peak')
                    a.axvline(dub[2], color='blue', linestyle=':', label='Release year peak')
                    a.axvline(half[1], color='red', linestyle=':', label='50% after peak')
                    a.axhline(this_data.loc[peak_freq]['freq'] * .5)
                    a.set_xlim(dub[1] - timedelta(days=30), half[1] + timedelta(days=30))
                dtimes.append(dub[0])
                htimes.append(half[0])
            movie2dub.update({key: dtimes})
            movie2half.update({key: htimes})

        except (AttributeError, TypeError, ValueError) as e:
            print(e, key)
            continue
    return movie2peakfreq, movie2half, movie2dub


def get_dec_inc_df(movie2shortnames, movie2year, movie2date, movie2tsdf, movies, savepath):

    movie2peakfreq, movie2half, movie2dub = calc_percent_change(
        movie2shortnames,
        movie2year,
        movie2date,
        movie2tsdf,
        savepath
    )

    increasing_df = pd.DataFrame(pd.DataFrame([movie2dub]).T[0].to_list(),
                                 index=pd.DataFrame([movie2dub]).T.index)  # .unstack()
    increasing_df.columns = [f'd{i}' for i in [.01, .1, .25, .5, .75, .9]]
    decreasing_df = pd.DataFrame(pd.DataFrame([movie2half]).T[0].to_list(),
                                 index=pd.DataFrame([movie2half]).T.index)  # .unstack()
    decreasing_df.columns = [f'h{i}' for i in [.01, .1, .25, .5, .75, .9]]

    dec_inc_df = pd.concat([increasing_df, decreasing_df, ], axis=1)

    dec_inc_df = pd.concat([dec_inc_df, movies.set_index('original_title')], axis=1)

    peaks = pd.DataFrame([movie2peakfreq]).T
    peaks.columns = ['peak']
    dec_inc_df = pd.concat([dec_inc_df, peaks], axis=1)

    dec_inc_df['peak_error'] = (dec_inc_df.peak - dec_inc_df.release_date).dt.days

    return dec_inc_df
