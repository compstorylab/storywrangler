
from __future__ import unicode_literals

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.offline as py
import seaborn as sns
from bidi import algorithm as bidialg
from matplotlib.gridspec import GridSpec
from matplotlib.lines import Line2D
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from pandas.plotting import register_matplotlib_converters
from plotly.subplots import make_subplots
from datetime import datetime, timedelta, date
import matplotlib.units as munits

from storywrangler import consts

register_matplotlib_converters()

import warnings
warnings.simplefilter("ignore")

plt.rcParams["font.family"] = "serif"
plt.rcParams["font.serif"] = ["Times New Roman"] + plt.rcParams["font.serif"]


def plot_zipf(savepath, ngrams_dict):

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    cols, rows = 2, 4
    fig, axes = plt.subplots(figsize=(8, 10), ncols=cols, nrows=rows)

    for n, dfs in ngrams_dict.items():
        for i, (label, d) in enumerate(dfs.items()):

            axes[i, 0].loglog(
                d['rank'],
                d['freq'],
                '.',
                color=consts.ngrams_colors[n],
                mfc=consts.ngrams_colors[n],
                mec=consts.ngrams_colors[n],
                label=n
            )
            axes[i, 0].grid(True, which="both", alpha=.5)
            axes[i, 0].set_ylabel("Rate of usage")

            axes[i, 1].loglog(
                d['rank_no_rt'],
                d['freq_no_rt'],
                '.',
                color=consts.ngrams_colors[n],
                mfc=consts.ngrams_colors[n],
                mec=consts.ngrams_colors[n],
                label=n
            )

            axes[i, 1].grid(True, which="both", alpha=.5)

            axes[i, 0].set_xlim(10**0, 10**9)
            axes[i, 1].set_xlim(10**0, 10**9)
            axes[i, 0].set_ylim(10**-9, 10**0)
            axes[i, 1].set_ylim(10**-9, 10**0)

            if i == 0:
                axes[i, 0].set_title(f'All tweets (AT)')
                axes[i, 1].set_title(f'Organic tweets (OT)')

                axes[i, 0].legend(frameon=0, loc='upper right', columnspacing=.5)
            else:
                axes[i, 0].set_title(f'{label} (AT)')
                axes[i, 1].set_title(f'{label} (OT)')

            if i == 3:
                axes[i, 0].set_xlabel('Word rank')
                axes[i, 1].set_xlabel('Word rank')

    sns.despine(offset=5)
    plt.tight_layout()
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_divergence(df, savepath, targets):

    rmax = 10 ** 7
    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif'
    })

    fig, ax = plt.subplots(figsize=(10, 10))

    east, west = {}, {}
    for n in df.index.values:
        if df.loc[n, targets[0]] > df.loc[n, targets[1]]:
            west[n] = [df.loc[n, targets[0]], df.loc[n, targets[1]]]
        else:
            east[n] = [df.loc[n, targets[0]], df.loc[n, targets[1]]]

    x = np.array(list(west.values()))[:, 0]
    y = np.array(list(west.values()))[:, 1]
    ax.scatter(x, y, marker='s', c=y, s=10, cmap='summer', alpha=.66)

    separator = range(1, rmax)
    ax.loglog(separator, separator, 'k', alpha=.5)

    x = np.array(list(east.values()))[:, 0]
    y = np.array(list(east.values()))[:, 1]
    ax.scatter(x, y, marker='s', c=y, s=10, cmap='autumn', alpha=.66)

    ax.set_xlabel(f"{targets[0].upper()} 1-grams Rank", )
    ax.set_ylabel(f"{targets[1].upper()} 1-grams Rank", rotation=270, labelpad=20)
    ax.tick_params(axis='x', rotation=-45)
    ax.tick_params(axis='y', rotation=-45)
    ax.set_xlim(rmax, 1)
    ax.set_ylim(rmax, 1)

    plt.savefig(savepath, dpi=300)


def plot_freq_dist(df, savepath=None, label=None, rank=False):

    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif'
    })

    plt.figure(figsize=(10, 10))

    ax = plt.gca()

    # ax.scatter(np.log10(df['count']),np.log10(df['freq'].values/df['freq'].sum()), s=2, label=label)
    if rank:
        df['rank'] = df['count'].rank(ascending=True)
        ax.plot(df['rank'], df['freq'].values / df['freq'].sum(), marker='.', label=label, linewidth=0)
        ax.set_xlabel('Rank $_r$')
        ax.set_ylabel('Normalized Frequency $p_r$')

    else:
        ax.plot(df['count'], df['freq'].values / df['freq'].sum(), marker='.', label=label, linewidth=0)
        ax.set_xlabel('Count $_k$')
        ax.set_ylabel('Normalized Frequency $p_k$')

    ax.loglog()

    ax.grid(alpha=.3)

    plt.subplots_adjust(top=0.97, right=0.97)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.1)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.1)


def plot_freq_to_zipf(filename2freqdist, savepath, langlist=consts.reachedtop10, fit=False):
    """Pivot freq distributions into raw observations"""

    # make list of language tags for use in labels, etc.
    keylist = [(x.split('/')[-1].split('_')[0], x) for x in filename2freqdist.keys() if
               any(xs == x.split('/')[-1].split('_')[0] for xs in langlist)]

    f, a = plt.subplots(1, 1, figsize=(12, 8))

    mods = []
    for k in keylist:

        df = filename2freqdist[k[1]].sort_values('count', ascending=False)

        y = df['count'].repeat(df['freq']).values  # convert to raw counts

        x = np.arange(len(y))
        # a.plot(x,y/y.sum(),label=k[0],alpha=.25)#,color=target_lans_colors_2[k])

        ylog = np.log10(y)
        xlog = np.log10(x)
        if fit:
            ylog2 = ylog[(xlog > 2.5) & (xlog < 4)]
            xlog2 = xlog[(xlog > 2.5) & (xlog < 4)]

            fit, err, *rest = np.polyfit(xlog2, ylog2, 1, full=True)
            # fit_fun = np.poly1d(fit)
            mods.append((k, fit[0]))
            a.plot(x, y / y.sum(), label=f'{k[0]} $\\rightarrow$ {fit[0]:.3f} $({err[0]:.3f})$', alpha=.25)

        else:
            a.plot(x, y / y.sum(), label=f'{k[0]}', alpha=.25)

        a.annotate(k[0], (max(x), min(y)), fontsize=22)

    a.loglog()
    a.legend()
    a.set_xlabel('Rank')
    a.set_ylabel('Normalized Frequency')

    plt.subplots_adjust(top=0.97, right=0.97)
    print(savepath)
    plt.savefig(str(savepath) + '.png', dpi=300, bbox_inches='tight', pad_inches=.1)
    plt.savefig(str(savepath) + '.pdf', bbox_inches='tight', pad_inches=.1)


def plot_zipf_grid(filename2freqdist, savepath, langlist, supported_languages, emoji_dfs=None):

    first_n_langs = 5

    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif'
    })

    # print(filename2freqdist)

    print(f'Lang list {langlist}')

    h, w = 12, len(filename2freqdist) * 2.66

    f, al = plt.subplots(len(filename2freqdist), len(langlist), figsize=(h, w))

    # f, al = plt.subplots(len(filename2freqdist), len(langlist), figsize=(12, 8))

    print(emoji_dfs)

    if emoji_dfs:
        emoji_dict_list = list(emoji_dfs.items())

    for i, k in enumerate(filename2freqdist.items()):  # iterate through Ngrams dicts (tarball_name2dict)
        # print(f'Our k {k}')
        new_k = {key.split('/')[-1].split('_')[0]: value for key, value in k[1].items()}

        if emoji_dfs:
            new_emoji = {key.split('/')[-1].split('_')[0]: value for key, value in emoji_dict_list[i][1].items()}

        # for j, entry in enumerate(k[1].items()): # iterate through language dicts (filename2counts)
        for j, lang in enumerate(langlist):

            entry = new_k[lang]

            print(f'Our coords {i},{j}')
            a = al[i, j] if len(al.shape) > 1 else al[j]  # account for single or multi languages

            y_rt = np.array(sorted(entry[0] - entry[1], reverse=True))  # grab all count array
            y_rt = y_rt[y_rt > 0]
            y_pure = np.array(sorted(entry[1], reverse=True))  # grab pure count array
            y_pure = y_pure[y_pure > 0]

            a.plot(np.log10(np.arange(len(y_pure))),
                   np.log10(np.array(y_pure) / sum(y_pure)),
                   label='Pure T',
                   linestyle='-',
                   color='k',
                   alpha=.75)

            a.plot(np.log10(np.arange(len(y_rt))),
                   np.log10(np.array(y_rt) / sum(y_rt)),
                   label='RTs',
                   linestyle='-.',
                   color='dimgrey',
                   alpha=.75)

            if emoji_dfs:
                emoji_entry = new_emoji[lang]
                y_emoji = sorted(emoji_entry[0], reverse=True)

                a.plot(np.log10(np.arange(len(y_emoji))),
                       np.log10(np.array(y_emoji) / sum(y_emoji)),
                       label='emoji',
                       linestyle=':',
                       color='dimgrey',
                       alpha=.75)

            if i == 0:
                # this_lang =  entry[0].split('/')[-1].split('_')[0]
                print(f'This lang {lang}')
                a.set_title(supported_languages[lang], color='dimgrey')

            if j == 0:
                a.set_ylabel(f'{i + 1}grams', color='dimgrey')

            # a.set_xlim((-10000, 10e7))
            # a.set_ylim((10e-10, 0))
            # a.set_xticks((10 ** 1, 10 ** 3, 10 ** 5, 10 ** 7))
            a.set_ylim((-8, 1))
            a.grid(alpha=.5, linestyle='dashed')

    handles = [
        Line2D([0], [0], color='dimgrey', lw=4, linestyle='-', label='Tweets'),
        Line2D([0], [0], color='dimgrey', lw=4, linestyle='--', label='Retweets')
    ]

    if emoji_dfs: handles.append(Line2D([0], [0], color='dimgrey', lw=4, linestyle=':', label='Emojis'))

    f.legend(
        handles=handles,
        bbox_to_anchor=(.75, 0.06), ncol=3,
        fontsize=18, frameon=False
    )

    f.tight_layout()

    f.text(0.5, .04, 'Rank', ha='center')
    f.text(.04, .5, 'Normalized Frequency', va='center', rotation='vertical')

    f.subplots_adjust(left=0.13)
    f.subplots_adjust(bottom=0.1)

    plt.subplots_adjust(top=0.97, right=.9)
    savepath = savepath / k[0].split('/')[-1].split('.')[0]
    print(f'Saving to {savepath} .pdf/png')
    plt.savefig(str(savepath) + '.png', dpi=300, bbox_inches='tight', pad_inches=.1)
    plt.savefig(str(savepath) + '.pdf', bbox_inches='tight', pad_inches=.1)


def plot_freq_to_zipf_grid(filename2freqdist, savepath, langlist, supported_languages):

    first_n_langs = 5

    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif'
    })

    # make list of language tags for use in labels, etc.

    f, al = plt.subplots(len(filename2freqdist), first_n_langs, figsize=(12, 8))  # , sharex=True, sharey=True)

    taglist = ['en', 'ja', 'es', 'pt', 'und']  # set order of langauges

    mods = []
    for i, k in enumerate(filename2freqdist):  # iterate through

        keylist = [(x.split('/')[-1].split('_')[0], x) for x in k.keys() if
                   any(xs == x.split('/')[-1].split('_')[0] for xs in langlist[:first_n_langs])]

        keylist = sorted(keylist, key=lambda x: taglist.index(x[0]))

        print([x[0] for x in keylist])

        for j, entry in enumerate(keylist):
            print(f'this lang: {entry[1]}')

            a = al[i, j]

            df = k[entry[1]].sort_values('count', ascending=False)
            y = df['count'].repeat(df['freq']).values  # y convert to raw counts
            x = np.arange(1, len(y) + 1)

            a.loglog(x, y / y.sum(), alpha=.75, color='dimgrey')

            if i == 0:
                a.set_title(f'{supported_languages[entry[0]]}', color='dimgrey')

            # a.loglog()
            a.grid(alpha=.5, linestyle='dashed')

            # a.spines['right'].set_visible(False)
            # a.spines['top'].set_visible(False)

            # a.xaxis.set_ticks(np.arange(0, 10e6, 10e2))

            a.set_xlim((-10, 10e7))
            # print(a.)
            a.set_ylim((10e-10, 10e-1))
            a.set_xticks((10 ** 1, 10 ** 3, 10 ** 5, 10 ** 7))

            if j == 0:
                a.set_ylabel(f'{i + 1}grams', color='dimgrey')

            # a.tick_params(axis='x', which='major', pad=0)
            # a.tick_params(axis='y', which='major', pad=0)
            a.tick_params(axis='both', which='major', pad=0)

    f.tight_layout()

    f.text(0.5, .04, 'Rank', ha='center')
    f.text(.04, .5, 'Normalized Frequency', va='center', rotation='vertical')

    f.subplots_adjust(left=0.13)
    f.subplots_adjust(bottom=0.1)
    # plt.subplots_adjust(left=.9)

    plt.subplots_adjust(top=0.97, right=.9)
    savepath = savepath / entry[1].split('/')[0]
    print(savepath)
    plt.savefig(str(savepath) + '.png', dpi=300, bbox_inches='tight', pad_inches=.1)
    plt.savefig(str(savepath) + '.pdf', bbox_inches='tight', pad_inches=.1)


def plot_zipf_grid_lite(filename2freqdist, savepath, langlist, supported_languages, emoji_dfs=None):

    first_n_langs = 5

    plt.rcParams.update({
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        'font.family': 'serif'
    })

    # print(filename2freqdist)

    print(f'Lang list {langlist}')

    h, w = 12, len(filename2freqdist) * 2.66

    f, al = plt.subplots(len(filename2freqdist), len(langlist), figsize=(h, w))

    print('Shape of al', al.shape)

    # if len(al.shape) < 2: al = np.array([i] for i in al)

    print(emoji_dfs)

    if emoji_dfs:
        emoji_dict_list = list(emoji_dfs.items())

    for i, k in enumerate(filename2freqdist.items()):  # iterate through Ngrams dicts (tarball_name2dict)
        # print(f'Our k {k}')
        new_k = {key.split('/')[-1].split('_')[0]: value for key, value in k[1].items()}

        if emoji_dfs:
            new_emoji = {key.split('/')[-1].split('_')[0]: value for key, value in emoji_dict_list[i][1].items()}

        # for j, entry in enumerate(k[1].items()): # iterate through language dicts (filename2counts)
        for j, lang in enumerate(langlist):

            zipf0, zipf1 = new_k[lang]  # plotting just all for testing purposes

            print(f'Our coords {i},{j}.')

            a = al[i, j] if len(al.shape) > 1 else al[j]  # account for single or multi languages

            # print('the entry:', entry)

            # y_rt = np.array(sorted(entry[0] - entry[1], reverse=True))  # grab all count array
            # y_rt = y_rt[y_rt > 0]
            # y_pure = entry[1] # np.array(sorted(entry[1], reverse=True))  # grab pure count array
            # y_pure = # y_pure[y_pure > 0]

            a.plot(zipf0[0],
                   zipf0[1],
                   label='Tweets',
                   ls='-', lw=2, markevery=12, color=consts.other_colors['tweets'],
                   alpha=1)

            a.plot(zipf1[0],
                   zipf1[1],
                   label='Retweets',
                   ls='--', lw=2, markevery=12, color=consts.other_colors['retweets'],
                   alpha=1)

            if emoji_dfs:
                emoji_entry = new_emoji[lang]
                y_emoji = sorted(emoji_entry[0], reverse=True)

                a.loglog(np.arange(len(y_emoji)),
                         np.array(y_emoji) / sum(y_emoji),
                         label='emoji',
                         linestyle=':',
                         color='dimgrey',
                         alpha=.75)

            if i == 0:
                # this_lang =  entry[0].split('/')[-1].split('_')[0]
                print(f'This lang {lang}')
                a.set_title(supported_languages[lang], color='dimgrey')

            if j == 0:
                a.set_ylabel(f'{i + 1}grams', color='dimgrey')

            # a.set_xlim((-100, 10e7))
            a.set_ylim((-8, 0))
            # a.set_xticks((10 ** 1, 10 ** 3, 10 ** 5, 10 ** 7))
            a.grid(alpha=.5, linestyle='dashed')

    handles = [
        Line2D([0], [0], color=consts.other_colors['tweets'], lw=4, linestyle='-', label='Tweets'),
        Line2D([0], [0], color=consts.other_colors['retweets'], lw=4, linestyle='--', label='Retweets')
    ]

    if emoji_dfs: handles.append(Line2D([0], [0], color='dimgrey', lw=4, linestyle=':', label='Emojis'))

    f.legend(
        handles=handles,
        # bbox_to_anchor=(.75, 0.06), ncol=3,
        # fontsize=18, frameon=False
    )

    f.tight_layout()

    f.text(0.5, .04, 'Rank', ha='center')
    f.text(.04, .5, 'Normalized Frequency', va='center', rotation='vertical')

    # f.subplots_adjust(left=0.13)
    # f.subplots_adjust(bottom=0.1)

    plt.subplots_adjust(top=0.97, right=.9)
    savepath = savepath / k[0].split('/')[-1].split('.')[0]
    print(f'Saving to {savepath} _lite.pdf/png')
    plt.savefig(str(savepath) + '_lite.png', dpi=300, bbox_inches='tight', pad_inches=.1)
    plt.savefig(str(savepath) + '_lite.pdf', bbox_inches='tight', pad_inches=.1)


def plot_zipf_assorted_lite(filename2freqdist, savepath, langlist, supported_languages, emoji_dfs=None,
                            other_dates=None):

    first_n_langs = 5

    plt.rcParams.update({
        'axes.titlesize': 14,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12
    })

    print(f'Lang list {langlist}')

    h, w = 12, len(filename2freqdist) * 2.66

    w = w + 2 if other_dates else w

    f = plt.figure(figsize=(h, w), constrained_layout=True)

    grid_h = len(filename2freqdist)
    grid_w = len(langlist) + 2 if other_dates else len(langlist)

    gs = GridSpec(grid_h, grid_w, figure=f)

    print(emoji_dfs)

    if emoji_dfs:
        emoji_dict_list = list(emoji_dfs.items())

    for i, k in enumerate(filename2freqdist.items()):  # iterate through Ngrams dicts (tarball_name2dict)
        # print(f'Our k {k}')
        new_k = {key.split('/')[-1].split('_')[0]: value for key, value in k[1].items()}

        if emoji_dfs:
            new_emoji = {key.split('/')[-1].split('_')[0]: value for key, value in emoji_dict_list[i][1].items()}

        # for j, entry in enumerate(k[1].items()): # iterate through language dicts (filename2counts)
        for j, lang in enumerate(langlist):

            zipf0, zipf1 = new_k[lang]  # plotting just all for testing purposes

            print(f'Our coords {i},{j}.')
            a = f.add_subplot(gs[i, j])

            a.plot(zipf0[0],
                   zipf0[1],
                   label='Tweets',
                   ls='-', lw=2, markevery=12, color=consts.other_colors['tweets'],
                   alpha=1)

            a.plot(zipf1[0],
                   zipf1[1],
                   label='Retweets',
                   ls='--', lw=2, markevery=12, color=consts.other_colors['retweets'],
                   alpha=1)

            if emoji_dfs:
                emoji_x, emoji_y = new_emoji[lang][0]

                a.plot(emoji_x,
                       emoji_y,
                       label='emoji',
                       linestyle=':',
                       color=consts.other_colors['emojis'],
                       alpha=.75)

            if i == 0:
                # this_lang =  entry[0].split('/')[-1].split('_')[0]
                print(f'This lang {lang}')
                a.set_title(supported_languages[lang], color='dimgrey')

            if j == 0:
                a.set_ylabel(f'{i + 1}grams', color='dimgrey')

            # a.set_xlim((-100, 10e7))
            a.set_ylim((-8, 0))
            # a.set_xticks((10 ** 1, 10 ** 3, 10 ** 5, 10 ** 7))
            a.grid(alpha=.5, linestyle='dashed')

        if other_dates:
            a = gs[i, j + 1:]
            for fname, fdist in other_dates.items():
                x_od, y_od = fdist['en'][0]
                a.plot(x_od, y_od)

    handles = [
        Line2D([0], [0], color=consts.other_colors['tweets'], lw=4, linestyle='-', label='Tweets'),
        Line2D([0], [0], color=consts.other_colors['retweets'], lw=4, linestyle='--', label='Retweets')
    ]

    if emoji_dfs: handles.append(Line2D([0], [0], color='dimgrey', lw=4, linestyle=':', label='Emojis'))

    f.legend(
        handles=handles,
        # bbox_to_anchor=(.75, 0.06), ncol=3,
        # fontsize=18, frameon=False
    )

    f.tight_layout()

    f.text(0.5, .04, 'Rank', ha='center')
    f.text(.04, .5, 'Normalized Frequency', va='center', rotation='vertical')

    # f.subplots_adjust(left=0.13)
    # f.subplots_adjust(bottom=0.1)

    plt.subplots_adjust(top=0.97, right=.99, left=.1)
    savepath = savepath / k[0].split('/')[-1].split('.')[0]
    print(f'Saving to {savepath} _lite_panel.pdf/png')
    plt.savefig(str(savepath) + '_lite_panel.png', dpi=300, bbox_inches='tight', pad_inches=.1)
    plt.savefig(str(savepath) + '_lite_panel.pdf', bbox_inches='tight', pad_inches=.1)


def plot_ngrams_grid(savepath, ngrams):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        #'font.family': 'Times New Roman',
    })

    window_size = 7
    start_date = ngrams[0].index[0]
    end_date = ngrams[0].index[-1]
    diff = end_date - start_date

    if diff.days < 365:
        date_format = '%m\n%Y'
        major_locator = mdates.MonthLocator(range(1, int(np.ceil(diff.days/30) + 1)))
        minor_locator = mdates.AutoDateLocator()

    else:
        date_format = '%Y'
        major_locator = mdates.YearLocator(3)
        minor_locator = mdates.YearLocator()

    fig, axes = plt.subplots(figsize=(10, 12), sharey=True, ncols=3, nrows=5)
    axes = axes.flatten()
    for i, ax in enumerate(axes):
        df = ngrams[i].dropna()
        df['freq'] = df['freq'].apply(np.log10)
        ax.set_title(df.index.name)
        try:
            ax.plot(
                df['freq'],
                marker='o',
                ms=3,
                color='lightblue',
                mfc='lightblue',
                mec='lightblue',
                lw=0,
            )

            ts = df['freq'].rolling(window_size, center=True).mean()
            ax.plot(
                ts,
                color='k',
                lw=1,
            )

            ax.plot(
                df['freq'].idxmax(), df['freq'].max(),
                'o', ms=15, color='orangered', alpha=0.5
            )

            twinx = ax.twinx()
            date = df['freq'].idxmax().strftime('%Y/%m/%d')
            twinx.set_ylabel(date, color='orangered', fontsize=12, rotation=270, labelpad=15, alpha=.75)
            twinx.set_yticks([])

        except ValueError as e:
            print(f'Value error for {df.index.name}: {e}.')
            pass

        ax.set_xlim(start_date, end_date)
        ax.xaxis.set_major_locator(major_locator)
        ax.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
        ax.xaxis.set_minor_locator(minor_locator)

        ax.set_ylim(-8, 0)

        if i % 3 == 0:
            ax.text(-0.3, 0.5, r"$\log_{10}$"+"\nRelative\nFrequency", ha='center',
                    verticalalignment='center', transform=ax.transAxes, )
            ax.text(-0.3, 0.1, "Less\nTalked\nAbout\n↓", ha='center',
                    verticalalignment='center', transform=ax.transAxes, color='grey')
            ax.text(-0.3, 0.9, "↑\nMore\nTalked\n About", ha='center',
                    verticalalignment='center', transform=ax.transAxes, color='grey')

        ax.grid(True, which="both", axis='both', alpha=.2, lw=2, linestyle='--')

    plt.tight_layout()
    sns.despine(offset=5)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)


def plot_chart(savepath, ngrams, shading=False):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    window_size = 30
    date_format = '%Y'
    major_locator = mdates.YearLocator(2)
    minor_locator = mdates.YearLocator()

    topics = ngrams['topic'].unique()
    phrases = ngrams['ngram'].unique()
    dates = pd.to_datetime(ngrams['date'].unique())

    fig, axes = plt.subplots(figsize=(8, 10), ncols=2, nrows=len(topics)//2)
    grid = {t: ax for t, ax in zip(topics, axes.flatten())}

    for i, w in enumerate(phrases):
        vals = ngrams[ngrams['ngram'] == w]['val']
        lang = ngrams[ngrams['ngram'] == w]['lang'][0]
        topic = ngrams[ngrams['ngram'] == w]['topic'][0]
        color = ngrams[ngrams['ngram'] == w]['color'][0]
        ax = grid[topic]

        ax.plot(
            vals.idxmin(), vals.min(),
            'o', ms=10, color=color, alpha=0.5
        )
        ax.plot(
            vals.idxmin(), vals.min(),
            'o', ms=2, color='k', alpha=0.5
        )

        if shading:
            ts = vals.resample('W')
            ax.fill_between(
                ts.mean().index,
                y1=ts.max(),
                y2=ts.min(),
                color='lightgrey',
                facecolor='lightgrey',
                edgecolor='lightgrey',
                zorder=0,
            )
        else:
            ax.plot(
                vals,
                color='lightgrey',
                alpha=.8,
                lw=1,
                zorder=0,
            )

        try:
            w = bidialg.get_display(w)
        except UnicodeEncodeError:
            w = str(w, 'utf-8')

        ax.plot(
            vals.rolling(window_size, center=True).mean(),
            lw=1,
            label=f"{w} ({lang})",
            color=color
        )

    labels = 'A B C D E F G H'.split(' ')
    for i, (ax, topic) in enumerate(zip(axes.flatten(), topics)):
        ax.set_ylim(1, 10**6)
        ax.invert_yaxis()
        ax.set_yscale('log')
        ax.yaxis.set_major_locator(
            ticker.LogLocator(base=10, numticks=12)
        )

        ax.set_yticks(
            [1, 10, 10 ** 2, 10 ** 3, 10 ** 4, 10 ** 5, 10 ** 6],
            minor=False
        )
        ax.set_yticklabels(
            ['1', '10', '100', r'$10^3$', r'$10^4$', r'$10^5$', r'$10^6$'],
            minor=False
        )
        ax.yaxis.set_minor_locator(
            ticker.LogLocator(base=10.0, subs=np.arange(.1, 1, step=.1), numticks=30)
        )

        ax.set_xlim(dates[0], None)
        ax.xaxis.set_major_locator(major_locator)
        ax.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
        ax.xaxis.set_minor_locator(minor_locator)

        ax.annotate(
            labels[i], xy=(-.175, 1.05), color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )

        if i % 2 == 0:
            ax.text(
                -0.275, 0.5, r"$n$-gram"+"\nrank\n"+r"$r$", ha='center',
                verticalalignment='center', transform=ax.transAxes
            )
            ax.text(
                -0.275, 0.15, "Less\nTalked\nAbout\n↓", ha='center',
                verticalalignment='center', transform=ax.transAxes, color='grey'
            )
            ax.text(
                -0.275, 0.85, "↑\nMore\nTalked\nAbout", ha='center',
                verticalalignment='center', transform=ax.transAxes, color='grey'
            )

        if topic in ['Fame']:
            ax.legend(
                loc='upper left', bbox_to_anchor=(-.03, 1.2),
                frameon=False, fontsize=10, columnspacing=.5
            )
        elif topic in ['Outbreaks', 'Conflicts', 'Years', 'Periodic']:
            ax.legend(
                loc='upper left', bbox_to_anchor=(-.03, 1.2),
                frameon=False, fontsize=10, columnspacing=.5, ncol=2
            )
        elif topic in ['Sports']:
            ax.legend(
                loc='upper left', bbox_to_anchor=(.4, 1.2),
                frameon=False, fontsize=10, columnspacing=.5, ncol=1
            )
        elif topic in ['Science', 'Movements']:
            ax.legend(
                loc='upper left', bbox_to_anchor=(-.03, 1.2),
                frameon=False, fontsize=10, columnspacing=-4, ncol=2
            )
        elif topic in ['Fame']:
            ax.legend(
                loc='upper left', bbox_to_anchor=(-.03, 1.2),
                frameon=False, fontsize=10, columnspacing=-3.5, ncol=2
            )

    sns.despine(offset=5)
    plt.tight_layout()
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_stats_timeseries(savepath, df):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'axes.facecolor': (0, 0, 0, 0),
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        # 'font.family': 'Times New Roman',
    })

    fig = plt.figure(figsize=(12, 10))
    cols, rows = 3, 3
    gs = fig.add_gridspec(ncols=cols, nrows=rows)
    tags = 'A B C D E F G'.split()

    res = 'M'
    window_size = 30
    ax = fig.add_subplot(gs[0, :])
    for i, n in enumerate(consts.ngrams):
        ts = f'unique_{n}'
        ax.plot(
            df[ts],
            color='lightgrey',
            zorder=0,
        )

        ax.plot(
            df[ts].rolling(window_size, center=True).mean(),
            label=f'Unique {n}',
            color=consts.ngrams_colors[n],
            lw=3,
        )

        ax.plot(
            df[ts].idxmax(), df[ts].max(),
            'o', ms=3, color='k'
        )

        ax.plot(
            df[ts].idxmax(), df[ts].max(),
            'o', ms=15, color=consts.ngrams_colors[n], alpha=0.5
        )

        ax.text(
            df[ts].idxmax(),
            df[ts].max() - 5*10**7,
            df[ts].idxmax().strftime('%Y/%m/%d'),
            ha='center',
            verticalalignment='center',
            color='dimgrey'
        )

        ax.xaxis.set_major_locator(mdates.YearLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        ax.xaxis.set_minor_locator(mdates.MonthLocator())
        ax.set_xlim(df.index[0], df.index[-1])
        ax.grid(True, which="major", axis='both', zorder=0, alpha=.3, linestyle='-')
        ax.ticklabel_format(axis='y', style='sci', useMathText=True)

        ax.annotate(
            tags[0], xy=(-.03, 1.05), color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        stream = fig.add_subplot(gs[1, i])
        d = df.loc['2009':, [f'num_{n}', f'num_{n}_rt', f'num_{n}_no_rt']].resample(res).mean()
        x = d.index
        y = d.values.T

        stream.set_title(n)
        stream.stackplot(
            x,
            y[::-1],
            baseline='sym',
            labels=list(consts.types_colors.keys())[::-1],
            colors=[consts.types_colors[t] for t in list(consts.types_colors.keys())[::-1]],
        )

        stream.set_xlabel("")
        stream.set_xlim(x[0], x[-1])
        stream.set_ylim(-8*10**8, 8*10**8)
        stream.ticklabel_format(axis='y', style='sci', useMathText=True)

        stream.annotate(
            tags[1 + i], xy=(-.1, 1.05), color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        ratio = fig.add_subplot(gs[2, i])
        y = y[1:] / y[0]

        ratio.stackplot(
            x,
            y,
            baseline='zero',
            labels=list(consts.types_colors.keys())[1:],
            colors=[consts.types_colors[t] for t in list(consts.types_colors.keys())[1:]],
        )

        ratio.set_xlabel("")
        ratio.set_xlim(x[0], x[-1])
        ratio.set_ylim(0, 1)
        ratio.tick_params(axis='x', rotation=45)
        ratio.xaxis.set_tick_params(rotation=45)

        ratio.annotate(
            tags[4 + i], xy=(-.1, 1.05), color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        if i % 3 != 0:
            stream.set_yticklabels([])
            ratio.set_yticklabels([])

        if i == 0:
            patchs, labels = stream.get_legend_handles_labels()
            stream.legend(patchs[::-1], labels[::-1], frameon=False, loc='upper left')

    patchs, labels = ax.get_legend_handles_labels()
    ax.legend(patchs[::-1], labels[::-1], frameon=False, loc='upper left')

    sns.despine(offset=5)
    plt.tight_layout()
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_stats_dists(savepath, df):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'axes.facecolor': (0, 0, 0, 0),
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
        # 'font.family': 'Times New Roman',
    })

    def dist(ax, t, c='grey', ll='', xlim=(0, None), ylim=(0, None)):
        sns.distplot(
            df[t],
            ax=ax,
            kde=True,
            bins=100,
            color=c,
            kde_kws={"lw": 3, "label": "KDE"},
        )

        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.axvline(df[t].median(), ls='--', lw=2, color='k', label='median')
        ax.ticklabel_format(axis='both', style='sci', useMathText=True)
        ax.legend().remove()
        ax.set_xlabel('')
        ax.set_title(ll)

    fig = plt.figure(figsize=(10, 12))
    cols, rows = 3, 4
    gs = fig.add_gridspec(ncols=cols, nrows=rows)
    tags = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'.split(' ')
    tag_pos = (-.25, 1.075)

    for i, n in enumerate(consts.ngrams):

        unique = fig.add_subplot(gs[0, i])
        dist(
            ax=unique,
            t=f'unique_{n}',
            c=consts.ngrams_colors[n],
            ll=f'{n}\n\nUnique',
            xlim=(0, 3 * 10**7) if i == 0 else (0, 2 * 10**8),
            ylim=(0, 2*10 ** -7) if i == 0 else (0, 3*10 ** -8),
        )
        unique.annotate(
            tags[i], xy=tag_pos, color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        at = fig.add_subplot(gs[1, i])
        dist(
            ax=at,
            t=f'num_{n}',
            c=consts.types_colors['AT'],
            ll=f'ATs',
            xlim=(0, 8 * 10 ** 8),
            ylim=(0, 5 * 10 ** -9),
        )
        at.annotate(
            tags[i+4], xy=tag_pos, color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        rt = fig.add_subplot(gs[2, i])
        dist(
            ax=rt,
            t=f'num_{n}_rt',
            c=consts.types_colors['RT'],
            ll=f'RTs',
            xlim=(0, 8 * 10 ** 8),
            ylim=(0, 5 * 10 ** -9),
        )
        rt.annotate(
            tags[i+8], xy=tag_pos, color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        ot = fig.add_subplot(gs[3, i])
        dist(
            ax=ot,
            t=f'num_{n}_no_rt',
            c=consts.types_colors['OT'],
            ll=f'OTs',
            xlim=(0, 8 * 10 ** 8),
            ylim=(0, 5 * 10 ** -9),
        )
        ot.annotate(
            tags[i+12], xy=tag_pos, color='k',
            xycoords="axes fraction", fontsize=16, weight='bold',
        )

        if i == 0:
            unique.legend(frameon=False)
            unique.set_ylabel('Density')
            at.set_ylabel('Density')
            ot.set_ylabel('Density')
            rt.set_ylabel('Density')

    sns.despine(offset=5)
    plt.tight_layout()
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_studies(
    savepath,
    pantheon,
    risk_dfs,
    gpr,
    gpr_betas,
    tv_movies_timeseries,
    movie2dub,
    half_med,
    med,
    dec_inc_df
):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    target = 'h0.5'
    cutoff = 0.9
    window_size = 30
    date_format = '%Y'
    vmin, vmax = 0, 6
    major_locator = mdates.YearLocator(2)
    minor_locator = mdates.YearLocator()

    fig = plt.figure(figsize=(8, 10))
    cols, rows = 6, 18
    gs = fig.add_gridspec(ncols=cols, nrows=rows)
    ax = fig.add_subplot(gs[:4, :3])
    ageax = fig.add_subplot(gs[:4, 3:])
    moviesax = fig.add_subplot(gs[5:9, :3])
    tvax = fig.add_subplot(gs[5:9, 3:])
    hlax = fig.add_subplot(gs[10:14, :3])
    peakax = fig.add_subplot(gs[10:14, 3:])
    gprax1 = fig.add_subplot(gs[-3, :4])
    gprax2 = fig.add_subplot(gs[-2, :4])
    gprax3 = fig.add_subplot(gs[-1, :4])
    gprinset = fig.add_subplot(gs[-3:, -2:])

    sns.despine(offset=5)

    ## Insets
    x1, x2, y1 = 0.05, .65, .8
    w, h = 0.25, 0.25
    hlbefore = inset_axes(
        hlax, width="100%", height="100%",
        bbox_to_anchor=(x1, y1, w, h),
        bbox_transform=hlax.transAxes,
    )
    hlafter = inset_axes(
        hlax, width="100%", height="100%",
        bbox_to_anchor=(.72, y1, w, h),
        bbox_transform=hlax.transAxes,
    )

    axin = inset_axes(
        peakax, width="100%", height="100%",
        bbox_to_anchor=(x2, y1-.05, w+.05, h+.05),
        bbox_transform=peakax.transAxes,
    )

    n = 5  # number of individuals to consider from each category
    occupations = ['singer', 'actor', 'soccer player', 'politician', 'businessperson']
    industry = 'film and theatre'

    cc = 0
    for i, occ in enumerate(occupations):
        topn = pantheon[pantheon['occupation'] == occ]
        topn = topn.pivot(index='date', columns='ngram', values='val')
        vals = topn.loc[:, (topn != vmax).any()]
        topn.index = pd.to_datetime(topn.index)
        print(f'{occ.upper()}: {topn.shape[1]} individuals')
        cc += topn.shape[1]

        topn = topn.apply(
            lambda x: pd.Series(
                x.sort_values(ascending=True).iloc[:n].values,
                index=[f'top{i}' for i in range(1, n + 1)]
            ), axis=1
        )

        ax.plot(
            topn.mean(axis=1).rolling(window_size, center=True).mean(),
            lw=1,
            label=f'{occ.capitalize()}s',
        )
        ax.set_xlim(topn.index[0], topn.index[-1])

    print(f'TOTAL: {cc}')

    ax.set_ylim(2, 6)
    ax.set_yticks(np.arange(2, 7))
    ax.invert_yaxis()
    ax.xaxis.set_major_locator(major_locator)
    ax.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
    ax.xaxis.set_minor_locator(minor_locator)

    ax.legend(
        loc='upper left', bbox_to_anchor=(-.03, 1.15),
        frameon=False, fontsize=10, columnspacing=0, ncol=2
    )

    vals = pantheon[pantheon['industry'] == industry]
    print(f'{industry.upper()}: {len(vals.ngram.unique())} individuals')
    vals = vals.groupby('age', as_index=False)['val'].min()

    sns.kdeplot(
        vals.age,
        vals.val,
        shade=True,
        shade_lowest=False,
        cut=True,
        label=f'{industry.capitalize()}',
        cmap='viridis_r',
        ax=ageax,
    )

    ageax.set_ylim(0, 6)
    ageax.set_yticks(np.arange(0, 7))
    ageax.invert_yaxis()
    ageax.set_xlim(10, 120)
    ageax.legend(
        loc='lower left', ncol=1,
        frameon=False, fontsize=10, columnspacing=.5
    )

    ax.set_ylabel(r'$\log_{10}~\langle r \rangle$')
    ageax.set_ylabel(r"$\log_{10}~r_{\mathrm{min}}$")
    ageax.set_xlabel(f"Years from birth")

    phrases = tv_movies_timeseries['ngram'].unique()
    dates = pd.to_datetime(tv_movies_timeseries['date'].unique())
    for i, w in enumerate(phrases):
        vals = tv_movies_timeseries[tv_movies_timeseries['ngram'] == w]['val']
        topic = tv_movies_timeseries[tv_movies_timeseries['ngram'] == w]['topic'][0]
        color = tv_movies_timeseries[tv_movies_timeseries['ngram'] == w]['color'][0]

        if topic == 'tv':
            a = tvax
        else:
            a = moviesax

        a.plot(
            vals.rolling(window_size, center=True).mean(),
            lw=1,
            label=w,
            color=color
        )

    for a in [tvax, moviesax]:
        a.set_ylim(2, 6)
        a.set_yticks(np.arange(2, 7))
        a.invert_yaxis()
        a.set_xlim(dates[0], None)
        a.xaxis.set_major_locator(major_locator)
        a.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
        a.xaxis.set_minor_locator(minor_locator)
        a.set_ylabel(r'$\log_{10}~\langle r \rangle$')

    moviesax.legend(
        loc='upper left', bbox_to_anchor=(-.03, 1.15),
        frameon=False, fontsize=10, columnspacing=.5, ncol=2
    )

    tvax.legend(
        loc='upper left', bbox_to_anchor=(-.03, 1.05),
        frameon=False, fontsize=10, columnspacing=.5, ncol=2
    )

    for key, d in movie2dub.items():
        hlax.plot(
            d,
            alpha=.25,
            color='lightgrey'
        )

    hlax.plot(
        np.arange(-half_med - 1, half_med),
        med,
        color='k',
        linewidth=2,
        alpha=.75
    )
    hlax.plot(
        np.arange(-20, -half_med),
        np.repeat(.5, 20 - half_med),
        linewidth=2,
        linestyle=':',
        color='k',
        alpha=.75
    )
    hlax.plot(
        np.arange(half_med - 1, 20),
        np.repeat(.5, (20 - half_med) + 1),
        linewidth=2,
        linestyle=':',
        color='k',
        alpha=.75
    )

    hlax.set_xlabel(r'Days from $f_{\mathrm{max}}$')
    hlax.set_ylabel(r'$f_t~/~f_{\mathrm{max}}$')
    hlax.set_ylim(.4, 1)
    hlax.set_yticks([.4, .6, .8, 1])
    hlax.set_yticklabels([".4", ".6", ".8", "1"])
    hlax.set_xlim(-20, 20)
    hlax.axvline(0, color='r', ls='--')

    hlbefore.hist(
        dec_inc_df['d0.5'],
        bins=np.arange(0, 25),
        edgecolor='C0',
        color='C0',
    )
    hlbefore.set_xlim(0, 14)
    hlbefore.set_xticks([0, 7, 14])
    hlbefore.set_xticklabels([0, 7, 14], fontsize=10)
    hlbefore.set_title(r'$f_{.5} \rightarrow f_{\mathrm{max}}$', fontsize=12)
    hlbefore.set_xlabel('Days', fontsize=10)
    hlbefore.set_yticks([])
    hlbefore.axes.get_yaxis().set_visible(False)

    hlafter.hist(
        dec_inc_df['h0.5'],
        bins=np.arange(0, 25),
        edgecolor='C1',
        color='C1',
    )
    hlafter.set_xlim(0, 14)
    hlafter.set_xticks([0, 7, 14])
    hlafter.set_xticklabels([0, 7, 14], fontsize=10)
    hlafter.set_title(r'$f_{\mathrm{max}} \rightarrow f_{.5}$', fontsize=12)
    hlafter.set_xlabel('Days', fontsize=10)
    hlafter.set_yticks([])
    hlafter.axes.get_yaxis().set_visible(False)

    data = dec_inc_df[(dec_inc_df[target] < dec_inc_df[target].quantile(cutoff)) & (
            dec_inc_df.revenue < dec_inc_df.revenue.quantile(cutoff))]

    peakax = sns.kdeplot(
        data['revenue']/10**8,
        data[target],
        cut=True,
        shade_lowest=False,
        shade=True,
        cmap='magma_r',
        ax=peakax
    )

    peakax.set_ylabel('Halflife (days)')
    peakax.set_xlabel('Revenue (100 million dollars)')
    peakax.set_ylim(0, 13)
    peakax.set_yticks(np.arange(0, 13, 3))
    peakax.set_xlim(.25, 5)

    axin.hist(
        dec_inc_df['peak_error'],
        bins=np.arange(-14, 28),
        density=False,
        edgecolor='k',
        color='k',
    )
    axin.set_xticks([-14, 0, 14, 28])
    axin.set_xticklabels(['-14', '0', '14', '28'], fontsize=10)
    axin.set_yticks([])
    axin.axes.get_yaxis().set_visible(False)
    axin.set_title('$d_{\mathrm{max}} - d_{0}$', fontsize=12)
    axin.set_ylabel(r'$N_{\mathrm{movies}}$', fontsize=12)
    axin.set_xlabel('Days', fontsize=10)

    gpr_words_of_interest = ['rebellion', 'crackdown']
    for word, color, gprax in zip(gpr_words_of_interest, ['C0', 'C1'], [gprax1, gprax2]):
        gprax.plot(
            risk_dfs[word],
            color=color
        )
        gprax.axhline(0, color='r', ls='--', zorder=0, lw=.75)
        gprax.set_xlim('2010', '2019')

        h = gprinset.hist(
            gpr_betas[word],
            bins='sqrt',
            histtype='step',
            edgecolor=color,
            density=True,
        )

        gprax.set_ylim(-2, 2)
        gprax.xaxis.set_major_locator(major_locator)
        gprax.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
        gprax.xaxis.set_minor_locator(minor_locator)
        gprax.set_xticklabels([])
        gprax.set_yticks([-2, 0, 2])
        gprax.set_yticklabels(['-2', '', '2'])

    gprax3.plot(gpr, color='darkgrey')
    gprax3.axhline(0, color='r', ls='--', zorder=0, lw=.75)

    gprax3.set_ylim(-2, 2)
    gprax3.set_xlim('2010', '2019')
    gprax3.xaxis.set_major_locator(major_locator)
    gprax3.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
    gprax3.xaxis.set_minor_locator(minor_locator)
    gprax3.set_yticks([-2, 0, 2])
    gprax3.set_yticklabels(['-2', '', '2'])

    gprax2.set_ylabel(r'$d_r$')
    gprinset.axvline(0, color='r', ls='--')
    gprinset.set_xlim(-.75, .75)
    gprinset.set_ylim(0, 4)
    gprinset.set_yticks([])
    gprinset.set_xlabel('$\\beta$')
    gprinset.set_ylabel('$p(\\beta)$')

    lgd = gprax3.legend(
        handles=[
            Line2D([0], [0], color='C0', lw=2, label=gpr_words_of_interest[0]),
            Line2D([0], [0], color='C1', lw=2, label=gpr_words_of_interest[1]),
            Line2D([0], [0], color='darkgrey', lw=2, label='GPR'),
        ],
        loc='lower center', bbox_to_anchor=(.5, -2.75), ncol=3,
        frameon=False, fontsize=10,
    )

    for i, (a, tag) in enumerate(zip(
            [ax, ageax, moviesax, tvax, hlax, peakax, gprax1, gprinset],
            ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    )):
        if tag in ['A', 'B', 'C', 'D', 'E', 'F']:
            xy = (-.2, 1.05)
        elif tag == 'G':
            xy = (-.15, 1.7)
        elif tag == 'H':
            xy = (.05, .9)

        a.annotate(
            tag, xy=xy, color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )

    plt.subplots_adjust(top=0.97, right=0.97, wspace=2, hspace=1)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_pantheon_figures(savepath, pantheon):
    n = 3  # number of individuals to consider from each category
    vmin, vmax = 0, 6
    occupations = pantheon['occupation'].unique()[:-1]
    colors = ['black', 'royalblue', 'darkorange', 'springgreen', 'red']

    cols, rows = 1, len(occupations)
    fig = make_subplots(
        rows=rows, cols=cols,
        subplot_titles=[f'{occ.capitalize()}s' for occ in occupations],
    )

    i = 0
    for r in range(1, rows+1):
        for c in range(1, cols+1):
            occ = occupations[i]
            topn = pantheon[pantheon['occupation'] == occ]
            topn = topn.pivot(index='date', columns='ngram', values='val')
            vals = vals.loc[:, (vals != vmax).any()]
            topn.index = pd.to_datetime(topn.index)
            print(f'{occ.upper()}: {topn.shape[1]} individuals')

            topn_names = topn.apply(
                lambda x: pd.Series(
                    x.sort_values(ascending=True).iloc[:n].index,
                    index=[f'top{i}' for i in range(1, n + 1)]
                ), axis=1
            )

            topn = topn.apply(
                lambda x: pd.Series(
                    x.sort_values(ascending=True).iloc[:n].values,
                    index=[f'top{i}' for i in range(1, n + 1)]
                ), axis=1
            )

            fig.add_trace(
                go.Scatter(
                    x=topn.index,
                    y=topn.loc[:, 'top1'],
                    text=topn_names.loc[:, 'top1'],
                    name='#1',
                    fill='none',
                    mode='lines',
                    xaxis=f'x{i+1}',
                    yaxis=f'y{i+1}',
                    hoveron='points+fills',
                    line=dict(color=colors[0]),
                ),
                row=r, col=c
            )

            for x in range(2, n+1):
                fig.add_trace(
                    go.Scatter(
                        x=topn.index,
                        y=topn.loc[:, f'top{x}'],
                        text=topn_names.loc[:, f'top{x}'],
                        name=f'#{x}',
                        fill='tonexty',
                        mode='lines',
                        xaxis=f'x{i+1}',
                        yaxis=f'y{i+1}',
                        hoveron='points+fills',
                        line=dict(color=colors[x-1]),
                    ),
                    row=r, col=c
                )

            fig.update_xaxes(
                row=r,
                col=c,
                range=['2010-01-01', '2020-05-01'],
                tickformat="%d %b '%y",
            )

            fig.update_yaxes(range=[0, 6], row=r, col=c, autorange="reversed", mirror=True)
            i += 1

    fig.layout.update(dict(
        height=5000,
        font=dict(size=14),
        hovermode='x',
        showlegend=False,
        title='Daily word rank (2grams)',
    ))

    py.plot(fig, filename=savepath)


def plot_pantheon_age(savepath, df):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    log = "$\log_{10}$"
    window_size = 30
    date_format = '%Y'
    major_locator = mdates.YearLocator(2)
    minor_locator = mdates.YearLocator()
    vmin, vmax = 0, 6
    n = 5  # number of individuals to consider from each category

    fig = plt.figure(figsize=(8, 10))
    cols, rows = 2, 3
    gs = fig.add_gridspec(ncols=cols, nrows=rows)
    tax1 = fig.add_subplot(gs[0, 0])
    tax2 = fig.add_subplot(gs[0, 1])
    ageax1 = fig.add_subplot(gs[1, 0])
    ageax2 = fig.add_subplot(gs[1, 1])
    ageax3 = fig.add_subplot(gs[2, 0])
    ageax4 = fig.add_subplot(gs[2, 1])

    l1 = ['actor', 'politician', 'film director', 'soccer player']
    l2 = ['businessperson', 'singer', 'social activist', 'extremist']

    cc = 0
    for i, (occ1, occ2) in enumerate(zip(l1, l2)):
        vals = df[df['occupation'] == occ1]
        vals = vals.pivot(index='date', columns='ngram', values='val')
        vals = vals.loc[:, (vals != vmax).any()]
        vals.index = pd.to_datetime(vals.index)
        print(f'{occ1.upper()}')
        print(vals.min().apply(lambda x: 10**x).describe())
        print()
        cc += vals.shape[1]

        vals = vals.apply(
            lambda x: pd.Series(
                x.sort_values(ascending=True).iloc[:n].values,
                index=[f'top{i}' for i in range(1, n + 1)]
            ), axis=1
        )

        tax1.plot(
            vals.mean(axis=1).rolling(window_size, center=True).mean(),
            lw=1,
            label=f'{occ1.capitalize()}s',
        )
        tax1.set_xlim(vals.index[0], vals.index[-1])

        vals = df[df['occupation'] == occ2]
        vals = vals.pivot(index='date', columns='ngram', values='val')
        vals = vals.loc[:, (vals != vmax).any()]
        vals.index = pd.to_datetime(vals.index)
        print(f'{occ2.upper()}')
        print(vals.min().apply(lambda x: 10**x).describe())
        print()
        cc += vals.shape[1]

        vals = vals.apply(
            lambda x: pd.Series(
                x.sort_values(ascending=True).iloc[:n].values,
                index=[f'top{i}' for i in range(1, n + 1)]
            ), axis=1
        )

        tax2.plot(
            vals.mean(axis=1).rolling(window_size, center=True).mean(),
            lw=1,
            label=f'{occ2.capitalize()}s',
        )
        tax2.set_xlim(vals.index[0], vals.index[-1])

    print(f'TOTAL: {cc}')
    print('-' * 50)

    cc = 0
    groups = ['music', 'government', 'business', 'film and theatre']
    axes = [ageax1, ageax2, ageax3, ageax4]
    for i, (group, ageax) in enumerate(zip(groups, axes)):
        vals = df[df['industry'] == group]
        print(f'{group.upper()}')

        cc += len(vals.ngram.unique())
        vals = vals.groupby('age', as_index=False)['val'].min()

        sns.kdeplot(
            vals.age,
            vals.val,
            shade=True,
            shade_lowest=False,
            cut=True,
            label=f'{group.capitalize()}',
            color=f'C{i}',
            ax=ageax,
        )
        ageax.set_ylabel("")

    print(f'TOTAL: {cc}')
    print('-' * 50)

    for i, (a, tag) in enumerate(zip([tax1, tax2], ['A', 'B'])):
        a.set_ylim(0, 6)
        a.set_yticks(np.arange(0, 7))
        a.invert_yaxis()
        a.xaxis.set_major_locator(major_locator)
        a.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
        a.xaxis.set_minor_locator(minor_locator)

        a.legend(
            loc='upper left', bbox_to_anchor=(0, 1), ncol=2,
            frameon=False, fontsize=10, columnspacing=.5
        )

        a.annotate(
            tag, xy=(-.15, 1.05), color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )

    for i, (a, tag) in enumerate(zip(axes, ['C', 'D', 'E', 'F'])):
        a.set_ylim(0, 6)
        a.set_yticks(np.arange(0, 7))
        a.invert_yaxis()
        a.set_xlim(10, 120)
        a.set_xlabel("")

        if i == 0:
            a.legend(
                loc='upper right', ncol=1,
                frameon=False, fontsize=10, columnspacing=.5
            )
        elif i == 1 or i == 3:
            a.legend(
                loc='lower left', ncol=1,
                frameon=False, fontsize=10, columnspacing=.5
            )
        else:
            a.legend(
                loc='upper left', ncol=1,
                frameon=False, fontsize=10, columnspacing=.5
            )

        a.annotate(
            tag, xy=(-.15, 1.05), color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )

    tax1.set_ylabel(f"Average rank ({log})")
    ageax1.set_ylabel(f"Top rank ({log})")
    ageax3.set_ylabel(f"Top rank ({log})")
    ageax3.set_xlabel(f"Years from birth")
    ageax4.set_xlabel(f"Years from birth")

    sns.despine(offset=5)
    plt.subplots_adjust(top=0.97, right=0.97, wspace=.2, hspace=.3)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_compare(savepath, ngrams):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 12,
        'axes.labelsize': 14,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    formats = ['%b\n%Y',  # ticks are mostly years
               '%b',  # ticks are mostly months
               '%d',  # ticks are mostly days
               '%H:%M',  # hrs
               '%H:%M',  # min
               '%S.%f', ]  # secs
    zero_formats = [''] + formats[:-1]
    zero_formats[3] = '%d-%b'

    converter = mdates.ConciseDateConverter(
        formats=formats,
        zero_formats=zero_formats,
        show_offset=False
    )

    munits.registry[np.datetime64] = converter
    munits.registry[date] = converter
    munits.registry[datetime] = converter

    phrases = ngrams['ngram'].unique()
    dates = pd.to_datetime(ngrams['date'].unique())
    labels = 'A B C D E F G H I J'.split(' ')

    size = 4
    ncols = 2
    nrows = (len(phrases)//2)*size
    fig = plt.figure(figsize=(8, 12))
    gs = fig.add_gridspec(ncols=ncols, nrows=nrows)

    cc = 0
    for r in np.arange(0, nrows, step=size):
        for c in np.arange(ncols):
            w = phrases[cc]
            tax = fig.add_subplot(gs[r, c])
            gax = fig.add_subplot(gs[r+1:r+2, c])
            nax = fig.add_subplot(gs[r+2:r+3, c])

            twitter = ngrams[ngrams['ngram'] == w]['twitter']
            twitter_ot = ngrams[ngrams['ngram'] == w]['twitter_ot']
            gtrends = ngrams[ngrams['ngram'] == w]['gtrends']
            news = ngrams[ngrams['ngram'] == w]['news']

            tax.plot(
                twitter,
                lw=1.5,
                label="Storywrangler (AT)",
                color='k'
            )

            tax.plot(
                twitter_ot,
                lw=1.5,
                label="(OT)",
                color='C0'
            )

            gax.plot(
                gtrends,
                lw=1.5,
                label=f"Google Trends",
                color='C1'
            )

            nax.plot(
                news,
                lw=1.5,
                label=f"Cable TV News",
                color='C2'
            )

            for ax in [tax, gax, nax]:
                ax.set_ylim(0, 100)
                ax.set_yticks([0, 50, 100])
                ax.set_xlim(dates[0], None)

                ax.axhline(50, color='lightgrey', ls='--', zorder=0, lw=.75)

            tax.set_xticklabels([])
            gax.set_xticklabels([])

            tax.set_title(f"`{w}`", y=1.25)
            tax.annotate(
                labels[cc], xy=(-.175, 1.3), color='k', weight='bold',
                xycoords="axes fraction", fontsize=16,
            )

            if c % 2 == 0:
                gax.text(
                    -0.25, .5, r"$n$-gram" + "\ninterest\nover\ntime", ha='center',
                    verticalalignment='center', transform=gax.transAxes,
                )

            if r == 0 and c == 0:
                tax.legend(
                    loc='upper left', bbox_to_anchor=(-.05, 1.6),
                    frameon=False, fontsize=10, ncol=2
                )

                gax.legend(
                    loc='upper left', bbox_to_anchor=(-.05, 1.6),
                    frameon=False, fontsize=10, ncol=1
                )

                nax.legend(
                    loc='upper left', bbox_to_anchor=(-.05, 1.6),
                    frameon=False, fontsize=10, ncol=1
                )

            cc += 1

    sns.despine(offset=5)
    plt.subplots_adjust(top=0.97, right=0.97, wspace=.2, hspace=1)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_perspective(savepath, ngrams, shading=False):
    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 16,
        'axes.labelsize': 14,
        'xtick.labelsize': 12,
        'ytick.labelsize': 12,
        'legend.fontsize': 12,
    })

    window_size = 30
    date_format = '%Y'
    major_locator = mdates.YearLocator(2)
    minor_locator = mdates.YearLocator()

    topics = ngrams['topic'].unique()
    phrases = ngrams['ngram'].unique()
    dates = pd.to_datetime(ngrams['date'].unique())

    fig, axes = plt.subplots(figsize=(8, 10), ncols=2, nrows=len(topics)//2)
    grid = {t: ax for t, ax in zip(topics, axes.flatten())}

    for i, w in enumerate(phrases):
        vals = ngrams[ngrams['ngram'] == w]['val']
        lang = ngrams[ngrams['ngram'] == w]['lang'][0]
        topic = ngrams[ngrams['ngram'] == w]['topic'][0]
        color = ngrams[ngrams['ngram'] == w]['color'][0]
        ax = grid[topic]

        ax.plot(
            vals.idxmin(), vals.min(),
            'o', ms=10, color=color, alpha=0.5
        )
        ax.plot(
            vals.idxmin(), vals.min(),
            'o', ms=2, color='k', alpha=0.5
        )

        if shading:
            ts = vals.resample('W')
            ax.fill_between(
                ts.mean().index,
                y1=ts.max(),
                y2=ts.min(),
                color='lightgrey',
                facecolor='lightgrey',
                edgecolor='lightgrey',
                zorder=0,
            )
        else:
            ax.plot(
                vals,
                color='lightgrey',
                alpha=.8,
                lw=1,
                zorder=0,
            )

        try:
            w = bidialg.get_display(w)
        except UnicodeEncodeError:
            w = str(w, 'utf-8')

        ax.plot(
            vals.rolling(window_size, center=True).mean(),
            lw=1,
            label=f"{w} ({lang})",
            color=color
        )

    labels = 'A B C D E F G H'.split(' ')
    for i, (ax, topic) in enumerate(zip(axes.flatten(), topics)):
        ax.set_ylim(1, 10**6)
        ax.invert_yaxis()
        ax.set_yscale('log')
        ax.yaxis.set_major_locator(
            ticker.LogLocator(base=10, numticks=12)
        )

        ax.set_yticks(
            [1, 10, 10 ** 2, 10 ** 3, 10 ** 4, 10 ** 5, 10 ** 6],
            minor=False
        )
        ax.set_yticklabels(
            ['1', '10', '100', r'$10^3$', r'$10^4$', r'$10^5$', r'$10^6$'],
            minor=False
        )
        ax.yaxis.set_minor_locator(
            ticker.LogLocator(base=10.0, subs=np.arange(.1, 1, step=.1), numticks=30)
        )

        ax.set_xlim(dates[0], None)
        ax.xaxis.set_major_locator(major_locator)
        ax.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
        ax.xaxis.set_minor_locator(minor_locator)

        ax.annotate(
            labels[i], xy=(-.175, 1.05), color='k', weight='bold',
            xycoords="axes fraction", fontsize=16,
        )

        if i % 2 == 0:
            ax.text(
                -0.275, 0.5, r"$n$-gram"+"\nrank\n"+r"$r$", ha='center',
                verticalalignment='center', transform=ax.transAxes
            )
            ax.text(
                -0.275, 0.15, "Less\nTalked\nAbout\n↓", ha='center',
                verticalalignment='center', transform=ax.transAxes, color='grey'
            )
            ax.text(
                -0.275, 0.85, "↑\nMore\nTalked\nAbout", ha='center',
                verticalalignment='center', transform=ax.transAxes, color='grey'
            )

        if i != 0:
            ax.legend(
                loc='upper left', bbox_to_anchor=(-.03, 1.1),
                frameon=False, fontsize=10, columnspacing=-3.5, ncol=1
            )

    sns.despine(offset=5)
    plt.tight_layout()
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_trending_grid(savepath, ngrams):
    plt.rcParams.update({
        'axes.titlesize': 14,
        'axes.labelsize': 11,
        'xtick.labelsize': 9,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    ncols = 3
    fig, axis = plt.subplots(nrows=len(ngrams)//ncols, ncols=ncols, figsize=(9, 12))
    labels = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'.split(' ')

    for i, (date, ax) in enumerate(zip(ngrams.keys(), axis.flatten())):

        ngrams[date].plot.barh(
            ax=ax,
            y='r_rel',
            legend=False,
            width=.75,
            edgecolor='white',
            alpha=.8
        )

        for container in ax.containers:
            for j, (w, bar) in enumerate(zip(ngrams[date].index, container.get_children())):
                x = ngrams[date].loc[w, 'r_rel']

                if x < 0:
                    ax.text(
                        x=.01, y=j+.2, s=w, size=8, color='k',
                        horizontalalignment='left',
                    )
                    bar.set_color('C0')
                else:
                    ax.text(
                        x=-.01, y=j+.2, s=w, size=8, color='k',
                        horizontalalignment='right',
                    )
                    bar.set_color('C1')

        ax.annotate(
            labels[i], xy=(-.1, 1.1), color='k', weight='bold',
            xycoords="axes fraction", fontsize=14,
        )

        ax.set_title(f"{date.strftime('%Y-%m-%d')}")
        ax.set_ylabel('')
        ax.axvline(0, linestyle='-', color='dimgrey')

        ax.set_ylabel('')
        ax.set_yticks([])
        ax.set_ylim((-.5, ngrams[date].shape[0]-.5))

        ax.set_xlim((-.25, .25))
        ax.set_xticks(np.arange(-.2, .25, step=.1))
        ax.set_xticks(np.arange(-.25, .3, step=.05), minor=True)
        ax.set_xticklabels(['-20%', '-10%', '0', '+10%', '+20%'])
        ax.grid(which='both', axis='x', zorder=0, alpha=.5)

        ax.spines["bottom"].set_alpha(0.0)
        ax.spines["right"].set_alpha(0.0)
        ax.spines['left'].set_position('center')

        ax.xaxis.set_ticks_position('top')
        ax.xaxis.set_label_position('top')
        ax.invert_yaxis()

    plt.subplots_adjust(top=0.97, right=0.97, wspace=.2, hspace=.2)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_trending(savepath, ngrams, vol):

    plt.rcParams.update({
        'font.family': 'sans-serif',
        'axes.titlesize': 10,
        'axes.labelsize': 11,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    major_locator = mdates.MonthLocator()
    minor_locator = mdates.DayLocator([10, 20])
    date_format = mdates.ConciseDateFormatter(major_locator, show_offset=False)
    labels = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'.split(' ')
    ncols = len(ngrams)
    minor_colors = [plt.cm.tab20(i) for i in [1, 3, 9, 5, 13, 17, 19]]
    major_colors = [plt.cm.tab20(i) for i in [0, 2, 8, 4, 12, 16, 18]]

    fig = plt.figure(figsize=(11, 8))
    gs = fig.add_gridspec(ncols=ncols, nrows=3)
    vax = fig.add_subplot(gs[-1, :])

    vax.fill_between(
        vol.index, 0, vol['num_1grams'],
        facecolor='dimgrey', label='AT'
    )

    vax.fill_between(
        vol.index, 0, vol['num_1grams_no_rt'],
        facecolor='lightgrey', label='OT'
    )

    vax.set_ylim(0, 6.5*10**8)
    vax.set_yticklabels(['', '100M', '200M', '300M', '400M', '500M', '600M'])
    vax.set_xlim(vol.index[0], vol.index[-1])
    vax.xaxis.set_major_locator(major_locator)
    vax.xaxis.set_major_formatter(date_format)
    vax.xaxis.set_minor_locator(minor_locator)
    vax.spines["top"].set_alpha(0)
    vax.spines["right"].set_alpha(0)
    vax.spines["left"].set_alpha(0)
    vax.grid(which='major', axis='y')
    vax.legend(
        loc='upper left', bbox_to_anchor=(.05, .9),
        frameon=False, fontsize=10,
    )

    for i, (date, cc) in enumerate(zip(ngrams.keys(), minor_colors)):
        ax = fig.add_subplot(gs[:-1, i])
        ngrams[date].plot.barh(
            ax=ax,
            y='r_rel',
            color=cc,
            legend=False,
            width=.9,
            edgecolor='white',
            alpha=.9
        )

        for container in ax.containers:
            for j, (w, bar) in enumerate(zip(ngrams[date].index, container.get_children())):
                ax.text(
                    x=0, y=j+.2, s=w, size=8, color='grey',
                    horizontalalignment='center', weight='bold'
                )

                if i == 0:
                    ax.text(
                        x=-.22, y=j + .2, s=f"{j+1}", size=9, color='grey',
                        horizontalalignment='right', weight='bold',
                    )

        ax.annotate(
            labels[i], xy=(-.15, 1.06), color='k', weight='bold',
            xycoords="axes fraction", fontsize=14,
        )

        ax.set_xlabel(
            f"{date.strftime('%Y-%m-%d')}",
            bbox=dict(facecolor='none', edgecolor=major_colors[i]),
            color=major_colors[i], ha='center', labelpad=10
        )

        if i == 0:
            ax.yaxis.set_label_position("left")
            ax.set_ylabel(
                r'Rank of narratively trending $n$-gram',
                color='k', labelpad=95,
            )
        else:
            ax.set_ylabel('')

        ax.set_yticks([])
        ax.set_ylim((-.5, ngrams[date].shape[0]-.5))

        ax.set_xlim((-.15, .15))
        ax.set_xticks([-.1, 0, .1])
        ax.set_xticks([-.15, -.1, -.05, 0, .05, .1, .15], minor=True)
        ax.set_xticklabels(['-10%', '0', '+10%'])
        ax.grid(which='both', axis='x', zorder=0, alpha=.5)

        ax.spines["bottom"].set_alpha(0)
        ax.spines["right"].set_alpha(0)
        ax.spines['left'].set_alpha(0)
        ax.spines['left'].set_position('center')

        ax.xaxis.set_ticks_position('top')
        ax.xaxis.set_label_position('bottom')
        ax.invert_yaxis()

        ax.set_title(r"$ \log_{10} \ R^{\mathsf{rel}}_{\tau,t,\ell}$")

        vax.axvspan(
            date-timedelta(days=1),
            date+timedelta(days=1),
            color=major_colors[i],
            alpha=.5
        )

    vax.annotate(
        labels[i+1], xy=(-.075, 1), color='k', weight='bold',
        xycoords="axes fraction", fontsize=14,
    )
    vax.set_ylabel(r'$\sum_{\tau \in \mathcal{D}_{t,\ell;n}} f_{\tau,t,\ell}$')

    plt.subplots_adjust(top=0.97, right=0.97, wspace=.2, hspace=.2)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_topics(savepath, ngrams):

    plt.rcParams.update({
        'axes.titlesize': 11,
        'axes.labelsize': 11,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    g = sns.relplot(
        data=ngrams,
        x="ref", y="soi", hue="delta",
        palette="coolwarm", edgecolor=".7",
        height=10, sizes=(50, 150),
    )

    g.ax.grid(which='major', axis='both', zorder=0, alpha=.3)
    g.set(xlabel="", ylabel="", aspect="equal")
    g.despine(left=True, bottom=True)
    g.ax.margins(.02)
    g.ax.invert_yaxis()
    for label in g.ax.get_xticklabels():
        label.set_rotation(90)

    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_risk(savepath, ngrams):

    plt.rcParams.update({
        'font.size': 10,
        'axes.titlesize': 12,
        'axes.labelsize': 14,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    date_format = '%Y'
    major_locator = mdates.YearLocator(2)
    minor_locator = mdates.YearLocator()

    phrases = ngrams['ngram'].unique()
    dates = pd.to_datetime(ngrams.index.unique())
    labels = 'A B C D E F G H I J'.split(' ')

    size = 4
    ncols = 2
    nrows = (len(phrases)//2)*size
    fig = plt.figure(figsize=(8, 10))
    gs = fig.add_gridspec(ncols=ncols, nrows=nrows)

    cc = 0
    for r in np.arange(0, nrows, step=size):
        for c in np.arange(ncols):
            w = phrases[cc]
            tax = fig.add_subplot(gs[r, c])
            gax = fig.add_subplot(gs[r+1:r+2, c])
            nax = fig.add_subplot(gs[r+2:r+3, c])

            twitter = ngrams[ngrams['ngram'] == w]['twitter_at']
            gtrends = ngrams[ngrams['ngram'] == w]['gtrends']
            news = ngrams[ngrams['ngram'] == w]['news']

            tax.plot(
                twitter,
                lw=1.5,
                color='C0'
            )

            gax.plot(
                gtrends,
                lw=1.5,
                color='C1'
            )

            nax.plot(
                news,
                lw=1.5,
                color='C2'
            )

            for ax in [tax, gax, nax]:
                ax.set_ylim(-1, 1)
                ax.set_yticks([-1, 0, 1])
                ax.set_xlim(dates[0], None)
                ax.axhline(0, color='lightgrey', ls='--', zorder=0, lw=.75)

                ax.set_xlim(dates[0], None)
                ax.xaxis.set_major_locator(major_locator)
                ax.xaxis.set_major_formatter(mdates.DateFormatter(date_format))
                ax.xaxis.set_minor_locator(minor_locator)

            tax.set_xticklabels([])
            gax.set_xticklabels([])

            tax.set_title(f"`{w}`", y=1.1)
            tax.annotate(
                labels[cc], xy=(-.175, 1.3), color='k', weight='bold',
                xycoords="axes fraction", fontsize=16,
            )

            if c % 2 == 0:
                tax.text(
                    -0.25, .5, "Twitter", ha='center',
                    verticalalignment='center', transform=tax.transAxes,
                )
                gax.text(
                    -0.25, .5, "Google\nTrends", ha='center',
                    verticalalignment='center', transform=gax.transAxes,
                )
                nax.text(
                    -0.25, .5, "Cable\nNews", ha='center',
                    verticalalignment='center', transform=nax.transAxes,
                )

            cc += 1

    sns.despine(offset=5)
    plt.subplots_adjust(top=0.97, right=0.97, wspace=.2, hspace=.3)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_risk_hists(savepath, words, gpr_samples, pct_s, pct_e):

    plt.rcParams.update({
        'font.size': 14,
        'axes.titlesize': 12,
        'axes.labelsize': 14,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    fig, axes = plt.subplots(3, 3, figsize=(8, 8))

    for i, ax in enumerate(axes.flatten()):

        ax.axvspan(pct_s[i], pct_e[i], linestyle='--', color='k', alpha=0.1, hatch='//')
        if pct_s[i] > 0.:
            ax.hist(gpr_samples['beta'][:, i].numpy(), bins='auto', facecolor='C1', edgecolor='lightgray', density=True)
        elif pct_e[i] < 0.:
            ax.hist(gpr_samples['beta'][:, i].numpy(), bins='auto', facecolor='C0', edgecolor='lightgray', density=True)
        else:
            ax.hist(gpr_samples['beta'][:, i].numpy(), bins='auto', facecolor='darkgrey', edgecolor='lightgray', density=True)

        if i >= 1:
            ax.set_title(words.columns[i - 1])
        else:
            ax.set_title('constant')

        ax.axvline(0., 0., 1., color='r', alpha=0.5, linestyle='--')
        ax.set_xlim(-1, 1)

        if i % 3 != 0:
            ax.set_yticklabels([])
        if i < 6:
            ax.set_xticklabels([])

    axes[1, 0].set_ylabel(r'$p(\beta)$')
    axes[-1, 1].set_xlabel(r'$\beta$ (normalized coefficient)')

    plt.tight_layout()
    sns.despine(offset=5)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)


def plot_risk_models(savepath, models):

    plt.rcParams.update({
        'font.size': 12,
        'axes.titlesize': 10,
        'axes.labelsize': 10,
        'xtick.labelsize': 10,
        'ytick.labelsize': 10,
        'legend.fontsize': 10,
    })

    fig, axes = plt.subplots(models.shape[1]-2, models['model'].nunique(), figsize=(7, 9))

    models['model'].replace(
        {
            'twitter_at': 'Storywrangler (AT)',
            'twitter_ot': 'Storywrangler (OT)',
            'gtrends': 'Google Trends',
            'news': 'Cable TV News',
        },
        inplace=True
    )

    for i, w in enumerate(models.columns[1:-1]):
        axes[i, 0].set_ylabel(f"'{w}'", rotation=0)

        for j, m in enumerate(models['model'].unique()):
            axes[0, j].set_title(m)
            dist = models[models['model'] == m][w]
            pct_s, pct_e = dist.quantile([.1, .9])

            if pct_s > 0:
                color = 'C1'
            elif pct_e < 0:
                color = 'C0'
            else:
                color = 'darkgrey'

            axes[i, j].hist(
                dist,
                bins='auto',
                facecolor=color,
                edgecolor=color,
                density=True,
                fill=True,
                alpha=.75
            )

            axes[i, j].set_yticks([])
            axes[i, j].set_xlim(-.75, .75)
            axes[i, j].axvline(0., 0., 1., color='r', alpha=0.5, linestyle='--')

            if i < 7:
                axes[i, j].set_xticklabels([])

    fig.text(0.55, .06, r'$\beta$ (normalized coefficient)', ha='center')

    sns.despine(left=True)
    plt.subplots_adjust(top=0.97, right=0.97, wspace=.1, hspace=.1)
    plt.savefig(savepath + '.pdf', bbox_inches='tight', pad_inches=.25)
    plt.savefig(savepath + '.png', dpi=300, bbox_inches='tight', pad_inches=.25)
