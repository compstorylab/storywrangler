
import sys
import os
from pathlib import Path
from datetime import datetime, timedelta


import ujson


def clean_realtime_ngrams(path):
    cutoff = datetime.today() - timedelta(days=33)

    for f in path.rglob('*'):
        if f.is_file():
            timestamp = datetime.fromtimestamp(f.stat().st_mtime)
            if timestamp < cutoff:
                f.unlink()

    for d in path.rglob('*'):
        if d.is_dir() and len(os.listdir(d)) == 0:
            d.rmdir()


def main():
    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    clean_realtime_ngrams(Path(config['realtime']))


if __name__ == '__main__':
    main()
