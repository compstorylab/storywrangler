
import sys
import time
from pathlib import Path
import ujson
import tarfile
import shutil
import numpy as np
import pandas as pd

from storywrangler import cli
from storywrangler import fileio
from storywrangler import consts
from storywrangler import eval
from storywrangler import eval_vis
from storywrangler import regexr


def parse_args(args, config):

    parser = cli.parser()

    # optional subparsers
    subparsers = parser.add_subparsers(help='Arguments for specific action.', dest='dtype')
    subparsers.required = False

    calc_parser = subparsers.add_parser(
        'confmat',
        help='Calculate a confusion matrix for a given day'
    )
    calc_parser.add_argument(
        'datapath',
        help='path to a (.tar.gz) file produced by (parser.py)'
    )

    contagion_confmat_parser = subparsers.add_parser(
        'contagion_confmat',
        help='Calculate a confusion matrix for a given day (RT & OT)'
    )
    contagion_confmat_parser.add_argument(
        'datapath',
        help='path to a (.tar.gz) file produced by (parser.py)'
    )

    plot_parser = subparsers.add_parser(
        'plot',
        help='Plot a heatmap comparing Twitter to FastText language labels'
    )
    plot_parser.add_argument(
        'datapath',
        help='path to a directory of (.npy) file(s) (ie. /tmp/mats)'
    )

    plot_contagion_parser = subparsers.add_parser(
        'plot_contagion',
        help='Plot a heatmap comparing Twitter to FastText language labels'
    )
    plot_contagion_parser.add_argument(
        'datapath',
        help='path to a directory of (.npy) file(s) (ie. /tmp/mats)'
    )

    error_parser = subparsers.add_parser(
        'error',
        help='Get an agreement baseline for LID between twitter and fasttext'
    )
    error_parser.add_argument(
        'datapath',
        help='path to a directory of (.npy) file(s) (ie. /tmp/mats)'
    )

    compare_parser = subparsers.add_parser(
        'compare',
        help='Plot a confusion matrix comparing Twitter to FastText language labels'
    )
    compare_parser.add_argument(
        'datapath',
        help='path to a directory of (.npy) file(s) (ie. /tmp/mats)'
    )
    compare_parser.add_argument(
        'year1',
        help='first year',
        type=int,
    )
    compare_parser.add_argument(
        'year2',
        help='second year',
        type=int,
    )

    plot_parser = subparsers.add_parser(
        'test',
        help='Compare a random sample of twitter/fasttext lang. labels'
    )
    plot_parser.add_argument(
        'datapath',
        help='path to a 15-minutes chunk of tweets '
    )

    gain_parser = subparsers.add_parser(
        'gain',
        help='Compare growth of sharing among languages'
    )
    gain_parser.add_argument(
        'datapath',
        help='path to a (.tar.gz) file produced by (parser.py)'
    )

    diagnosis_parser = subparsers.add_parser(
        'diagnosis',
        help='Analyses of 140/280 tweet length on LID'
    )
    diagnosis_parser.add_argument(
        'datapath',
        help='path to a directory of daily compressed ngrams (.tar.gz)'
    )

    aggregate_parser = subparsers.add_parser(
        'aggregate',
        help='aggregate diagnoses'
    )
    aggregate_parser.add_argument(
        'datapath',
        help='path to a directory of daily compressed CSV diagnosis files (csv.gz)'
    )

    tweet_length_parser = subparsers.add_parser(
        'tweet_length',
        help='number of mismatches as a function of tweet-length for the top 10 languages'
    )
    tweet_length_parser.add_argument(
        'datapath',
        help='path to a compressed ngrams file (.tar.gz)'
    )

    agreement_parser = subparsers.add_parser(
        'agreement',
        help='compare the normalized usage of languages on twitter'
    )
    agreement_parser.add_argument(
        'datapath',
        help='path to a compressed diagnosis CSV file (see diagnosis)'
    )

    val_parser = subparsers.add_parser(
        'val',
        help='validation of the contagion ratios based on agreement between fasttext and twitter'
    )
    val_parser.add_argument(
        'languages',
        help='path to a compressed diagnosis CSV file (see languages)'
    )
    val_parser.add_argument(
        'error',
        help='path to a compressed error CSV file (see error)'
    )

    # optional args
    parser.add_argument(
        '-r', '--resolution',
        default='Y',
        help="group days based on a given timescale [i.e. 'D', 'W', 'M', '6M', 'Y']"
    )

    parser.add_argument(
        '-t', '--targets',
        type=str,
        default=consts.reachedtop15,
        help='list of languages to use (i.e. "en es ja")'
    )

    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['plots']),
        help='absolute Path to save figures'
    )

    parser.add_argument(
        '-s', '--score',
        default=float(config['model_threshold']),
        type=float,
        help='confidence score threshold for the language identifier'
    )

    parser.add_argument(
        '-n', '--samples',
        default=1000,
        type=int,
        help='number of sample tweets to get'
    )

    parser.add_argument(
        '-m', '--model',
        default=config['model'],
        help='absolute Path to FastText pre-trained model'
    )

    parser.add_argument(
        '-e', '--emoji',
        action='store_true',
        help='download new codes for emojis from (https://www.unicode.org/) and re-compile regex to parse out ngrams'
    )

    parser.add_argument(
        '-x', '--codes',
        default=Path(config['language_codes']),
        help='path to (.json) language hashtable'
    )

    return parser.parse_args(args)


def main(args=None):

    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)

    if args.emoji:
        regexr.update_parsers(config['twitterlid'])

    eparser = regexr.get_emojis_parser(config['emoji_parser'])
    nparser = regexr.get_ngrams_parser(config['ngrams_parser'])

    lookup_tbl = pd.read_csv(config['supported_languages'], header=0)
    languages_hashtbl = pd.read_json(config['language_hashtbl'], orient='index').to_dict()['new_label']
    language_codes = fileio.load_json(Path(config['language_codes']))

    lookup_tbl = lookup_tbl.replace(languages_hashtbl)
    lookup_tbl.fillna(value='unknown', inplace=True)

    all_languages = np.union1d(lookup_tbl.FastText.values, lookup_tbl.Twitter.values)
    all_languages = sorted(set(all_languages) - set(languages_hashtbl.keys()))

    confmats_out = Path(args.outdir)/'confmats'
    confmats_out.mkdir(parents=True, exist_ok=True)

    out = Path(args.outdir)/'eval'
    out.mkdir(parents=True, exist_ok=True)

    if args.dtype == 'confmat':

        eval.cmat(
            Path(args.datapath),
            confmats_out,
            languages=all_languages,
            hashtbl=languages_hashtbl,
            codes=language_codes
        )

    elif args.dtype == 'contagion_confmat':

        eval.contagion_cmat(
            Path(args.datapath),
            confmats_out,
            languages=all_languages,
            hashtbl=languages_hashtbl,
            codes=language_codes
        )

    elif args.dtype == 'plot':
        targets = [lang for lang in args.targets.split()] if type(args.targets) == str else args.targets

        files = [p for p in Path(args.datapath).rglob('*.npy')]
        if 'all' in targets:
            targets = all_languages

        eval.confmat(
            files,
            confmats_out,
            languages=all_languages,
            targets=targets,
            hashtbl=lookup_tbl.values,
            resolution=args.resolution
        )

    elif args.dtype == 'plot_contagion':
        targets = [lang for lang in args.targets.split()] if type(args.targets) == str else args.targets

        ot_files = [p for p in Path(args.datapath).rglob('*_ot.npy')]
        rt_files = [p for p in Path(args.datapath).rglob('*_rt.npy')]

        if 'all' in targets:
            targets = all_languages

        for files in [ot_files, rt_files]:
            eval.error(
                files,
                confmats_out,
                languages=all_languages,
                targets=targets,
                hashtbl=lookup_tbl.values,
                resolution='D'
            )

    elif args.dtype == 'compare':
        targets = [lang for lang in args.targets.split()] if type(args.targets) == str else args.targets
        if 'all' in targets:
            targets = all_languages

        eval_vis.compare(
            [p for p in Path(args.datapath).glob(f'{args.year1}*.npy')],
            [p for p in Path(args.datapath).glob(f'{args.year2}*.npy')],
            languages=all_languages,
            targets=targets,
            hashtbl=lookup_tbl.values,
            savepath=f'{confmats_out}/confmat'
        )

        try:
            eval_vis.compare(
                [p for p in Path(args.datapath).glob(f'{args.year1}*_ot.npy')],
                [p for p in Path(args.datapath).glob(f'{args.year2}*_ot.npy')],
                languages=all_languages,
                targets=targets,
                hashtbl=lookup_tbl.values,
                savepath=f'{confmats_out}/confmat_ot'
            )

            eval_vis.compare(
                [p for p in Path(args.datapath).glob(f'{args.year1}*_rt.npy')],
                [p for p in Path(args.datapath).glob(f'{args.year2}*_rt.npy')],
                languages=all_languages,
                targets=targets,
                hashtbl=lookup_tbl.values,
                savepath=f'{confmats_out}/confmat_rt'
            )
        except IndexError:
            pass

        print(f'Saved: {out}/confmat')

    elif args.dtype == 'test':
        eval.test(
            Path(args.datapath),
            out/'samples.json',
            args.model,
            eparser,
            nparser,
            float(args.score),
            int(args.samples)
        )

    elif args.dtype == 'gain':
        targets = [lang for lang in args.targets.split()] if type(args.targets) == str else args.targets

        if 'all' in targets:
            targets = all_languages

        eval.gain(Path(args.datapath), out, languages=targets)

        print('Compressing files... ')
        with tarfile.open(f'{out}.tar.gz', "w:gz") as tar:
            tar.add(out, arcname=out.stem)

        print('Deleting temporary files... ')
        shutil.rmtree(out, ignore_errors=True)

    elif args.dtype == 'diagnosis':
        eval.diagnosis(
            Path(f'{args.datapath}.tar.gz'),
            out,
            args.codes
        )

    elif args.dtype == 'aggregate':
        out = Path(config['languages']).parent
        out.mkdir(parents=True, exist_ok=True)

        files = [p for p in Path(args.datapath).rglob('*.csv.gz')]
        eval.aggregate(files, out/'diagnosis.csv.gz')

    elif args.dtype == 'tweet_length':
        eval.tweet_length(
            Path(args.datapath),
            out,
            args.codes,
            languages_hashtbl,
        )

    elif args.dtype == 'agreement':
        try:
            df = pd.read_csv(
                Path(args.datapath),
                sep=',',
                header=0,
                memory_map=True,
                index_col='date'
            )
            df.language.fillna('unknown', inplace=True)
            df.index = pd.to_datetime(df.index)
        except:
            print('Error: wrong file format!')
            exit()

        # VERY special data cleaning for the paper...
        # ----------------------------------------------------------------------------------------------------------- #
        supported_languages = pd.read_csv(config['supported_languages'], header=0).fillna(value='unknown')
        languages_hashtbl = pd.read_json(config['language_hashtbl'], orient='index').to_dict()['new_label']
        df = df.replace(languages_hashtbl)
        df = df.groupby(['date', 'language']).sum().reset_index()
        df = df.set_index('date')
        df = df.drop(consts.corrupted_days)
        # ----------------------------------------------------------------------------------------------------------- #

        eval.agreement(
            df,
            out,
            supported_languages.values
        )

    elif args.dtype == 'error':
        out = Path(config['languages']).parent
        out.mkdir(parents=True, exist_ok=True)

        eval.error(
            Path(args.datapath),
            out/'errors',
            languages=all_languages,
            hashtbl=lookup_tbl.values,
        )

    elif args.dtype == 'val':
        try:
            languages = pd.read_csv(
                Path(args.languages),
                sep=',',
                header=0,
                memory_map=True,
                index_col=['date', 'language'],
            )

            error = pd.read_csv(
                Path(args.error),
                sep=',',
                header=0,
                memory_map=True,
                index_col=['date', 'language'],
            )
        except:
            print('Error: wrong file format!')
            exit()

        df = pd.merge(languages, error, on=['date', 'language'], sort=False)

        # VERY special data cleaning for the paper...
        # ----------------------------------------------------------------------------------------------------------- #
        supported_languages = pd.read_csv(config['supported_languages'], header=0).fillna(value='unknown')
        languages_hashtbl = pd.read_json(config['language_hashtbl'], orient='index').to_dict()['new_label']
        df = df.rename(languages_hashtbl)
        df = df.drop(consts.corrupted_days, errors='ignore')
        # ----------------------------------------------------------------------------------------------------------- #

        eval.val(
            df,
            out,
            supported_languages.values
        )

    else:
        print('Error: unknown action!')

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec.')


if __name__ == "__main__":
    main()
