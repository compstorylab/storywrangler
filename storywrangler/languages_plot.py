
import sys
import time
from pathlib import Path

import numpy as np
import pandas as pd
import ujson

from storywrangler import cli
from storywrangler import consts
from storywrangler import languages_analytics
from storywrangler import languages_vis


def parse_args(args, config):

    parser = cli.parser()

    # optional subparsers
    subparsers = parser.add_subparsers(help='Arguments for specific action.', dest='dtype')
    subparsers.required = True

    all_parser = subparsers.add_parser(
        'all',
        help='Run all available plots',
    )
    all_parser.add_argument(
        'datapath',
        help='path to a CSV file(s) (ie. /tmp/[N]grams)'
    )

    summary_parser = subparsers.add_parser(
        'summary',
        help='Plot a summary figure of language evolution of Twitter'
    )
    summary_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )

    story_parser = subparsers.add_parser(
        'story',
        help='Plot total number of languages captured by Twitter and FastText\
              along with the volume of tweets for top languages'
    )
    story_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )

    rank_parser = subparsers.add_parser(
        'rank',
        help='Plot top-15 ranked languages over time'
    )
    rank_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )

    compare_parser = subparsers.add_parser(
        'compare',
        help='Compare top-30 ranked language time series by twitter and FastText'
    )
    compare_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )

    ratio_parser = subparsers.add_parser(
        'ratio',
        help='Compare the ratio of tweets to retweets of the top-30 ranked languages on Twitter'
    )
    ratio_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )

    zipf_parser = subparsers.add_parser(
        'zipf',
        help='Plot zipf distribution for all supported languages by twitter/FastText',
    )
    zipf_parser.add_argument(
        'datapath',
        help='path to a CSV file(s) (ie. /tmp/languages)'
    )

    timeline_parser = subparsers.add_parser(
        'timeline',
        help='Plot a timeline comparing the frequency of languages on twitter'
    )
    timeline_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )

    shift_parser = subparsers.add_parser(
        'shift',
        help='Plot language shifts comparing the normalized usage of languages on twitter'
    )
    shift_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )

    divergence_parser = subparsers.add_parser(
        'div',
        help='Compare Twitter to FastText language labels'
    )
    divergence_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )

    contagiograms_parser = subparsers.add_parser(
        'contagiograms',
        help='Plot a grid of contagiograms: rate of usage timeseries + contagion fractions'
    )
    contagiograms_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )

    stats_parser = subparsers.add_parser(
        'stats',
        help='Plot a table of total number of messages captured by language',
    )
    stats_parser.add_argument(
        'datapath',
        help='path to a CSV file(s) (ie. /tmp/languages)'
    )

    lang_usage_parser = subparsers.add_parser(
        'lang_usage',
        help='Plot a timeseries of language usage'
    )
    lang_usage_parser.add_argument(
        'datapath',
        help='path to CSV file(s) (ie. /tmp/languages)'
    )
    lang_usage_parser.add_argument(
        'lang',
        default='_all',
        help='language to query'
    )

    # optional args
    parser.add_argument(
        '-r', '--resolution',
        default='W',
        help="group days based on a given timescale [i.e. 'D', 'W', 'M', '6M', 'Y']"
    )

    parser.add_argument(
        '-t', '--targets',
        type=str,
        default=consts.defaultLangs,
        help='list of languages to use (i.e. "en es ja")'
    )

    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['plots'])/'languages',
        help='absolute Path to save figures'
    )

    parser.add_argument(
        '-c', '--compressed',
        action='store_true',
        help='compressed days'
    )

    parser.add_argument(
        '-x', '--compute',
        action='store_true',
        help='option to re-compute dataframes'
    )

    return parser.parse_args(args)


def main(args=None):

    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)
    out = Path(args.outdir)
    out.mkdir(parents=True, exist_ok=True)

    supported_languages = pd.read_csv(config['supported_languages'], header=0).fillna(value='unknown')
    languages = np.union1d(supported_languages.FastText.values, supported_languages.Twitter.values)
    targets = [lang for lang in args.targets.split()] if type(args.targets) == str else args.targets

    if args.compute:
        if args.compressed:
            files = [p for p in Path(args.datapath).rglob('*.tar.gz')]
            df = languages_analytics.aggregate_counts(files, out/'languages.csv.gz', vacc=True)
        else:
            files = [p for p in Path(args.datapath).rglob('*.csv.gz') if p.match('*/languages/*')]
            df = languages_analytics.aggregate_counts(files, out/'languages.csv.gz')
    else:
        try:
            df = pd.read_csv(
                Path(args.datapath),
                sep=',',
                header=0,
                usecols=[
                    'date', 'language',
                    'ft_count', 'ft_rank', 'ft_freq', 'ft_tweets', 'ft_retweets', 'ft_comments', "ft_speakers",
                    'tw_count', 'tw_rank', 'tw_freq', 'tw_tweets', 'tw_retweets', 'tw_comments', "tw_speakers",
                ],
                memory_map=True,
                index_col='date'
            )
            df.language.fillna('unknown', inplace=True)
            df.index = pd.to_datetime(df.index)

        except:
            print('Error: wrong file format!')
            exit()

    # VERY special data cleaning for the paper...
    # ---------------------------------------------------------------------------------------------------------------- #
    languages_hashtbl = pd.read_json(config['language_hashtbl'], orient='index').to_dict()['new_label']
    targets = list(set(languages) - set(languages_hashtbl.keys())) if 'all' in targets else targets
    df = df.replace(languages_hashtbl)
    df = df.groupby(['date', 'language']).sum().reset_index()
    df = df.set_index('date')
    df = df.drop(consts.corrupted_days)

    df['tw_freq'] = df['tw_count'] / df['tw_count'].sum()
    df['ft_freq'] = df['ft_count'] / df['ft_count'].sum()
    df['tw_rank'] = df['tw_count'].rank(method='average', ascending=False)
    df['ft_rank'] = df['ft_count'].rank(method='average', ascending=False)
    # ---------------------------------------------------------------------------------------------------------------- #

    if args.dtype == 'all':
        languages_analytics.stats(df, out, supported_languages.values)
        languages_analytics.zipf(df, supported_languages.values, out)
        languages_analytics.shift(df, out, supported_languages.values)
        languages_analytics.divergence(df, out, supported_languages.values)
        languages_analytics.story(df, out, targets, supported_languages.values)
        languages_analytics.rank(df, out, supported_languages.values, n=15)
        languages_analytics.summary(df, out, supported_languages.values)
        languages_analytics.ratio(df, out, supported_languages.values)
        languages_analytics.compare(df, out, supported_languages.values)
        languages_analytics.timeline(df, out, supported_languages.values, args.resolution)

    elif args.dtype == 'summary':
        languages_analytics.summary(df, out, hashtbl=supported_languages.values)

    elif args.dtype == 'ratio':
        languages_analytics.ratio(df, out, hashtbl=supported_languages.values)

    elif args.dtype == 'story':
        languages_analytics.story(df, out, targets=targets, hashtbl=supported_languages.values)

    elif args.dtype == 'rank':
        languages_analytics.rank(df, out, hashtbl=supported_languages.values, n=15)

    elif args.dtype == 'compare':
        languages_analytics.compare(df, out, hashtbl=supported_languages.values)

    elif args.dtype == 'zipf':
        languages_analytics.zipf(df, supported_languages.values, out)

    elif args.dtype == 'div':
        languages_analytics.divergence(df, out, hashtbl=supported_languages.values)

    elif args.dtype == 'shift':
        languages_analytics.shift(df, out, supported_languages.values)

    elif args.dtype == 'timeline':
        languages_analytics.timeline(df, out, hashtbl=supported_languages.values, resolution=args.resolution)

    elif args.dtype == 'contagiograms':
        supported_languages.set_index('FastText', inplace=True)
        supported_languages.drop(columns='Twitter', inplace=True)
        supported_languages = supported_languages.to_dict()['Language']
        supported_languages.update({'und': 'Undefined'})

        languages_analytics.contagiograms(
            savepath=Path(args.outdir),
            lang_hashtbl=supported_languages
        )

    elif args.dtype == 'stats':
        languages_analytics.stats(
            df,
            savepath=out,
            hashtbl=supported_languages.values
        )

        supported_languages.set_index('FastText', inplace=True)
        supported_languages.drop(columns='Twitter', inplace=True)
        supported_languages = supported_languages.to_dict()['Language']
        supported_languages.update({'und': 'Undefined'})

        d = languages_analytics.statsdf(lang='_all')
        languages_vis.plot_stats_timeseries(f'{Path(args.outdir)}/stats_timeseries', d)
        print(f'Saved: {Path(args.outdir)}/stats_timeseries')

        languages_vis.plot_stats_dists(f'{Path(args.outdir)}/stats_dists', d)
        print(f'Saved: {Path(args.outdir)}/stats_dists')

    elif args.dtype == 'lang_usage':
        d = languages_analytics.statsdf(lang=args.lang)
        languages_vis.plot_lang_usage(f'{Path(args.outdir)}/usage_{args.lang}', d)
        print(f'Saved: {Path(args.outdir)}/usage_{args.lang}')

    else:
        print('Error: unknown action!')

    print(f'Total time elapsed: {time.time() - timeit:.2f} secs.')


if __name__ == "__main__":
    main()
