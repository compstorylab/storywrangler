
import regex as re
from datetime import datetime, timedelta
from pathlib import Path
from pytrends.request import TrendReq

import numpy as np
import pandas as pd
from nltk.corpus import stopwords
import ujson
from PyPDF2 import PdfFileReader, PdfFileMerger

from storywrangler import consts
from storywrangler import ngrams_vis
from storywrangler import regexr
from storywrangler import tweet_utils
from storywrangler.ngrams_query import Query

from storywrangling import Storywrangler


def flipbook(savepath, datapath):
    """Combine PDFs into a flipBook

    Args:
      savepath: path to save generated pdf
      datapath: a list of files to combine

    """
    pdf = PdfFileMerger()

    for f in datapath.rglob('*.pdf'):
        print(f)
        pdf.append(PdfFileReader(str(f), 'rb'))

    pdf.write(f"{savepath}/flipbook_{datapath.stem}.pdf")
    pdf.close()

    print(f"Saved: {savepath}/flipbook_{datapath.stem}.pdf")


def collect_stats(datapath, savepath):
    """Generate summary dataframe for ngrams

    Args:
      datapath: path to ngrams directories
      savepath: path to save summary file

    """
    data = {}
    for day in datapath.rglob(f'*.tar.gz'):
        date = day.stem.split('.')[0]
        if data.get(date) is None:
            data[date] = {}

        try:
            s = tweet_utils.read_tarball(
                day,
                filename=f'summary_fastText',
                open_func=ujson.load,
            )
        except Exception:
            continue

        try:
            data[date]['num_tweets'] = s['num_tweets']
            data[date]['num_retweets'] = s['num_retweets']
            data[date]['num_comments'] = s['num_comments']
            data[date]['num_corrupted'] = s['num_corrupted']
            data[date]['num_activities'] = s['num_activities']
        except KeyError:
            pass

        for n in range(1, 4):
            try:
                data[date][f'num_{n}grams'] = s[f'num_{n}grams']
                data[date][f'num_{n}grams_no_rt'] = s[f'num_{n}grams_no_rt']
                data[date][f'unique_{n}grams'] = s[f'unique_{n}grams']
            except KeyError:
                pass

    df = pd.DataFrame.from_dict(data, orient='index')
    df.index = pd.to_datetime(df.index)
    df.to_csv(f'{savepath}/ngrams.csv')
    print(f'Saved: {savepath}/ngrams.csv')


def stats(datapath, savepath):
    """Plot general ngram statistics overtime

    Args:
      datapath: path to daily language usage on Twitter
      savepath: path to save generated plot

    """
    df = pd.read_csv(datapath, header=0, index_col=0)
    df.index = pd.to_datetime(df.index)
    df = df.drop(consts.corrupted_days)
    for n in consts.ngrams:
        df[f'num_{n}_rt'] = df[f'num_{n}'] - df[f'num_{n}_no_rt']

    for n in consts.ngrams:
        print(df.filter(
            items=[
                f'num_{n}',
                f'num_{n}_no_rt',
                f'num_{n}_rt',
                f'unique_{n}'
            ]
        ).describe())
        print('-' * 75)

    ngrams_vis.plot_stats_timeseries(f'{savepath}/stats_timeseries', df)
    print(f'Saved: {savepath}/stats_timeseries')

    ngrams_vis.plot_stats_dists(f'{savepath}/stats_dists', df)
    print(f'Saved: {savepath}/stats_dists')


def aggregate_ngrams(paths):
    """Combine a list of dataframes

    Args:
      paths: a list of paths to csv file(s)

    Returns:
      a dataframe

    """
    cols = ['ngram', 'count']
    df = pd.read_csv(paths[0], sep='\t', header=0, usecols=cols, memory_map=True)
    df['date'] = datetime.strptime(Path(paths[0]).stem.split('.')[0], '%Y-%m-%d')

    for p in paths[1:]:
        d = pd.read_csv(p, sep='\t', header=0, usecols=cols, memory_map=True)
        d['date'] = datetime.strptime(Path(p).stem.split('.')[0], '%Y-%m-%d')
        df = pd.concat([df, d], sort=False, ignore_index=True)

    return df.set_index('date')


def zipf(path, date, savepath, eparser, lang='en'):
    """Plot a zipf distribution of [N]-grams for a desired language

    Args:
      path: path to a ngrams directory
      date: desired date to parse out
      savepath: path to save plot
      lang: desired language (Default value = 'en')
      eparser: emoji regex parser

    """

    def hashtags_parser(s):
        return bool(re.search(r'\B(\#\w+\b)', s))

    def handles_parser(s):
        return bool(re.search(r'\B(\@\w+\b)', s))

    def emojis_parser(s):
        return bool(eparser.search(s))

    dfs = {
        '1grams': {},
        '2grams': {},
        '3grams': {},
    }

    sample_size = .01

    for n in dfs.keys():
        print(f"Processing {n}: ")

        df = tweet_utils.read_tarball(
            Path(f'{path}/{n}/{date}.tar.gz'),
            filename=f'{lang}_',
            kwargs=dict(
                header=0,
                memory_map=True,
                sep='\t',
                compression='gzip',
                encoding='utf-8',
            )
        )
        inds = sorted(np.random.choice(
            df.index.values,
            p=df.freq.values,
            size=int(sample_size * df.shape[0]),
        ))
        df = df.iloc[inds, :]

        print('Parsing hashtags...')
        hashtags = df[df['ngram'].astype(str).apply(hashtags_parser)]

        print('Parsing handles...')
        handles = df[df['ngram'].astype(str).apply(handles_parser)]

        print('Parsing emojis...')
        emojis = df[df['ngram'].astype(str).apply(emojis_parser)]

        dfs[n] = {
            'All': df,
            'Hashtags': hashtags,
            'Handles': handles,
            'Emojis': emojis
        }

    print('Plotting...')
    ngrams_vis.plot_zipf(
        f'{savepath}/{lang}_{path.stem.split(".")[0]}_zipf',
        dfs
    )

    print(f'Saved: {savepath}/{lang}_{path.stem.split(".")[0]}_zipf')


def load_freq_distribution(paths, languages=consts.reachedtop10):
    """Helper function to load frequency distribution files"""

    print(f'The Langs {languages}')

    return tweet_utils.read_tarball_multi(paths, languages, {'sep': '\t'})


def get_counts(df, all='count', no_rt='count_no_rt'):
    """Get an array of just counts

    Args:
      df: dataframe
      all: column name for all (Default value = 'count')
      no_rt: column name for count_no_rt (Default value = 'count_no_rt')

    """

    return df[all].values, df[no_rt].values


def get_zipf_dist(df, n_interp=None, x_min=1, x_max=None, y_min=1, y_max=None):
    """Get the Zipf distribution for counts of organic and all tweets

    Args:
      df(pd.DataFrame): daily Zipf DataFrame
      n_interp(None or int, optional): optional number of points to linearly interpolate (Default value = None)
      x_min:  (Default value = 1)
      x_max:  (Default value = None)
      y_min:  (Default value = 1)
      y_max:  (Default value = None)

    Returns:
      tuple of all and no_rt Zipf

    """

    counts_all, counts_no_rt = get_counts(df)

    counts_rt = np.array(sorted(counts_all - counts_no_rt, reverse=True))
    counts_no_rt = np.array(sorted(counts_no_rt, reverse=True))

    rt_zipf = (np.log10(np.arange(counts_rt.shape[0])), np.log10(counts_rt / counts_rt.sum()))
    no_rt_zipf = (np.log10(np.arange(counts_no_rt.shape[0])), np.log10(counts_no_rt / counts_no_rt.sum()))
    combo = (no_rt_zipf, rt_zipf)

    if n_interp:  # if linearly interpolate values to save space
        new_combo = []
        for (x, y) in combo:
            x, y = x[x_min:x_max], y[y_min:y_max]

            interp_x = np.linspace(x.min(), x.max(), n_interp)
            interp_y = np.interp(interp_x, x, y)
            new_combo.append((interp_x, interp_y))

        return new_combo

    else:
        return combo


def load_daily_zipfs(paths, lang_list=['tsv'], stream_function=get_counts, sf_args={}):
    """Load daily zipf distributions to a dict of filename2zipf distribution

    Args:
      paths: list of paths to load from
      lang_list: list of languages of interest (Default value = ['tsv'])
      stream_function: function to use within tarball stream (Default value = get_counts)
      sf_args: arguments for stream function (Default value = {})

    """

    return_dict = {}

    print(f'Supported langs {lang_list}')

    for path in paths:
        print(f'Working on {path}.')
        temp_df = tweet_utils.read_tarball_multi(
            path,
            lang_list,
            {'compression': 'gzip',
             'encoding': 'utf-8',
             'sep': '\t',
             'na_filter': False
             },
            open_func=pd.read_csv,
            stream_func=stream_function,
            sfargs=sf_args,
        )

        print('temp_df shape', len(temp_df))

        return_dict.update({path: temp_df})

    return return_dict


def ngrams_grid(
        savepath,
        lang_hashtbl,
        case_sensitive=True,
        start_date=datetime(2010, 1, 1)
):
    """Plot a grid of time series

    Args:
      savepath: path to save generated plot
      lang_hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
      case_sensitive: a toggle for case_sensitive lookups (Default value = True)
      start_date: starting date for the query

    """
    words = [
        ('😊', 'en'), ('trump', 'en'), ('#metoo', 'en'),
        ('#newyear', 'en'), ('2015', 'en'), ('i\'m', 'en'),
        ('virus', 'en'), ('@bts_twt', 'en'), ('thrones', 'en'),
        ('black hole', 'en'), ('Stephen Hawking', 'en'), ('avengers', 'en'),
        ('katrina', 'en'), ('9/11', 'en'), ('#lasvegasshooting', 'en'),
    ]

    ngrams = []
    for i, (w, lang) in enumerate(words):
        n = len(w.split())
        print(f"Retrieving {lang_hashtbl.get(lang)}: {n}gram -- '{w}'")

        q = Query(f'{n}grams', lang)

        if case_sensitive:
            d = q.query_timeseries(w, start_time=start_date)
        else:
            d = q.query_insensitive_timeseries(w, start_time=start_date)

        d.index.name = f"{lang_hashtbl.get(lang)}\n'{w}'"
        ngrams.append(d)

    ngrams_vis.plot_ngrams_grid(
        f'{savepath}/ngrams_grid',
        ngrams,
    )
    print(f'Saved: {savepath}/ngrams_grid')


def chart(
        savepath,
        lang_hashtbl,
        nparser,
        case_sensitive=True,
        start_date=datetime(2010, 1, 1),
):
    """Plot a grid of ngrams timeseries

    Args:
      savepath: path to save generated plot
      lang_hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
      nparser: compiled ngram parser
      case_sensitive: a toggle for case_sensitive lookups (Default value = True)
      start_date: starting date for the query

    """

    targets = dict(
        Years=[
            ('2012', '_all', 'C0'),
            ('2014', '_all', 'C1'),
            ('2016', '_all', 'C2'),
            ('2018', '_all', 'C3'),
        ],
        Periodic=[
            ('Christmas', 'en', 'C0'),
            ('Pasqua', 'it', 'C1'),
            ('eleição', 'pt', 'C2'),
            ('sommar', 'sv', 'C3'),
        ],
        Sports=[
            ('Olympics', 'en', 'C0'),
            ('World Cup', 'en', 'C1'),
            ('Super Bowl', 'en', 'C3'),
        ],
        Science=[
            ('Higgs', 'en', 'C0'),
            ('#AlphaGo', 'en', 'C1'),
            ('gravitational waves', 'en', 'C2'),
            ('CRISPR', 'en', 'C5'),
            ('black hole', 'en', 'C3'),
        ],
        Fame=[
            ('Cristiano Ronaldo', 'pt', 'C0'),
            ('Donald Trump', 'en', 'C1'),
            ('Papa Francesco', 'it', 'C3'),
        ],
        Outbreaks=[
            ('cholera', 'en', 'C0'),
            ('Ebola', 'en', 'C1'),
            ('Zika', 'en', 'C2'),
            ('coronavirus', 'en', 'C3'),
        ],
        Conflicts=[
            ('غزة', 'ar', 'C0'),
            ('Libye', 'fr', 'C1'),
            ('Suriye', 'tr', 'C2'),
            ('Росія', 'uk', 'C3'),
        ],
        Movements=[
            ('ثورة', 'ar', 'C0'),
            ('Occupy', 'en', 'C1'),
            ('Black Lives Matter', 'en', 'C3'),
            ('Brexit', 'en', 'C2'),
            ('#MeToo', 'en', 'C5'),
        ],
    )

    df = None
    for topic, ngrams in targets.items():
        for i, (w, lang, color) in enumerate(ngrams):
            n = len(regexr.ngrams(w, parser=nparser, n=1))
            print(f"Retrieving {lang_hashtbl.get(lang)}: {n}gram -- '{w}'")

            q = Query(f'{n}grams', lang)
            if case_sensitive:
                d = q.query_timeseries(w, start_time=start_date)
            else:
                d = q.query_insensitive_timeseries(w, start_time=start_date)

            print(f"Top rank: {d['rank'].min()} -- {d['rank'].idxmin().date()}")

            d = d.dropna(how='all')
            ts = pd.DataFrame(dict(
                ngram=w,
                topic=topic,
                date=d.index,
                val=d['rank'].fillna(10 ** 6),
                color=color,
                lang=lang if lang_hashtbl.get(lang) is not None else 'All',
            ))

            if df is not None:
                df = df.append(ts)
            else:
                df = ts

    ngrams_vis.plot_chart(
        f'{savepath}/chart',
        df,
        shading=False
    )
    print(f'Saved: {savepath}/chart')


def compare(
        savepath,
        lang_hashtbl,
        nparser,
        tvnews,
        case_sensitive=False,
        start_date=datetime(2018, 12, 1),
        end_date=datetime(2021, 2, 20),
        timescale='W'
):
    """Plot a grid of ngrams timeseries

    Args:
      savepath: path to save generated plot
      lang_hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
      nparser: compiled ngram parser
      tvnews: path to tv news data
      case_sensitive: a toggle for case_sensitive lookups (Default value = False)
      start_date: starting date for the query
      end_date: ending date for the query
      timescale: desired timescale to plot (Default value = 'W')

    """
    thresholds = {
        'twitter': 10,
        'news': 3,
    }
    targets = [
        'working from home',
        'unemployment',
        'fake news',
        'qanon',
        'antifa',
        'fandom',
        '#freebritney',
        '@jypetwice',
        'keto diet',
        'leggings',
    ]

    pytrends = TrendReq(
        hl='en-US',
        tz=-300,
        timeout=(3, 12),
        retries=1,
        backoff_factor=0.1,
        requests_args={'verify': False}
    )

    tvnews = pd.read_csv(tvnews, header=0, index_col='Time')
    tvnews['Query'] = tvnews['Query'].str.lower()
    tvnews['Query'] = tvnews['Query'].str.replace('"', '')
    tvnews['Query'] = tvnews['Query'].str.replace('text=', '')
    tvnews.index = pd.to_datetime(tvnews.index)

    df = None
    for w in targets:
        n = len(regexr.ngrams(w, parser=nparser, n=1))
        print(f"Retrieving {lang_hashtbl.get('en')}: {n}gram -- '{w}'")
        q = Query(f'{n}grams', 'en')

        if case_sensitive:
            d = q.query_timeseries(w, start_time=start_date)
        else:
            d = q.query_insensitive_timeseries(w, start_time=start_date)

        pytrends.build_payload(
            [w],
            timeframe=f"{start_date.strftime('%Y-%m-%d')} {end_date.strftime('%Y-%m-%d')}",
            geo='',
            gprop='',
        )
        gtrends = pytrends.interest_over_time()
        gtrends = pd.Series(0, index=pd.date_range(start_date, end_date)) if gtrends.empty else gtrends[w]
        gtrends = gtrends.resample(timescale).sum()

        twitter = d['count'].fillna(0).resample(timescale).sum()
        twitter = twitter.loc[gtrends.index]
        twitter[twitter < thresholds['twitter']] = 0
        twitter = ((twitter - twitter.min()) / (twitter.max() - twitter.min())) * 100

        twitter_ot = d['count_no_rt'].fillna(0).resample(timescale).sum()
        twitter_ot = twitter_ot.loc[gtrends.index]
        twitter_ot[twitter_ot < thresholds['twitter']] = 0
        twitter_ot = ((twitter_ot - twitter_ot.min()) / (twitter_ot.max() - twitter_ot.min())) * 100

        news = tvnews[tvnews['Query'] == w]['Value']
        if news.empty:
            news = pd.Series(0, index=pd.date_range(start_date, end_date))
        else:
            news = pd.concat([news, pd.Series(0, index=pd.date_range(start_date, end_date))]).fillna(0)

        news = news.resample(timescale).sum()
        news = news.loc[gtrends.index]
        news[news < thresholds['news']] = 0
        news = ((news - news.min()) / (news.max() - news.min())) * 100

        ts = pd.DataFrame(dict(
            ngram=w,
            date=twitter.index,
            twitter=twitter,
            twitter_ot=twitter_ot,
            gtrends=gtrends,
            news=news
        )).fillna(0)

        if df is not None:
            df = df.append(ts)
        else:
            df = ts

    ngrams_vis.plot_compare(
        f'{savepath}/compare',
        df,
    )
    print(f'Saved: {savepath}/compare')


def perspective(
        savepath,
        lang_hashtbl,
        nparser,
        case_sensitive=True,
        start_date=datetime(2012, 1, 1),
):
    """Plot a grid of ngrams timeseries

    Args:
      savepath: path to save generated plot
      lang_hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
      nparser: compiled ngram parser
      case_sensitive: a toggle for case_sensitive lookups (Default value = True)
      start_date: starting date for the query

    """

    targets = dict(
        Emojis=[
            ('🍑', '_all', 'C0'),
            ('😂', '_all', 'C1'),
            ('🇺🇸', '_all', 'C2'),
            ('😷', '_all', 'C3'),
        ],
        Handles=[
            ('@BarackObama', '_all', 'C0'),
            ('@katyperry', '_all', 'C1'),
            ('@neiltyson', '_all', 'C2'),
            ('@bts_twt', '_all', 'C3'),
        ],
        Tech=[
            ('#DigitalTransformation', '_all', 'C0'),
            ('#futureofwork', '_all', 'C1'),
            ('#Automation', '_all', 'C3'),
            ('#IoT', '_all', 'C4'),
            ('#RPA', '_all', 'C5'),
        ],
        Campaigns=[
            ('#MentalHealthAwareness', '_all', 'C0'),
            ('#TogetherWeCan', '_all', 'C1'),
            ('#qanda', '_all', 'C2'),
            ('#BellLetsTalk', '_all', 'C3'),
        ],
        Emerging=[
            ('alternative facts', 'en', 'C0'),
            ('deep state', 'en', 'C1'),
            ('fake news', 'en', 'C2'),
            ('PRISM', 'en', 'C3'),
            ('WFH', 'en', 'C4'),
        ],
        Pop=[
            ('@ygofficialblink', '_all', 'C0'),
            ('@JYPETWICE', '_all', 'C1'),
            ('@BTS_twt', '_all', 'C2'),
            ('#GIDLE', '_all', 'C3'),
            ('K/DA', '_all', 'C4'),
            ('EXO', '_all', 'C5'),
        ],
        Movments=[
            ('#BringBackOurGirls', '_all', 'C0'),
            ('#HongKongProtests', '_all', 'C1'),
            ('#MeToo', '_all', 'C2'),
            ('#LGBTQ', '_all', 'C3'),
        ],
        Movments2=[
            ('#BlackLivesMatter', '_all', 'C0'),
            ('#AllLivesMatter', '_all', 'C1'),
            ('#BlueLivesMatter', '_all', 'C2'),
            ('#TransLivesMatter', '_all', 'C3'),
        ],
    )

    df = None
    for topic, ngrams in targets.items():
        for i, (w, lang, color) in enumerate(ngrams):
            n = len(regexr.ngrams(w, parser=nparser, n=1))
            print(f"Retrieving {lang_hashtbl.get(lang)}: {n}gram -- '{w}'")

            q = Query(f'{n}grams', lang)
            if case_sensitive:
                d = q.query_timeseries(w, start_time=start_date)[:-2]
            else:
                d = q.query_insensitive_timeseries(w, start_time=start_date)[:-2]

            print(f"Top rank: {d['rank'].min()} -- {d['rank'].idxmin().date()}")

            ts = pd.DataFrame(dict(
                ngram=w,
                topic=topic,
                date=d.index,
                val=d['rank'].fillna(10 ** 6),
                color=color,
                lang=lang if lang_hashtbl.get(lang) is not None else 'All',
            ))

            if df is not None:
                df = df.append(ts)
            else:
                df = ts

    ngrams_vis.plot_perspective(
        f'{savepath}/perspective',
        df,
        shading=False
    )
    print(f'Saved: {savepath}/perspective')


def cleanup(text):
    """Remove non-latin, punctuation, links, and retweet prefix"""

    cleaned = ""
    ignore = stopwords.words('english') + ['rt']

    for t in text.split(' '):
        t = str(t).encode('ascii', 'ignore').decode('ascii').lower()
        t = re.sub(r">|<", "", t)
        t = re.sub(r"^\p{P}+$", "", t)
        t = re.sub(r"http\S+", "", t)
        t = re.sub(r"@\S+", "", t)
        t = "" if t in ignore else t

        if t == "":
            return ""
        else:
            cleaned = t if cleaned == "" else f"{cleaned} {t}"
    return cleaned


def case_permutations(s):
    """Generate all case insensitive permutations for a string"""
    if not s:
        yield ""
    else:
        first = s[:1]
        if first.lower() == first.upper():
            for sub in case_permutations(s[1:]):
                yield first + sub
        else:
            for sub in case_permutations(s[1:]):
                yield first.lower() + sub
                yield first.upper() + sub


def trending(savepath, nwords=21):
    """Plot narratively trending ngrams

    Args:
      savepath: path to save generated plot
      nwords: number of ngrams (Default value = 21)

    """
    api = Storywrangler()

    ngrams = {
        "Soleimani": (datetime(2020, 1, 3), '2grams'),
        "COVID19": (datetime(2020, 3, 11), '1grams'),
        "BlackLivesMatter": (datetime(2020, 6, 2), '1grams'),
        "RBG": (datetime(2020, 9, 18), '2grams'),
        "2020 Election": (datetime(2020, 11, 4), '1grams'),
        "Capital": (datetime(2021, 1, 6), '2grams'),
    }

    vol = api.get_lang('en', start_time=datetime(2020, 1, 1), end_time=datetime(2021, 2, 1))

    trends = {}

    for k, (date, n) in ngrams.items():
        trends[date] = {}
        df = api.get_divergence(
            date=date,
            lang="en",
            ngrams=n,
        ).reset_index()

        df['ngram'] = df['ngram'].apply(cleanup)
        df = df[df['ngram'] != ''].fillna(0)

        ngrams[k] = df.groupby('ngram').agg({
            'rd_contribution': 'sum',
            'rd_contribution_no_rt': 'sum',
        })
        ngrams[k] = (ngrams[k] - ngrams[k].min()) / (ngrams[k].max() - ngrams[k].min())

        words_of_interest = ngrams[k].nlargest(nwords, 'rd_contribution').index.values
        for w in words_of_interest:
            if len(trends[date].keys()) == nwords:
                break

            print(f"Finding case sensitive permutations for: {w}")
            try:
                cases = list(case_permutations(w))
                cases = [w.lower(), w.capitalize(), w.title(), w.upper()] if len(cases) > 10**5 else cases

                rates = api.get_ngrams_array(
                    ngrams_list=cases,
                    start_time=date,
                    end_time=date,
                    lang='en'
                )
            except KeyError:
                continue

            word = rates[['count']].idxmax()[-1][-1]

            rates = rates.groupby('time').agg({
                'count': 'sum',
                'count_no_rt': 'sum',
                'freq': 'sum',
                'freq_no_rt': 'sum',
                'rank': 'min',
                'rank_no_rt': 'min',
            })
            r = (rates['count'] - rates['count_no_rt']) / rates['count']
            lang = (vol.loc[date, f'num_{n}'] - vol.loc[date, f'num_{n}_no_rt']) / vol.loc[date, f'num_{n}']
            rates['r_rel'] = (r / lang).replace([np.inf, -np.inf, np.nan], 1)

            trends[date][word] = {
                'r_rel': np.log10(rates['r_rel'].values[0]),
                'count': rates['count'].values[0],
                'count_no_rt': rates['count_no_rt'].values[0],
                'freq': rates['freq'].values[0],
                'freq_no_rt': rates['freq_no_rt'].values[0],
                'rank': rates['rank'].values[0],
                'rank_no_rt': rates['rank_no_rt'].values[0],
                'rd_contribution': ngrams[k].loc[w, 'rd_contribution'],
                'rd_contribution_no_rt': ngrams[k].loc[w, 'rd_contribution_no_rt'],
            }

        trends[date] = pd.DataFrame.from_dict(trends[date], orient='index')

    ngrams_vis.plot_trending(
        f'{savepath}/trending',
        trends,
        vol
    )

    print(f'Saved: {savepath}/trending')


def trending_grid(savepath, n="1grams", nwords=20):
    """Plot a grid of narratively trending ngrams

    Args:
      savepath: path to save generated plot
      n:  (Default value = "1grams")
      nwords:  (Default value = 20)

    """
    api = Storywrangler()
    ngrams = {
        "Japan": datetime(2011, 3, 11),
        "SuperBowl": datetime(2012, 2, 5),
        "Boston": datetime(2013, 4, 15),
        "Oscars": datetime(2014, 3, 2),
        "LGBTQ": datetime(2015, 6, 26),
        "2016 Election": datetime(2016, 11, 8),
        "Charlottesville": datetime(2017, 8, 12),
        "Kavanaugh": datetime(2018, 10, 5),
        "Hong Kong": datetime(2019, 11, 18),
    }

    vol = api.get_lang('en', start_time=datetime(2010, 1, 1), end_time=datetime(2020, 1, 1))

    trends = {}
    for k, date in ngrams.items():
        trends[date] = {}

        df = api.get_divergence(
            date=date,
            lang="en",
            ngrams=n,
        ).reset_index()

        df['ngram'] = df['ngram'].apply(cleanup)
        df = df[df['ngram'] != ''].fillna(0)

        ngrams[k] = df.groupby('ngram').agg({
            'rd_contribution': 'sum',
            'rd_contribution_no_rt': 'sum',
        })
        ngrams[k] = (ngrams[k] - ngrams[k].min()) / (ngrams[k].max() - ngrams[k].min())

        words_of_interest = ngrams[k].nlargest(nwords, 'rd_contribution').index.values
        for w in words_of_interest:
            if len(trends[date].keys()) == nwords:
                break

            print(f"Finding case sensitive permutations for: {w}")
            try:
                cases = list(case_permutations(w))
                cases = [w.lower(), w.capitalize(), w.title(), w.upper()] if len(cases) > 10**5 else cases

                rates = api.get_ngrams_array(
                    ngrams_list=cases,
                    start_time=date,
                    end_time=date,
                    lang='en'
                )
            except KeyError:
                continue

            word = rates[['count']].idxmax()[-1][-1]

            rates = rates.groupby('time').agg({
                'count': 'sum',
                'count_no_rt': 'sum',
                'freq': 'sum',
                'freq_no_rt': 'sum',
                'rank': 'min',
                'rank_no_rt': 'min',
            })
            r = (rates['count'] - rates['count_no_rt']) / rates['count']
            lang = (vol.loc[date, f'num_{n}'] - vol.loc[date, f'num_{n}_no_rt']) / vol.loc[date, f'num_{n}']
            rates['r_rel'] = (r / lang).replace([np.inf, -np.inf, np.nan], 1)

            trends[date][word] = {
                'r_rel': np.log10(rates['r_rel'].values[0]),
                'count': rates['count'].values[0],
                'count_no_rt': rates['count_no_rt'].values[0],
                'freq': rates['freq'].values[0],
                'freq_no_rt': rates['freq_no_rt'].values[0],
                'rank': rates['rank'].values[0],
                'rank_no_rt': rates['rank_no_rt'].values[0],
                'rd_contribution': ngrams[k].loc[w, 'rd_contribution'],
                'rd_contribution_no_rt': ngrams[k].loc[w, 'rd_contribution_no_rt'],
            }

        trends[date] = pd.DataFrame.from_dict(trends[date], orient='index')

    ngrams_vis.plot_trending_grid(
        f'{savepath}/trending_grid_{n}',
        trends,
    )
    print(f'Saved: {savepath}/trending_grid_{n}')


def topics(savepath, indir):
    """Plot topic-based subsets

    Args:
      savepath: path to save generated plot
      indir: path to dir of zipf dists

    """
    nwords = 50
    date = '2021-01-06'
    ignore = stopwords.words('english') + ['rt']

    def clean(text):
        """Remove non-latin, punctuation, links, and retweet prefix

        Args:
          text: 

        Returns:

        """
        cleaned = ""
        for t in text.split(' '):
            t = str(t).encode('ascii', 'ignore').decode('ascii').lower()
            t = "" if t in ignore else t
            t = re.sub(r">|<", "", t)
            t = re.sub(r"^\p{P}+$", "", t)
            t = re.sub(r"http\S+", "", t)
            t = re.sub(r"rt", "", t)

            if t == "":
                return ""
            else:
                cleaned = t if cleaned == "" else f"{cleaned} {t}"
        return cleaned

    zipf = {}
    for f in indir.rglob(f'*{date}.tsv'):
        label = str(f.name).split('_')[0]
        df = pd.read_csv(f, sep='\t', header=0, na_filter=False)[:10 ** 5]

        df['ngram'] = df['ngram'].apply(clean)
        df = df[df['ngram'] != ''].fillna(0)

        # add up counts and freqs
        df = df.groupby('ngram').agg({
            'count': 'sum',
            'count_no_rt': 'sum',
        })

        df['rank'] = df['count'].rank(method='average', ascending=False)
        df['rank_no_rt'] = df['count_no_rt'].rank(method='average', ascending=False)
        df['freq'] = df['count'] / df['count'].sum()
        df['freq_no_rt'] = df['count_no_rt'] / df['count_no_rt'].sum()
        df.sort_values(by='count', ascending=False, inplace=True)

        r = (df['count'] - df['count_no_rt']) / df['count']
        vol = (df['count'].sum() - df['count_no_rt'].sum()) / df['count'].sum()

        df['r_rel'] = r / vol
        df['r_rel'] = df['r_rel'].replace([np.inf, -np.inf, np.nan], 1)
        zipf[label] = df

    ref = 'BLM'
    soi = 'Twitter'

    arr = zipf[soi]['r_rel'].values[:nwords, None] - zipf[ref]['r_rel'].values[:nwords]
    df = pd.DataFrame(arr, index=zipf[ref].index[:nwords], columns=zipf[soi].index[:nwords])
    df.index.name = 'ref'
    df = df.stack().reset_index()
    df.columns = ['ref', 'soi', 'delta']

    ngrams_vis.plot_topics(
        f'{savepath}/topics',
        df,
    )
    print(f'Saved: {savepath}/topics')


def risk(datapath, savepath):
    df = pd.read_csv(
        datapath,
        header=0,
        index_col=0,
    )
    ngrams_vis.plot_risk_models(
        f'{savepath}/geopolitical_risk_models',
        df,
    )
    print(f'Saved: {savepath}/geopolitical_risk_models')


