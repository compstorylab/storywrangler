
import os
import json

import statsmodels.api as sm
from statsmodels.tsa.ar_model import AutoReg
from statsmodels.regression.rolling import RollingOLS

import numpy as np
import pandas as pd
from datetime import datetime
from pytrends.request import TrendReq
from pathlib import Path

from storywrangler.ngrams_query import Query

SEED = 2021
np.random.seed(SEED)

words_of_interest = [
    'revolution',
    'rebellion',
    'uprising',
    'coup',
    'overthrow',
    'unrest',
    'protests',
    'crackdown',
]


def load_json(data_path, data_files, pct_change=False):
    word_data_files = [x for x in os.listdir(data_path) if x.endswith('json')]
    word_classes = [x.strip('.json') for x in word_data_files]

    dfs = {c: None for c in word_classes}
    for i, f in enumerate(data_files):
        with open(data_path / f, 'r') as file_:
            words = json.load(file_)

        df = dict()
        for name, value in words['data'].items():
            value = np.array(value)
            df[name] = pd.Series(
                value[:, -1].astype(float),
                index=pd.to_datetime(value[:, 0]),
            )
        df = pd.concat(df, axis=1).fillna(method='ffill')
        if pct_change:
            df = df.apply(np.log).diff().dropna()
        dfs[word_classes[i]] = df

    '''
    word_data_files = [x for x in os.listdir(datapath) if x.endswith('json')]
    word_dfs = load_json(datapath, word_data_files)['basic_social_unrest']
    word_dfs.index = word_dfs.index.tz_localize(None)
    word_dfs = word_dfs.replace(to_replace=0, method='ffill')
    word_dfs = word_dfs.apply(np.log).diff().dropna().resample('M').mean()
    social_unrest_monthly_design = word_dfs.loc[start:end, words_of_interest]
    print(social_unrest_monthly_design)
    '''

    return dfs


def get_data(
    data_path,
    start_date=datetime(2010, 1, 1),
    end_date=datetime(2021, 1, 1),
):

    timescale = 'M'
    pytrends = TrendReq(
        hl='en-US',
        tz=-300,
        timeout=(3, 12),
        retries=1,
        backoff_factor=0.1,
        requests_args={'verify': False}
    )
    q = Query(f'1grams', 'en')

    tvnews = pd.read_csv(data_path / "news.csv", header=0, index_col='Time')
    tvnews['Query'] = tvnews['Query'].str.lower()
    tvnews['Query'] = tvnews['Query'].str.replace('"', '')
    tvnews['Query'] = tvnews['Query'].str.replace('text=', '')
    tvnews.index = pd.to_datetime(tvnews.index)

    df = None
    for w in words_of_interest:
        print(f"Retrieving: '{w}'")

        pytrends.build_payload(
            [w],
            timeframe=f"{start_date.strftime('%Y-%m-%d')} {end_date.strftime('%Y-%m-%d')}",
            geo='',
            gprop='',
        )
        gtrends = pytrends.interest_over_time()
        gtrends = pd.Series(0, index=pd.date_range(start_date, end_date)) if gtrends.empty else gtrends[w]
        gtrends = gtrends.fillna(0).replace(to_replace=0, method='ffill')
        gtrends = gtrends.apply(np.log).fillna(method='ffill').diff().dropna()
        gtrends = gtrends.resample(timescale).mean()
        gtrends = (gtrends - gtrends.mean()) / gtrends.std()

        twitter = q.query_insensitive_timeseries(w, start_time=start_date)
        twitter_at = twitter['count'].replace(to_replace=0, method='ffill')
        twitter_at = twitter_at.apply(np.log).fillna(method='ffill').diff().dropna()
        twitter_at = twitter_at.resample(timescale).mean()
        twitter_at = twitter_at.loc[gtrends.index]
        twitter_at = (twitter_at - twitter_at.mean()) / twitter_at.std()

        twitter_ot = twitter['count_no_rt'].replace(to_replace=0, method='ffill')
        twitter_ot = twitter_ot.apply(np.log).fillna(method='ffill').diff().dropna()
        twitter_ot = twitter_ot.resample(timescale).mean()
        twitter_ot = twitter_ot.loc[gtrends.index]
        twitter_ot = (twitter_ot - twitter_ot.mean()) / twitter_ot.std()

        news = tvnews[tvnews['Query'] == w]['Value']
        if news.empty:
            news = pd.Series(0, index=pd.date_range(start_date, end_date))
        else:
            news = pd.concat([news, pd.Series(0, index=pd.date_range(start_date, end_date))]).fillna(0)

        news = news.replace(to_replace=0, method='ffill')
        news = news.apply(np.log).fillna(method='ffill').diff().dropna()
        news = news.resample(timescale).mean()
        news = news.loc[gtrends.index]
        news = (news - news.mean()) / news.std()

        ts = pd.DataFrame(dict(
            ngram=w,
            twitter_at=twitter_at,
            twitter_ot=twitter_ot,
            gtrends=gtrends,
            news=news
        ))

        if df is not None:
            df = df.append(ts)
        else:
            df = ts

    df.to_csv(data_path / "words_timeseries.csv")


def get_monthly_gpr_data(data_path):
    geo = pd.read_excel(data_path / 'geopolitical_risk_index.xlsx').iloc[:425]
    geo.index = pd.to_datetime(geo.Date)
    gpr = geo.GPR
    return gpr


def get_gpr_data(datapath, start="2010", end="2019",):

    gpr = get_monthly_gpr_data(datapath)
    gpr = gpr.loc[start:end]
    gpr = gpr.apply(np.log).diff().dropna()
    gpr = gpr.iloc[1:]
    orig_ix = gpr.index
    gpr.index = pd.RangeIndex(len(gpr))
    return gpr, orig_ix


def get_design(
    datapath,
    m,
    words_of_interest,
    start="2010",
    end="2019",
):

    df = pd.read_csv(
        Path(datapath/"words_timeseries.csv"),
        header=0,
        index_col=0
    )
    df.index = pd.to_datetime(df.index)
    data = df.pivot(values=m, columns='ngram')
    data = data.loc[start:end, words_of_interest].iloc[:-1]
    if end == '2020': 
        # horrible hack to do things quickly -- 
        # we record data up to 2020-11-30, but gpr only to 2020-05-01
        # so cut panel at 2020-04-30
        data = data[:"2020-04-30"]
    # response is normalized, so we don't need to add constant
    data.index = pd.RangeIndex(len(data))
    return data


def gpr_model(datapath,):

    start, end = '2010', '2019'
    gpr, time_ix = get_gpr_data(datapath, start=start, end=end)

    ### fit AR model to GPR
    adf = sm.tsa.stattools.adfuller(gpr)
    with open(datapath / f"{end}/gpr-adf-after-logdiff.txt", "w") as f:
        adf_results = f"ADF: {adf[0]}, p = {adf[1]}\n"
        adf_results += f"Based on {adf[2]} lags."
        f.write(adf_results)
    gpr_ar_model = AutoReg(
        gpr, lags=list(range(1, adf[2] + 1))
    )
    gpr_ar_results = gpr_ar_model.fit()
    ar_summary = gpr_ar_results.summary()
    print(ar_summary)
    with open(datapath / f"{end}/ar-results.csv", "w") as f:
            f.write(ar_summary.as_csv())
    with open(datapath / f"{end}/ar-results.tex", "w") as f:
        f.write(ar_summary.as_latex())

    # now forecast on OOS
    gpr_oos, _ = get_gpr_data(datapath, start=start, end="2020")
    start_pred_ind = len(gpr)
    end_pred_ind = len(gpr_oos) - 1
    preds = gpr_ar_results.predict(
        start=start_pred_ind,
        end=end_pred_ind,
        dynamic=True,
    )
    mse = (preds - gpr_oos[start_pred_ind:]).pow(2).mean()
    with open(datapath / f"{end}/gpr-ar-pred-mse.txt", "w") as f:
        f.write(
            f"Pred MSE from ix = {start_pred_ind} to {end_pred_ind} = {mse}"
        )

    models = {
        'twitter_at': None,
        'twitter_ot': None,
        'gtrends': None,
        'news': None,
    }
    for m in models.keys():
        print(f"Model: {m}")

        data = get_design(
            datapath,
            m,
            words_of_interest,
            start=start,
            end=end,
        )
        ### Basic MLE OLS
        data_with_constant = sm.add_constant(data)
        model = sm.OLS(gpr, data_with_constant)
        results = model.fit()
        summary = results.summary()
        print(summary)
        with open(datapath / f"{end}/{m}-ols-results.csv", "w") as f:
            f.write(summary.as_csv())
        with open(datapath / f"{end}/{m}-ols-results.tex", "w") as f:
            f.write(summary.as_latex())

        # now confront AR model with exogenous design
        gpr_arx_model = AutoReg(
            gpr, lags=list(range(1, adf[2] + 1)),
            exog=data
        )
        gpr_arx_results = gpr_arx_model.fit()
        arx_summary = gpr_arx_results.summary()
        print(arx_summary)
        with open(datapath / f"{end}/{m}-arx-results.csv", "w") as f:
            f.write(arx_summary.as_csv())
        with open(datapath / f"{end}/{m}-arx-results.tex", "w") as f:
            f.write(arx_summary.as_latex())

        # rolling regression -- coefficient variation and pictures
        roll_ols = RollingOLS(
            gpr, data_with_constant, window=12
        )
        roll_res = roll_ols.fit()
        param_ts = roll_res.params
        param_ts.index = time_ix
        param_ts.to_csv(datapath / f"{end}/{m}-rolling-ols-parameters.csv")

        # now out of sample forcasting with exog
        data_oos = get_design(
            datapath,
            m,
            words_of_interest,
            start=start,
            end="2020",
        )
        start_pred_ind = len(gpr)
        end_pred_ind = len(gpr_oos) - 1
        preds = gpr_arx_results.predict(
            start=start_pred_ind,
            end=end_pred_ind,
            exog=data,
            exog_oos=data_oos[start_pred_ind:],
            dynamic=True,
        )
        mse = (preds - gpr_oos[start_pred_ind:]).pow(2).mean()
        with open(datapath / f"{end}/{m}-gpr-arx-pred-mse.txt", "w") as f:
            f.write(
                f"Pred MSE from ix = {start_pred_ind} to {end_pred_ind} = {mse}"
            )


if __name__ == '__main__':
    gpr_model(
        Path("../data/"),
    )
