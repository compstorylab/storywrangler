
import warnings
warnings.filterwarnings("ignore")

import argparse
from argparse import ArgumentDefaultsHelpFormatter
from operator import attrgetter
from datetime import datetime


class SortedMenu(ArgumentDefaultsHelpFormatter):
    def add_arguments(self, actions):
        actions = sorted(actions, key=attrgetter('option_strings'))
        super(SortedMenu, self).add_arguments(actions)


def parser():
    """Util function to parse command-line arguments"""

    return argparse.ArgumentParser(
        formatter_class=SortedMenu,
        description='StoryWrangler. \
        Copyright (c) 2020 The Computational Story Lab. Licensed under the MIT License;'
    )


def valid_date(d):
    try:
        return datetime.strptime(d, "%Y-%m-%d")
    except ValueError:
        raise argparse.ArgumentTypeError(f"Invalid date format in provided input: '{d}'")
