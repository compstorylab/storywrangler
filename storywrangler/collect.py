
import logging
import shutil
import sys
import time
import ujson
from pathlib import Path

from storywrangler import cli
from storywrangler import regexr
from storywrangler.utils import collect_ngrams


def parse_args(args, config):
    parser = cli.parser()

    parser.add_argument(
        'datapth',
        help='path to a [day] directory of compressed 15-minutes twitter files (ie. /tmp/2019-01-01)'
    )

    # optional args
    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['ngrams']),
        help='absolute Path to save network predictions'
    )

    parser.add_argument(
        '-n', '--ngrams',
        default=1,
        type=int,
        help='n-grams scheme to use'
    )

    parser.add_argument(
        '--codes',
        default=Path(config['language_codes']),
        help='path to (.json) language hashtable'
    )

    parser.add_argument(
        '--langs',
        default="en es pt ar ko fr id tr hi de it ru",
        type=str,
        help='a list of languages to parse out'
    )

    parser.add_argument(
        '--model',
        default=config['model'],
        help='absolute Path to FastText pre-trained model'
    )

    parser.add_argument(
        '--predictions',
        action='store_true',
        help='keep track of network predictions and save to _predictions.csv.gz'
    )

    parser.add_argument(
        '--emoji',
        action='store_true',
        help='download new codes for emojis from (https://www.unicode.org/) and re-compile regex to parse out ngrams'
    )

    parser.add_argument(
        '--score',
        default=float(config['model_threshold']),
        type=float,
        help='confidence score threshold for the language identifier'
    )

    parser.add_argument(
        '--anchor',
        default=None,
        type=Path,
        help='path to a json file of anchor ngrams to subsample tweets'
    )

    parser.add_argument(
        '--case_sensitive',
        default=False,
        type=bool,
        help='a toggle to get case sensitive ngrams'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)

    Path(config["realtime_logs"]).mkdir(parents=True, exist_ok=True)
    logfile = f'{config["realtime_logs"]}/n{args.ngrams}{Path(args.datapth).stem}.log'
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO, filename=logfile)
    print = logging.info
    logging.info('Initialized logging...')
    logging.info(args)

    if args.emoji:
        regexr.update_parsers(config['twitterlid'])

    eparser = regexr.get_emojis_parser(config['emoji_parser'])
    nparser = regexr.get_ngrams_parser(config['ngrams_parser'])
    outdir = Path(args.outdir)

    done = collect_ngrams(
        args.datapth,
        int(args.ngrams),
        str(args.model),
        eparser,
        nparser,
        outdir,
        args.score,
        args.codes,
        args.langs.split(' '),
        args.predictions,
        args.case_sensitive,
        args.anchor
    )

    if not done:
        print('Deleting temporary files... ')
        shutil.rmtree(outdir, ignore_errors=True)
        exit()

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec. ~ {args.outdir}')


if __name__ == "__main__":
    main()
