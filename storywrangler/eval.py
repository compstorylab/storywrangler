
import sys
if sys.platform == 'darwin':
    import matplotlib
    matplotlib.use("TkAgg")

import gzip
import ujson
import time
import random
import traceback
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
import fasttext

from storywrangler import utils
from storywrangler import regexr
from storywrangler import fileio
from storywrangler import consts
from storywrangler import eval_vis
from storywrangler import tweet_utils


def cmat(pth, out, languages, hashtbl, codes):
    """Calculate a confusion matrix comparing twitter vs. fasttext lang. labels

    Args:
      pth: path to a single (.tar.gz) file produced by (parser.py)
      out: path to output directory
      languages: a list of all supported languages
      hashtbl: a dict of language labels to remap
      codes: a dict of language label integer encodings

    Returns:
      saves a confusion_matrix to the desired directory

    """
    print('\nComputing confusion matrices:')
    start = time.time()

    pth = Path(str(pth) + '.tar.gz') if not str(pth).endswith('.tar.gz') else pth
    cols = ['twitter_label', 'fastText_label']

    df = tweet_utils.read_tarball(
        pth, filename='predictions',
        kwargs=dict(
            header=0,
            memory_map=True,
            compression='gzip',
            encoding='utf-8',
            sep=',',
            usecols=cols
        )
    )

    df = df.replace({v: k for k, v in codes.items()})
    df = df.replace(hashtbl)

    mat = confusion_matrix(
        df['twitter_label'].fillna(value='unknown').values,
        df['fastText_label'].fillna(value='unknown').values,
        languages
    )

    np.save(
        f"{out}/{pth.stem[:10]}.npy",
        mat
    )
    print(f'{time.time() - start:.2f} secs. | {out}/{pth.stem[:10]}.npy [{len(df)} tweets]')


def contagion_cmat(pth, out, languages, hashtbl, codes):
    """Calculate a confusion matrix comparing twitter vs. fasttext lang. labels

    Args:
      pth: path to a single (.tar.gz) file produced by (parser.py)
      out: path to output directory
      languages: a list of all supported languages
      hashtbl: a dict of language labels to remap
      codes: a dict of language label integer encodings

    Returns:
      saves a confusion_matrix to the desired directory

    """
    print('\nComputing confusion matrices:')
    start = time.time()

    pth = Path(str(pth) + '.tar.gz') if not str(pth).endswith('.tar.gz') else pth
    cols = ['twitter_label', 'fastText_label', 'rt']

    df = tweet_utils.read_tarball(
        pth, filename='predictions',
        kwargs=dict(
            header=0,
            memory_map=True,
            compression='gzip',
            encoding='utf-8',
            sep=',',
            usecols=cols
        )
    )

    df['twitter_label'] = df['twitter_label'].replace({v: k for k, v in codes.items()})
    df['twitter_label'] = df['twitter_label'].replace(hashtbl)

    df['fastText_label'] = df['fastText_label'].replace({v: k for k, v in codes.items()})
    df['fastText_label'] = df['fastText_label'].replace(hashtbl)

    df['rt'] = df['rt'].replace({2: 0})

    for t, k in {'ot': 0, 'rt': 1}.items():
        d = df[df.rt == k]
        mat = confusion_matrix(
                d['twitter_label'].fillna(value='unknown').values,
                d['fastText_label'].fillna(value='unknown').values,
                languages
            )

        np.save(
            f"{out}/{pth.stem[:10]}_{t}.npy",
            mat
        )

        print(f'{time.time() - start:.2f} secs. | {out}/{pth.stem[:10]}_{t}.npy [{len(df)} tweets]')


def confmat(files, savepath, languages, targets, hashtbl, resolution='D'):
    """Plot a heatmap comparing twitter to fasttext lang. labels

    Args:
      files: a list of (.npy) files
      savepath: path to save confusion matrix plot
      languages: np.array of all listed languages
      targets: a list of languages to parse out
      hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
      resolution: a time-scale to aggregate on (i.e. 'H', 'D', 'W', 'M', '6M', 'Y') (Default value = 'D')

    """
    print(f'\nLanguages [{len(languages)}]: {languages}')
    print(f'\nTargets [{len(targets)}]: {targets}')
    print('\nAggregating confusion matrices:')
    languages = np.array(languages, dtype=object)
    targets = np.array(targets, dtype=object)
    codes = np.array(targets, dtype=object)
    dates = [datetime.strptime(f.stem, '%Y-%m-%d') for f in files]
    df = pd.DataFrame(files, index=dates, columns=['path'])
    groups = df.resample(resolution)
    date_ranges = [[timestamp.to_pydatetime(), list(g.path.values)] for timestamp, g in groups.__iter__()]

    mask = np.outer(np.in1d(languages, targets), np.in1d(languages, targets).T)

    for i, lang in enumerate(languages[np.where(np.in1d(languages, targets))[0]]):
        try:
            targets[i] = hashtbl[hashtbl[:, 1] == lang, 0][0]
            codes[i] = lang
        except IndexError:
            targets[i] = hashtbl[hashtbl[:, 2] == lang, 0][0]
            codes[i] = lang

    for date, files in date_ranges:
        cm = np.zeros((len(languages), len(languages)), dtype=float)
        for m in files:
            m = np.load(m).astype(float)
            cm += m

        # normalize matrix
        cm = cm / (1 + cm.sum(axis=1)[:, np.newaxis])
        cm = cm[mask].reshape((len(targets), len(targets)))

        eval_vis.plot_confmat(
            cm,
            targets,
            codes,
            f'{savepath}/{date.strftime("%Y-%m-%d")}_confmat'
        )
        print(f'Saved: {savepath}/{date.strftime("%Y-%m-%d")}_confmat')
        print('-'*25)


def contagion_confmat(files, savepath, languages, targets, hashtbl, resolution='D'):
    """Plot a heatmap comparing twitter to fasttext lang. labels

    Args:
      files: a list of (.npy) files
      savepath: path to save confusion matrix plot
      languages: np.array of all listed languages
      targets: a list of languages to parse out
      hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
      resolution: a time-scale to aggregate on (i.e. 'H', 'D', 'W', 'M', '6M', 'Y') (Default value = 'D')

    """
    print(f'\nLanguages [{len(languages)}]: {languages}')
    print(f'\nTargets [{len(targets)}]: {targets}')
    print('\nAggregating confusion matrices:')
    languages = np.array(languages, dtype=object)
    targets = np.array(targets, dtype=object)
    codes = np.array(targets, dtype=object)
    dates = [datetime.strptime(f.stem.split('_')[0], '%Y-%m-%d') for f in files]
    df = pd.DataFrame(files, index=dates, columns=['path'])
    groups = df.resample(resolution)
    date_ranges = [[timestamp.to_pydatetime(), list(g.path.values)] for timestamp, g in groups.__iter__()]

    mask = np.outer(np.in1d(languages, targets), np.in1d(languages, targets).T)

    for i, lang in enumerate(languages[np.where(np.in1d(languages, targets))[0]]):
        try:
            targets[i] = hashtbl[hashtbl[:, 1] == lang, 0][0]
            codes[i] = lang
        except IndexError:
            targets[i] = hashtbl[hashtbl[:, 2] == lang, 0][0]
            codes[i] = lang

    for date, files in date_ranges:
        cm = np.zeros((len(languages), len(languages)), dtype=float)
        for m in files:
            t = m.stem.split('_')[1]
            m = np.load(m).astype(float)
            cm += m

        # normalize matrix
        cm = cm / (1 + cm.sum(axis=1)[:, np.newaxis])
        cm = cm[mask].reshape((len(targets), len(targets)))

        eval_vis.plot_confmat(
            cm,
            targets,
            codes,
            f'{savepath}/{date.strftime("%Y-%m-%d")}_confmat_{t}'
        )
        print(f'Saved: {savepath}/{date.strftime("%Y-%m-%d")}_confmat_{t}')
        print('-'*25)


def test(path, savepath, model, emoji_parser, ngrams_parser, threshold, n):
    """Compare a random sample of twitter/fasttext lang. labels

    Args:
      path: path to a single raw tweets file (.gz)
      savepath: path to output file
      model: path to a fastText model
      emoji_parser: a compiled regular expression that matches emojis
      ngrams_parser: a compiled regular expression that matches ngrams
      threshold: confidence score threshold for FastText LID
      n: number of samples

    Returns:
      saves a json file containing
      `N` x [tweet_text, 'twitter_label', 'fastText_label', 'fastText_conf_score']
      for each language

    """
    print('Evaluating tweets...')
    predictions = []
    clf = fasttext.load_model(model)
    with gzip.open(path, 'rb') as data:
        for act in data:
            if act.isspace(): continue  # filter out empty tweets
            try:
                twt_id, usr_id, tw_lang, text, retweet = utils.parse_tweet(ujson.loads(act))
            except ValueError:
                continue

            if twt_id is not None and text is not None:
                try:
                    text = regexr.remove_whitespaces(text)
                    emoji_freezone = emoji_parser.sub(r'', regexr.filter_text(text))
                    emoji_freezone = regexr.remove_whitespaces(emoji_freezone)
                    ft_lang, conf = utils.predict_lang(clf.predict(emoji_freezone), threshold)

                    predictions.append({
                        'tweet_id': twt_id,
                        'user_id': usr_id,
                        'retweet': retweet,
                        'raw_text': text,
                        'filtered_text': emoji_freezone,
                        'twitter_label': tw_lang,
                        'fastText_label': ft_lang,
                        'fastText_conf_score': conf,
                        '1grams': list(regexr.ngrams(text, parser=ngrams_parser, n=1)),
                        '2grams': list(regexr.ngrams(text, parser=ngrams_parser, n=2)),
                        '3grams': list(regexr.ngrams(text, parser=ngrams_parser, n=3))
                    })
                except TypeError:
                    traceback.print_exc()

    fileio.save_json(
        random.sample(predictions, n),
        savepath
    )
    print(f'Saved: {savepath}')


def gain(pth, out, languages):
    """Compute gain statistics for ngrams (all // fresh)

    Args:
      pth: path to a single (.tar.gz) file produced by (parser.py)
      out: path to output directory
      languages: a list of languages to use

    """

    def calc_stats(s):
        """

        Args:
          s: 

        Returns:

        """
        avg_gain = s.mean() if not np.isnan(s.mean()) else -1
        std_gain = s.std() if not np.isnan(s.std()) else -1
        min_gain = s.min() if not np.isnan(s.min()) else -1
        gain_25 = s.quantile(.25) if not np.isnan(s.quantile(.25)) else -1
        med_gain = s.median() if not np.isnan(s.median()) else -1
        gain_75 = s.quantile(.75) if not np.isnan(s.quantile(.75)) else -1
        max_gain = s.max() if not np.isnan(s.max()) else -1

        return {
            'avg_gain': avg_gain,
            'std_gain': std_gain,
            'min_gain': min_gain,
            '25_gain': gain_25,
            'med_gain': med_gain,
            '75_gain': gain_75,
            'max_gain': max_gain,
        }

    print('\nComputing gain values:')
    start = time.time()

    pth = Path(str(pth) + '.tar.gz') if not str(pth).endswith('.tar.gz') else pth
    date = pth.name.split('.')[0]
    stats = {
        'count': {},
        'freq': {},
        'random': {},
        'freq_weighted_random': {},
        'freq_fresh_weighted_random': {},
    }

    for lang in languages:
        try:
            ngrams = tweet_utils.read_tarball(
                pth, filename=f'grams/{lang}_',
                kwargs=dict(
                    header=0,
                    sep='\t',
                    na_filter=False,
                    compression='gzip',
                    low_memory=True,
                    memory_map=True,
                    index_col=0,
                    usecols=[
                        'ngram',
                        'count', 'count_no_rt',
                        'rank', 'rank_no_rt',
                        'freq', 'freq_no_rt'
                    ]
                )
            )

            if ngrams.shape[0] <= 2:
                raise Exception(f'Warning: no ngrams found for [{lang}]')

            ngrams['gain_count'] = ngrams['count'] / ngrams['count_no_rt']
            ngrams['gain_count'] = ngrams['gain_count'].replace([np.inf, -np.inf], 1)

            ngrams['gain_freq'] = ngrams['freq'] / ngrams['freq_no_rt']
            ngrams['gain_freq'] = ngrams['gain_freq'].replace([np.inf, -np.inf], 1)

            samples = ngrams.sample(
                frac=.1,
            ).sort_values(by='count', ascending=False)

            freq_weighted_random = ngrams.sample(
                frac=.1, weights=ngrams['freq']
            ).sort_values(by='count', ascending=False)

            freq_fresh_weighted_random = ngrams.sample(
                frac=.1, weights=ngrams['freq_no_rt']
            ).sort_values(by='count', ascending=False)

            stats['count'][lang] = calc_stats(ngrams['gain_count'])
            stats['freq'][lang] = calc_stats(ngrams['gain_freq'])
            stats['random'][lang] = calc_stats(samples['gain_count'])
            stats['freq_weighted_random'][lang] = calc_stats(freq_weighted_random['gain_count'])
            stats['freq_fresh_weighted_random'][lang] = calc_stats(freq_fresh_weighted_random['gain_count'])

            samples.to_csv(
                f'{out}/{lang}_random_{date}.tsv.gz', sep='\t'
            )
            freq_weighted_random.to_csv(
                f'{out}/{lang}_freq_weighted_random_{date}.tsv.gz', sep='\t'
            )
            freq_fresh_weighted_random.to_csv(
                f'{out}/{lang}_freq_fresh_weighted_random_{date}.tsv.gz', sep='\t'
            )

        except Exception:
            print(f'Warning: language [{lang}] was not found!')
            continue

    fileio.save_json(stats, f'{out}/{date}.json')
    print(f'{time.time() - start:.2f} secs. | {out}/{date}.json')


def diagnosis(path, outdir, p_language_codes):
    """Create a dataframe of language usage for a given day

    Args:
      path: path to predictions (.csv/.tar) file
      outdir: path to output
      p_language_hashtable: path to a dict of language codes
      p_language_codes: 

    """
    language_codes = fileio.load_json(p_language_codes)
    cols = ['id', 'twitter_label', 'fastText_label', 'fastText_conf_score', 'user_id', 'rt', 'length']

    df = tweet_utils.read_tarball(
        path, filename='predictions',
        kwargs=dict(
            header=0,
            memory_map=True,
            compression='gzip',
            encoding='utf-8',
            sep=',',
            usecols=cols,
            index_col='id'
        )
    )
    lens = {'short': 0, 'long': 1}
    conf_levels = {'und': -1, 'low': 0, 'mid': 1, 'high': 2}

    def check_conf(x):
        if x <= .25:
            return conf_levels['und']
        elif x <= .5:
            return conf_levels['low']
        elif x <= .75:
            return conf_levels['mid']
        else:
            return conf_levels['high']

    df['long'] = (df.length > 140).map({True: 1, False: 0})
    df['conf'] = df['fastText_conf_score'].apply(check_conf)
    print(f'MEM={utils.df_mem_usage(df)} Mb.')

    d = df.groupby(['rt', 'long', 'twitter_label'], as_index=False) \
        .size().to_frame(name='count')

    tw = df.twitter_label.value_counts().to_frame(name='count')

    for tt, v in consts.tweet_types.items():
        for length in lens:
            try:
                tw[f'{length}_{tt}s'] = d.xs((v, lens[length]))
            except KeyError:
                tw[f'{length}_{tt}s'] = 0

    tw.index = tw.index.map({v: k for k, v in language_codes.items()})

    d = df.groupby(['rt', 'long', 'conf', 'fastText_label'], as_index=False) \
        .size().to_frame(name='count')

    ft = df.fastText_label.value_counts().to_frame(name='count')

    for tt, v in consts.tweet_types.items():
        for length in lens:
            for conf in conf_levels:
                try:
                    ft[f'{length}_{tt}s_{conf}'] = d.xs((v, lens[length], conf_levels[conf]))
                except KeyError:
                    ft[f'{length}_{tt}s_{conf}'] = 0

    ft.index = ft.index.map({v: k for k, v in language_codes.items()})

    del df

    if len(tw) > 0:
        tw['rank'] = tw['count'].rank(method='average', ascending=False)
        total = tw['count'].sum()
        tw['freq'] = tw['count'] / total
    else:
        print('Warning: no twitter labels!')

    if len(ft) > 0:
        ft['rank'] = ft['count'].rank(method='average', ascending=False)
        total = ft['count'].sum()
        ft['freq'] = ft['count'] / total
    else:
        print('Warning: no fastText labels!')

    ft = ft.add_prefix('ft_')
    tw = tw.add_prefix('tw_')
    df = pd.concat([ft, tw], axis=1, join='outer', sort=False).sort_values('ft_count', ascending=False)
    df.index.name = 'language'

    fname = path.name.split(".")[0]
    df.to_csv(outdir/f'{fname}.csv.gz')
    print(f'{outdir/f"{fname}.csv.gz"} | MEM={utils.df_mem_usage(df)} Mb.')


def aggregate(paths, savepath):
    """Combine a list of dataframes

    Args:
      paths: a list of paths to csv file(s)
      savepath: path to dataframe

    Returns:
      a dataframe

    """

    def load_tbl(path):
        path = Path(path)
        print(f'Processing: {path}')
        df = pd.read_csv(path, sep=',', header=0)

        try:
            df['date'] = datetime.strptime(path.stem.split('_')[0], '%Y-%m-%d')
        except ValueError:
            df['date'] = datetime.strptime(path.stem.split('.')[0], '%Y-%m-%d')
        return df

    df = load_tbl(paths[0])
    for p in paths[1:]:
        try:
            d = load_tbl(p)
            df = df.merge(d, how='outer')
        except Exception:
            continue

    df = df.set_index('date')
    df.to_csv(savepath)
    print(f'Saved: {savepath}')
    return df


def tweet_length(path, outdir, p_language_codes, hashtbl):
    """Create a dataframe of language usage for a given day

    Args:
      path: path to predictions (.csv/.tar) file
      outdir: path to output
      p_language_codes: path to a dict of language codes
      hashtbl: a dict of language labels to remap

    """
    language_codes = fileio.load_json(p_language_codes)
    cols = ['id', 'twitter_label', 'fastText_label', 'fastText_conf_score', 'user_id', 'rt', 'length']
    lid, lens = {}, {}

    for f in path.glob('*.tar.gz'):
        date = f.name.split(".")[0]
        df = tweet_utils.read_tarball(
            f, filename='predictions',
            kwargs=dict(
                header=0,
                memory_map=True,
                compression='gzip',
                encoding='utf-8',
                sep=',',
                usecols=cols,
                index_col='id'
            )
        )

        df.twitter_label = df.twitter_label.map({v: k for k, v in language_codes.items()})
        df.fastText_label = df.fastText_label.map({v: k for k, v in language_codes.items()})
        df = df.replace(hashtbl)

        df = df[df['twitter_label'] != 'unknown']
        df = df[df['fastText_label'].isin(consts.top10)]
        df['match'] = df['twitter_label'].eq(df['fastText_label'])

        print(f"{date}: ~{int(round(df.shape[0] / 10**6, 0))} Million Messages")
        stats = df.groupby(['fastText_label', 'match'], as_index=False).size().to_frame(name=date)
        print(stats)
        stats /= df.groupby(['fastText_label'], as_index=False).size().to_frame(name=date)
        print(stats.round(4))

        lid[date] = df.groupby(['match', 'length'], as_index=False).size().to_frame(name=date).loc[False]
        lens[date] = df.groupby('length', as_index=False).size().to_frame(name=date)

    eval_vis.plot_mismatches(
        pd.concat(lid.values(), axis=1).fillna(value=0),
        pd.concat(lens.values(), axis=1).fillna(value=0),
        outdir/'mismatches'
    )


def agreement(df, savepath, hashtbl):
    """Saves a shift plot comparing twitter to fasttext lang. labels

    Args:
      df: a dataframe of daily language usage on Twitter
      hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
      savepath: path to save plot

    """

    def decode_langs(d):
        d.rename(  # FastText codes
            index={code: f'{label}' for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
            inplace=True
        )

        d.rename(  # Twitter codes
            index={code: f'{label}' for label, code in zip(hashtbl[:, 0], hashtbl[:, 2])},
            inplace=True
        )
        return d

    length = ['short', 'long']
    ttype = ['tweets', 'retweets', 'comments']
    conf_levels = ['und', 'low', 'mid', 'high']

    for c in conf_levels:
        for t in ttype:
            df[f'ft_{t}_{c}'] = 0
            for l in length:
                df[f'ft_{t}_{c}'] += df[f'ft_{l}_{t}_{c}']

    for t in ttype:
        df[f'ft_{t}'] = 0
        df[f'tw_{t}'] = 0
        for l in length:
            df[f'ft_{l}_{t}'] = 0
            df[f'tw_{t}'] += df[f'tw_{l}_{t}']
            for c in conf_levels:
                df[f'ft_{t}'] += df[f'ft_{l}_{t}_{c}']
                df[f'ft_{l}_{t}'] += df[f'ft_{l}_{t}_{c}']

    y1 = '2017-10'
    y2 = '2017-12'
    df1 = pd.DataFrame(df.loc[y1].groupby(['language']).agg("sum")).loc[consts.top10]
    df2 = pd.DataFrame(df.loc[y2].groupby(['language']).agg("sum")).loc[consts.top10]

    eval_vis.plot_agreement((y1, decode_langs(df1)), (y2, decode_langs(df2)), f'{savepath}/agreement')
    print(f'Saved: {savepath}/agreement')

    ftlangs = df.loc['2016-09':'2019-09', ['language', 'ft_count']]
    ftlangs['language'] = ftlangs.language.apply(lambda x: x if x in consts.defaultLangs else 'other')
    ftlangs = ftlangs.groupby(['date', 'language']).sum().reset_index()
    ftlangs.set_index('date', inplace=True)
    ftlangs = ftlangs.pivot(columns='language', values='ft_count').fillna(value=0)
    ftlangs = ftlangs.loc[:, consts.defaultLangs]
    ftlangs = ftlangs.resample('W').mean()
    ftlangs = ftlangs.divide(ftlangs.sum(axis=1), axis=0)

    fttypes = df.loc['2016-09':'2019-09']
    fttypes = fttypes.groupby(['date']).sum().reset_index()
    fttypes.set_index('date', inplace=True)
    fttypes = fttypes.resample('W').mean()
    fttypes = fttypes.div(fttypes.ft_count, axis=0)

    eval_vis.plot_usage(ftlangs, fttypes, hashtbl, f'{savepath}/usage')
    print(f'Saved: {savepath}/usage')


def error(path, savepath, languages, hashtbl):
    """Compute margin of errors for the contagion ratio based on agreement between fasttext and twitter

    Args:
      path: path to a list of (.npy) files
      savepath: path to save confusion matrix plot
      languages: np.array of all listed languages
      hashtbl: a list of languages and their corresponding codes in both FastText and Twitter

    """
    print(f'\nLanguages [{len(languages)}]: {languages}')
    languages = np.array(languages, dtype=object)
    codes = np.array(languages, dtype=object)
    files = [p for p in path.glob('*.npy')]
    dates = [datetime.strptime(f.stem.split('_')[0], '%Y-%m-%d') for f in files]

    for i, lang in enumerate(languages[np.where(np.in1d(languages, languages))[0]]):
        try:
            languages[i] = hashtbl[hashtbl[:, 1] == lang, 0][0]
            codes[i] = lang
        except IndexError:
            languages[i] = hashtbl[hashtbl[:, 2] == lang, 0][0]
            codes[i] = lang

    ind = pd.MultiIndex.from_product([dates, codes], names=(u'date', u'language'))
    df = pd.DataFrame(index=ind, columns=['error'])

    for date, f in zip(dates, files):
        m = np.load(f).astype(float)
        m = m / (1 + m.sum(axis=1)[:, np.newaxis])
        df.loc[(date, codes), 'agreement'] = m.diagonal()

    df.to_csv(f'{savepath}.csv.gz')
    print(f'{savepath}.csv.gz | MEM={utils.df_mem_usage(df)} Mb.')


def val(df, savepath, hashtbl):
    """
    Args:
      df: a dataframe of daily language usage on Twitter
      hashtbl: a list of languages and their corresponding codes in both FastText and Twitter
      savepath: path to save plot

    """
    df = df.reset_index().set_index('date')
    df.index = pd.to_datetime(df.index)

    lang_ratio = df.loc['2014':'2019']
    lang_ratio['OT'] = lang_ratio.ft_tweets + lang_ratio.ft_comments
    lang_ratio['RT'] = lang_ratio.ft_retweets

    lang_ratio['ratio'] = lang_ratio['RT'] / lang_ratio['OT']
    lang_ratio['adj_ratio'] = lang_ratio.agreement * (lang_ratio['RT'] / lang_ratio['OT'])
    lang_ratio['diff'] = abs(lang_ratio['ratio'] - lang_ratio['adj_ratio'])

    lang_ratio = lang_ratio.loc[:, ['language', 'diff']]
    lang_ratio = lang_ratio.pivot(columns='language', values='diff')
    lang_ratio = lang_ratio.resample('Y').mean()
    lang_ratio.index = lang_ratio.index.strftime('%Y')
    lang_ratio = lang_ratio.loc[:, consts.langsOfInterest]
    lang_ratio = lang_ratio.replace([np.inf, -1. * np.inf, np.nan], 0)
    lang_ratio.rename(
        columns={code: label for label, code in zip(hashtbl[:, 0], hashtbl[:, 1])},
        inplace=True
    )
    lang_ratio = lang_ratio.T

    print(lang_ratio)
    print('-'*75)
    print(lang_ratio.sort_values('2019').head(10).round(2).to_latex())
    print(lang_ratio.sort_values('2019').tail(10).round(2).to_latex())

    eval_vis.plot_val_heatmap(
        f'{savepath}/val_heatmap',
        lang_ratio
    )
    print(f'Saved: {savepath}/val_heatmap')


