
import logging
import sys
import time
import ujson
from pathlib import Path

from storywrangler import cli
from storywrangler.utils import aggregate_ngrams


def parse_args(args, config):
    parser = cli.parser()

    parser.add_argument(
        'indir',
        help='path to daily compressed tarballs (see `parser.py`)'
    )

    # optional args
    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['ngrams']),
        help='absolute path to save aggregated ngrams'
    )

    parser.add_argument(
        '-t', '--time',
        default='W',
        type=str,
        help="a time-scale to aggregate on (eg, 'W', 'M', 'Y')"
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)

    Path(config["logs"]).mkdir(parents=True, exist_ok=True)
    logfile = f'{config["logs"]}/{args.time}{Path(args.indir).stem}.log'
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO, filename=logfile)
    print = logging.info
    logging.info('Initialized logging...')
    logging.info(args)

    indir = Path(args.indir)
    outdir = Path(args.outdir) / args.time
    outdir.mkdir(parents=True, exist_ok=True)

    aggregate_ngrams(
        indir,
        outdir,
        args.time
    )

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec. ~ {outdir}')


if __name__ == "__main__":
    main()
