
import sys
import time
from pathlib import Path
import pandas as pd
import ujson

from storywrangler import cli
from storywrangler import consts
from storywrangler import ngrams_analytics
from storywrangler import ngrams_studies
from storywrangler import regexr


def parse_args(args, config):
    parser = cli.parser()

    # optional subparsers
    subparsers = parser.add_subparsers(help='Arguments for specific action.', dest='dtype')
    subparsers.required = True

    zipf_parser = subparsers.add_parser(
        'zipf',
        help='Plot zipf distribution for [N]-grams for a given language'
    )
    zipf_parser.add_argument(
        'datapath',
        help='path to ngrams directory (ie. /tmp/ngrams)'
    )

    collect_stats_parser = subparsers.add_parser(
        'collect_stats',
        help='generate a dataframe of ngram statistics overtime'
    )
    collect_stats_parser.add_argument(
        'datapath',
        help='path to ngram dir (ie. /tmp/ngrams)',
    )

    stats_parser = subparsers.add_parser(
        'stats',
        help='plot ngram statistics overtime'
    )
    stats_parser.add_argument(
        'datapath',
        help='path to stats csv file (ie. /tmp/ngrams.csv)',
    )

    risk_parser = subparsers.add_parser(
        'risk',
        help='run risk models'
    )
    risk_parser.add_argument(
        'datapath',
        help='path to risk data dir (ie. /tmp/risk_data)',
    )

    flipbook_parser = subparsers.add_parser(
        'flipbook',
        help='combine PDFs into a single flipbook'
    )
    flipbook_parser.add_argument(
        'datapath',
        help='path to a dir of pdfs (ie. /tmp/pdfs)',
    )

    studies_parser = subparsers.add_parser(
        'studies',
        help='plot median age of famous figures in the last century'
    )
    studies_parser.add_argument(
        'pantheon',
        help='path to pantheon dataset file (ie. /tmp/figures.tsv.gz)',
    )
    studies_parser.add_argument(
        'risk',
        help='path to risk data dir (ie. /tmp/risk_data)',
    )
    studies_parser.add_argument(
        'movies',
        help='path to movie data dir (ie. /tmp/movie_data)',
    )

    pantheon_figures_parser = subparsers.add_parser(
        'pantheon_figures',
        help='plot rank of famous individuals on twitter'
    )
    pantheon_figures_parser.add_argument(
        'pantheon',
        help='path to pantheon dataset file (ie. /tmp/figures.tsv.gz)',
    )

    pantheon_age_parser = subparsers.add_parser(
        'pantheon_age',
        help='plot KDE of rank and age of famous individuals on twitter'
    )
    pantheon_age_parser.add_argument(
        'pantheon',
        help='path to pantheon dataset file (ie. /tmp/figures.tsv.gz)',
    )

    subparsers.add_parser(
        'grid',
        help='plot a grid of ngram timeseries'
    )

    subparsers.add_parser(
        'chart',
        help='Plot a grid of ngrams timeseries'
    )

    subparsers.add_parser(
        'perspective',
        help='Plot a grid of ngrams timeseries'
    )

    subparsers.add_parser(
        'trending',
        help='Plot narratively trending ngrams'
    )

    topics_parser = subparsers.add_parser(
        'topics',
        help='Plot topic-based subsets'
    )
    topics_parser.add_argument(
        'zipfs',
        help='path to a dir of zipf dists',
    )

    subparsers.add_parser(
        'trending_grid',
        help='Plot a grid of narratively trending ngrams'
    )

    compare = subparsers.add_parser(
        'compare',
        help='Compare ngrams timeseries with gtrends and tvnews'
    )
    compare.add_argument('tvnews', help='path to tvnews .csv file')

    # optional args
    parser.add_argument(
        '-n', '--ngrams',
        default=1,
        type=int,
        help='n-grams scheme to use'
    )

    parser.add_argument(
        '--date',
        default=None,
        help='date to insert into '
    )

    parser.add_argument(
        '-r', '--resolution',
        default='D',
        help="group days based on a given timescale [i.e. 'D', 'W', 'M', '6M', 'Y']"
    )

    parser.add_argument(
        '-t', '--targets',
        type=str,
        default=consts.reachedtop10,
        help='list of languages to use (i.e. "en es ja")'
    )

    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['plots']) / 'ngrams',
        help='absolute Path to save figures'
    )

    parser.add_argument(
        '-c', '--cachedir',
        default=Path(config['datacache']) / 'ngrams',
        help='absolute Path to save figures'
    )

    parser.add_argument(
        '-e', '--emoji',
        help='include emoji component (when available)',
        action='store_true'
    )

    parser.add_argument(
        '-l', '--lite',
        help='run the lite version of a given function, generally reducing space complexity',
        action='store_true'
    )

    parser.add_argument(
        '--target_date',
        help='target date for comparison plot',
        default=None
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p / 'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)
    Path(args.outdir).mkdir(parents=True, exist_ok=True)
    languages = [lang for lang in args.targets.split()] if type(args.targets) == str else args.targets

    supported_languages = pd.read_csv(config['supported_languages'], header=0).fillna(value='unknown')
    supported_languages.set_index('FastText', inplace=True)
    supported_languages.drop(columns='Twitter', inplace=True)
    supported_languages = supported_languages.to_dict()['Language']
    supported_languages.update({'und': 'Undefined'})

    if args.dtype == 'flipbook':
        ngrams_analytics.flipbook(
            savepath=Path(args.outdir),
            datapath=Path(args.datapath),
        )

    elif args.dtype == 'collect_stats':
        ngrams_analytics.collect_stats(
            datapath=Path(args.datapath),
            savepath=Path(args.outdir)
        )

    elif args.dtype == 'stats':
        ngrams_analytics.stats(
            datapath=Path(args.datapath),
            savepath=Path(args.outdir)
        )

    elif args.dtype == 'risk':
        ngrams_analytics.risk(
            datapath=Path(args.datapath),
            savepath=Path(args.outdir),
        )

    elif args.dtype == 'studies':
        nparser = regexr.get_ngrams_parser(config['ngrams_parser'])

        ngrams_studies.studies(
            pantheon=Path(args.pantheon),
            risk=Path(args.risk),
            movies=Path(args.movies),
            nparser=nparser,
            savepath=Path(args.outdir),
        )

    elif args.dtype == 'pantheon_figures':
        ngrams_studies.pantheon_figures(
            pantheon=Path(args.pantheon),
            savepath=Path(args.outdir),
        )

    elif args.dtype == 'pantheon_age':
        ngrams_studies.pantheon_age(
            pantheon=Path(args.pantheon),
            savepath=Path(args.outdir),
        )

    elif args.dtype == 'zipf':
        eparser = regexr.get_emojis_parser(config['emoji_parser'])

        ngrams_analytics.zipf(
            path=Path(args.datapath),
            date='2020-05-01',
            savepath=Path(args.outdir),
            eparser=eparser,
            lang='en'
        )

    elif args.dtype == 'grid':
        ngrams_analytics.ngrams_grid(
            savepath=Path(args.outdir),
            lang_hashtbl=supported_languages
        )

    elif args.dtype == 'chart':
        nparser = regexr.get_ngrams_parser(config['ngrams_parser'])

        ngrams_analytics.chart(
            savepath=Path(args.outdir),
            lang_hashtbl=supported_languages,
            nparser=nparser,
        )

    elif args.dtype == 'perspective':
        nparser = regexr.get_ngrams_parser(config['ngrams_parser'])

        ngrams_analytics.perspective(
            savepath=Path(args.outdir),
            lang_hashtbl=supported_languages,
            nparser=nparser,
        )

    elif args.dtype == 'compare':
        nparser = regexr.get_ngrams_parser(config['ngrams_parser'])

        ngrams_analytics.compare(
            savepath=Path(args.outdir),
            tvnews=Path(args.tvnews),
            lang_hashtbl=supported_languages,
            nparser=nparser,
        )

    elif args.dtype == 'topics':
        ngrams_analytics.topics(savepath=Path(args.outdir), indir=Path(args.zipfs))

    elif args.dtype == 'trending':
        ngrams_analytics.trending(savepath=Path(args.outdir))

    elif args.dtype == 'trending_grid':
        ngrams_analytics.trending_grid(savepath=Path(args.outdir))

    else:
        print('Error: unknown action!')

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec.')


if __name__ == "__main__":
    main()
