
import argparse

import torch
import pyro
import pyro.distributions as dist
import arviz

import numpy as np
import pandas as pd
from scipy import stats
from datetime import datetime
from pytrends.request import TrendReq
from pathlib import Path


from storywrangler import ngrams_vis
from storywrangler.ngrams_query import Query

SEED = 2021
torch.manual_seed(SEED)
pyro.set_rng_seed(SEED)

words_of_interest = [
    'revolution',
    'rebellion',
    'uprising',
    'coup',
    'overthrow',
    'unrest',
    'protests',
    'crackdown',
]


def get_data(
    data_path,
    start_date=datetime(2009, 1, 1),
    end_date=datetime(2021, 1, 1),
):

    timescale = 'M'
    pytrends = TrendReq(
        hl='en-US',
        tz=-300,
        timeout=(3, 12),
        retries=1,
        backoff_factor=0.1,
        requests_args={'verify': False}
    )
    q = Query(f'1grams', 'en')

    tvnews = pd.read_csv(data_path / "news.csv", header=0, index_col='Time')
    tvnews['Query'] = tvnews['Query'].str.lower()
    tvnews['Query'] = tvnews['Query'].str.replace('"', '')
    tvnews['Query'] = tvnews['Query'].str.replace('text=', '')
    tvnews.index = pd.to_datetime(tvnews.index)

    df = None
    for w in words_of_interest:
        print(f"Retrieving: '{w}'")

        pytrends.build_payload(
            [w],
            timeframe=f"{start_date.strftime('%Y-%m-%d')} {end_date.strftime('%Y-%m-%d')}",
            geo='',
            gprop='',
        )
        gtrends = pytrends.interest_over_time()
        gtrends = pd.Series(0, index=pd.date_range(start_date, end_date)) if gtrends.empty else gtrends[w]
        gtrends = gtrends.fillna(0).replace(to_replace=0, method='ffill')
        gtrends = gtrends.apply(np.log).fillna(method='ffill').diff().dropna()
        gtrends = gtrends.resample(timescale).mean()
        gtrends = (gtrends - gtrends.mean()) / gtrends.std()

        twitter = q.query_insensitive_timeseries(w, start_time=start_date)
        twitter_at = twitter['count'].replace(to_replace=0, method='ffill')
        twitter_at = twitter_at.apply(np.log).fillna(method='ffill').diff().dropna()
        twitter_at = twitter_at.resample(timescale).mean()
        twitter_at = twitter_at.loc[gtrends.index]
        twitter_at = (twitter_at - twitter_at.mean()) / twitter_at.std()

        twitter_ot = twitter['count_no_rt'].replace(to_replace=0, method='ffill')
        twitter_ot = twitter_ot.apply(np.log).fillna(method='ffill').diff().dropna()
        twitter_ot = twitter_ot.resample(timescale).mean()
        twitter_ot = twitter_ot.loc[gtrends.index]
        twitter_ot = (twitter_ot - twitter_ot.mean()) / twitter_ot.std()

        news = tvnews[tvnews['Query'] == w]['Value']
        if news.empty:
            news = pd.Series(0, index=pd.date_range(start_date, end_date))
        else:
            news = pd.concat([news, pd.Series(0, index=pd.date_range(start_date, end_date))]).fillna(0)

        news = news.replace(to_replace=0, method='ffill')
        news = news.apply(np.log).fillna(method='ffill').diff().dropna()
        news = news.resample(timescale).mean()
        news = news.loc[gtrends.index]
        news = (news - news.mean()) / news.std()

        ts = pd.DataFrame(dict(
            ngram=w,
            twitter_at=twitter_at,
            twitter_ot=twitter_ot,
            gtrends=gtrends,
            news=news
        ))

        if df is not None:
            df = df.append(ts)
        else:
            df = ts

    df.to_csv(data_path / "words_timeseries.csv")


def get_monthly_gpr_data(data_path):

    geo = pd.read_excel(data_path / 'geopolitical_risk_index.xlsx').iloc[:425]
    geo.index = pd.to_datetime(geo.Date)
    gpr = geo.GPR
    return gpr


def sample(model, X, y, num_samples=2000):

    pyro.clear_param_store()
    kernel = pyro.infer.NUTS(model)
    mcmc = pyro.infer.MCMC(kernel, num_samples=2000, warmup_steps=500)
    mcmc.run(X, y)

    # some more summary statistics
    av_obj = arviz.from_pyro(posterior=mcmc)
    pred = pyro.infer.Predictive(model, mcmc.get_samples())
    mean_preds = pred(X, None)['y']
    r, p_r = stats.pearsonr(mean_preds.detach().mean(dim=0), y)
    print(f"Overall, Corr(mu, y) = {r}, p(r) = {p_r}")
    r_df = pd.DataFrame([[r, r**2.0, p_r]], columns=["r", "R^2", "p"])

    # return values for plotting
    samples = {k: v.detach() for k, v in mcmc.get_samples().items()}
    posterior = pyro.infer.Predictive(
        model,
        samples,
        num_samples=num_samples,
    )
    return samples, posterior, av_obj, r_df


def basic_linear_model(X, y):

    beta = pyro.sample(
        'beta',
        dist.Normal(0., 1.).expand((X.shape[-1] + 1,))
    )
    sigma = pyro.sample(
        'sigma',
        dist.LogNormal(0., 1.)
    )
    with pyro.plate('data', size=X.shape[0]) as n:
        mu = torch.matmul(X[n], beta[1:]) + beta[0]
        obs = pyro.sample(
            'y',
            dist.Normal(mu, sigma),
            obs=y[n] if y is not None else None
        )
    return mu


def gpr_model(datapath, start, end):

    gpr = get_monthly_gpr_data(datapath)
    gpr = gpr.apply(np.log).diff().dropna()
    gpr = gpr.loc[start:end]
    gpr = gpr.iloc[1:]

    print(
        f"GPR:\t\t[{datetime.strftime(gpr.index[0], '%Y-%m')} : "
        f"{datetime.strftime(gpr.index[-1], '%Y-%m')}]"
    )

    gpr_tensor = torch.tensor(gpr.values, dtype=torch.float)
    gpr_mu = gpr_tensor.mean(axis=0).unsqueeze(0)
    gpr_std = gpr_tensor.std(axis=0).unsqueeze(0)
    gpr_tensor = (gpr_tensor - gpr_mu) / gpr_std

    # lag one month behind the gpr data
    design_start_date = pd.to_datetime(gpr.index[0] - pd.offsets.Day(1))
    design_start_date = datetime.strftime(design_start_date, '%Y-%m')

    if end == '2020':
        # horrible hack to do things quickly --
        # we record data up to 2020-11-30, but gpr only to 2020-05-01
        # so cut panel at 2020-04-30
        design_end_date = "2020-04-30"
    else:
        design_end_date = pd.to_datetime(gpr.index[-1] - pd.offsets.Day(1))
        design_end_date = datetime.strftime(design_end_date, '%Y-%m')

    df = pd.read_csv(
        Path(datapath/"words_timeseries.csv"),
        header=0,
        index_col=0
    )
    df.index = pd.to_datetime(df.index)
    df = df.loc[design_start_date:design_end_date]

    print(
        f"Features:\t[{datetime.strftime(df.index[0], '%Y-%m')} : "
        f"{datetime.strftime(df.index[-1], '%Y-%m')}]"
    )

    outdir = datapath / f"{datetime.strftime(df.index[0], '%Y-%m')}_{datetime.strftime(gpr.index[-1], '%Y-%m')}"
    outdir.mkdir(exist_ok=True, parents=True)

    models = {
        'twitter_at': None,
        'twitter_ot': None,
        'gtrends': None,
        'news': None,
    }
    for m in models.keys():
        print(f"Model: {m}")

        data = df.pivot(values=m, columns='ngram')
        data = data[words_of_interest]
        social_unrest_design_tensor = torch.tensor(data.values, dtype=torch.float)

        gpr_samples, gpr_posterior, gpr_av_obj, corr_df = sample(
            basic_linear_model,
            social_unrest_design_tensor,
            gpr_tensor,
        )

        torch.save(
            social_unrest_design_tensor,
            outdir/f"social-unrest-design-tensor.pt"
        )
        torch.save(
            gpr_tensor,
            outdir/f"gpr-tensor.pt"
        )
        torch.save(
            gpr_posterior,
            outdir/f"{m}-posterior-predictive.pt"
        )

        gpr_av_obj.to_json(outdir/f"{m}-inference-data.json")
        corr_df.to_csv(outdir/f"{m}-correlation.csv")

        pct_s = torch.kthvalue(gpr_samples['beta'], int(gpr_samples['beta'].shape[0] * .1), dim=0)[0]
        pct_e = torch.kthvalue(gpr_samples['beta'], int(gpr_samples['beta'].shape[0] * .9), dim=0)[0]

        ngrams_vis.plot_risk_hists(
            f"{outdir}/gpr_{m}",
            data,
            gpr_samples,
            pct_s,
            pct_e,
        )
        models[m] = {w: gpr_samples['beta'][:, i].numpy() for i, w in enumerate(['constant']+words_of_interest)}
        models[m] = pd.DataFrame.from_dict(models[m], orient='columns')
        models[m]['model'] = m
        print('-'*100)

    df = pd.concat(list(models.values()), ignore_index=True)
    df.to_csv(outdir/"models.csv")


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='[CLI] GPR Case Study.')

    parser.add_argument(
        'datapath', default=Path.cwd(), type=Path,
        help='path to risk data (gpr + words timeseries)'
    )

    parser.add_argument(
        '--start_year', default='2009-12', type=str,
    )

    parser.add_argument(
        '--end_year', default='2020-01', type=str,
    )

    args = parser.parse_args()

    gpr_model(
        Path(args.datapath),
        start=args.start_year,
        end=args.end_year,
    )
