import json
from pathlib import Path

from setuptools import setup, find_packages
from setuptools.command.develop import develop
from setuptools.command.install import install


def config():
    repo = Path.cwd().parent / 'storywrangler'
    model = repo / "resources/lid.176.bin"
    eparser = repo / "resources/emojis.bin"
    nparser = repo / "resources/ngrams.bin"
    langs = repo / "resources/iso-codes.csv"
    codes = repo / "resources/language_codes.json"
    hashtbl = repo / "resources/language_hashtbl.json"

    paths = {
        "storywrangler": f"{repo}",
        "fasttext-version": "v0.9.2",
        "model_threshold": ".25",
        "emoji_parser": f"{eparser}",
        "ngrams_parser": f"{nparser}",
        "model": f"{model}",
        "supported_languages": f"{langs}",
        "language_hashtbl": f"{hashtbl}",
        "language_codes": f"{codes}",
    }

    with open(repo / 'config.json', 'w', encoding='utf-8') as file:
        json.dump(paths, file, ensure_ascii=False, indent=4)


class PostDevelopCommand(develop):
    """Post-installation for development mode."""
    def run(self):
        develop.run(self)
        if not Path(Path.cwd()/"config.json").exists():
            config()


class PostInstallCommand(install):
    """Post-installation for installation mode."""
    def run(self):
        install.run(self)
        if not Path(Path.cwd()/"config.json").exists():
            config()

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt") as f:
    libs = f.read().splitlines()


setup(
    name="storywrangler",
    version="1.0.0",
    description="The Storywrangler project is a curation of Twitter into day-scale usage ranks and frequencies "
                "of n-grams for over 150 billion tweets in 100+ languages starting from 2008.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords="NLP twitter ngrams",
    url="https://gitlab.com/compstorylab/storywrangler",
    author="Thayer Alshaabi",
    author_email="thayer.alshaabi@uvm.edu",
    packages=find_packages(),
    python_requires=">=3.6",
    install_requires=libs,
    license="MIT",
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Topic :: Text Processing :: Linguistic",
        "Operating System :: OS Independent",
    ],
    cmdclass={
        'develop': PostDevelopCommand,
        'install': PostInstallCommand,
    },
)

