# README for visualization code

For ease of reference, we will use 
`*_plot.py` to refer to scripts that provide command-line interfaces that users can run to get plots,
while `*_analytics.py` will include any analytical tools needed for a given figure 
and `*_vis.py` will have the visualization code to produce plots.  

## LID Evaluation 
- [`evaluate.py`](storywrangler/evaluate.py): Command-line interface 
- [`eval.py`](storywrangler/eval.py) Analytical scripts
- [`eval_vis.py`](storywrangler/eval_vis.py) Visualization scripts 


## Languages
- [`languages_plot.py`](storywrangler/languages_plot.py): Command-line interface 
- [`languages_analytics.py`](storywrangler/languages_analytics.py) Analytical scripts
- [`languages_vis.py`](storywrangler/languages_vis.py) Visualization scripts 


## $n$-grams
- [`ngrams_plot.py`](storywrangler/ngrams_plot.py): Command-line interface 
- [`ngrams_analytics.py`](storywrangler/ngrams_analytics.py) Analytical scripts
- [`ngrams_vis.py`](storywrangler/ngrams_vis.py) Visualization scripts 


## Contagion Ratio/Gain
- [`gain_plot.py`](storywrangler/gain_plot.py): Command-line interface 
- [`gain_analytics.py`](storywrangler/gain_analytics.py) Analytical scripts
- [`gain_vis.py`](storywrangler/gain_vis.py) Visualization scripts 


## Case Studies
- [`ngrams_studies.py`](storywrangler/ngrams_studies.py)
- [`ngrams_movies.py`](storywrangler/ngrams_movies.py)
- [`ngrams_risk.py`](storywrangler/ngrams_risk.py)
- [`ngrams_risk_comparison.py`](storywrangler/ngrams_risk_comparison.py)