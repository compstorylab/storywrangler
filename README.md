![storywrangler](resources/storywrangler.png)

- [Introduction](#introduction)
- [Usage](#usage)
  - [UVM VPN](#uvm-vpn)
  - [Installation](#installation)
  - [Basic Functionality](#basic-functionality)
  - [Command-line Interface](#command-line-interface)
  - [Processing Pipeline](#processing-pipeline)
- [Citation](#citation)
- [Resources](#resources)
- [Supported Languages](#supported-languages)


## Introduction

In real-time, 
social media data strongly imprints world events, popular culture, 
and day-to-day conversations by millions of ordinary people 
at a scale that is scarcely conventionalized and recorded. 
Vitally, 
and absent from many standard corpora such as books and news archives, 
sharing and commenting mechanisms are native to social media platforms, 
enabling us to quantify social amplification (i.e., popularity) 
of trending storylines and contemporary cultural phenomena. 

Here, 
we describe Storywrangler, 
a natural language processing instrument designed to carry out an ongoing, 
day-scale curation of over 100 billion tweets containing 
roughly 1 trillion 1-grams from 2008 to 2021. 
For each day, we break tweets into 
unigrams, bigrams, and trigrams spanning over 100 languages.
We track n-gram usage frequencies, and generate Zipf distributions, 
for words, hashtags, handles, numerals, symbols, and emojis. 

We make the data set available through an interactive time series viewer, 
and as downloadable time series and daily distributions. 
Although Storywrangler leverages Twitter data, 
our method of extracting and tracking dynamic changes of n-grams 
can be extended to any similar social media platform. 


We make the data set available through an interactive
time series [viewer](https://storywrangling.org),
and as downloadable time series and daily distributions using 
our [API](https://github.com/janeadams/storywrangler).
Although Storywrangler leverages Twitter data, 
our method of extracting and tracking dynamic changes of n-grams 
can be extended to any similar social media platform. 

We showcase a few examples of the many possible avenues of study
we aim to enable including
how social amplification can be visualized through 
[contagiograms](https://gitlab.com/compstorylab/contagiograms).
We also present some example case studies that bridge n-gram time series 
with disparate data sources to explore sociotechnical dynamics of 
famous individuals, box office success, and social unrest. 



## Usage

This repository is primarily designed to power the backend of our 
[Storywrangler](https://storywrangling.org/) project 
working with raw tweets using the 
[Decahose stream](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/sample-realtime/overview/decahose) from Twitter. 

If you would like to use our n-grams dataset in your research, 
we provide an easy way to download daily n-gram timeseries as JSON
files via the [API](https://github.com/janeadams/storywrangler).

> If there is a large subset of n-grams you would like from our database, please send us an email.


### UVM VPN

Our computations were performed on 
the Vermont Advanced Computing Core ([VACC](https://www.uvm.edu/vacc)) 
supported in part by NSF award No. OAC-1827314. 
You can connect to the UVM VPN at 
`sslvpn2.uvm.edu` using your UVM credentials 
and access our database using this
[Python API](https://gitlab.com/compstorylab/storywrangling).



### Installation

You can install the latest version by cloning the repo 
and running the [setup.py](setup.py) script in your terminal:

```bash
git clone https://gitlab.com/compstorylab/storywrangler.git
cd storywrangler
python setup.py develop
```


### Basic Functionality

To use our framework in your project,
you can simply import any specific function 
from Storywrangler 

```python
from storywrangler import get_emojis_parser
from storywrangler import get_ngrams_parser

from storywrangler import Storyon
```


### Command-line Interface

You can run [predict.py](storywrangler/predict.py) script to get predictions for some short text.

```shell
usage: predict.py [-h] [-n NGRAMS] [-s SCORE] [-m MODEL] [-e] {text} ...

positional arguments:
  {text, doc}           Arguments for specific data types.
    text                run ngrams parser and fastText LID on short text
    doc                 run ngrams parser and fastText LID on .txt document

optional arguments:
  -e, --emoji           download new codes for emojis from (https://www.unicode.org/) 
                        and nre-compile regex to parse out ngrams (default: False)
  -h, --help            show this help message and exit
  -m MODEL, --model MODEL
                        absolute Path to FastText pre-trained model 
                        (default: ../storywrangler/opt/tlid/resources/lid.176.bin)
  -n NGRAMS, --ngrams NGRAMS
                        n-grams scheme to use (default: 1)
  -s SCORE, --score SCORE
                        confidence score threshold for the language identifier
                        (default: 0.25)
```

You can also run [lid.py](storywrangler/lid.py) script to select tweets for a given language 
over a single day (Decahose stream format).

```shell
usage: lid.py [-h] [-l LANG] [-m MODEL] [-o OUTDIR] [-e] [--score SCORE] datapth

positional arguments:
  datapth               path to a [day] directory of compressed 15-minutes
                        twitter files (ie. /tmp/2019-01-01)

optional arguments:
  --anchor ANCHOR       path to a json file of anchor ngrams to subsample
                        tweets (default: None)
  --score SCORE         confidence score threshold for the language identifier
                        (default: 0.25)
  -e, --emoji           download new codes for emojis from
                        (https://www.unicode.org/) and re-compile regex to
                        parse out ngrams (default: False)
  -h, --help            show this help message and exit
  -l LANG, --lang LANG  desired language for tweet selection (default: en)
  -m MODEL, --model MODEL
                        absolute Path to FastText pre-trained model 
                        (default: ../storywrangler/opt/tlid/resources/lid.176.bin)
  -o OUTDIR, --outdir OUTDIR
                        absolute Path to save network predictions 
                        (default: ../storywrangler/languages)
```


### Processing Pipeline

For processing tweets through the Decahose stream,
you can use [parser.py](storywrangler/parser.py) to run 
both classification and aggregation at once for a given day of tweets 
([Decahose stream](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/sample-realtime/overview/decahose) format required).

```shell
usage: parser.py [-h] [-n NGRAMS] [-x CODES] [-i IGNORE] [-m MODEL]
                 [-c CLASSIFIER] [-o OUTDIR] [-p] [-l] [-e] [--score SCORE]
                 [--anchor ANCHOR]
                 datapth

positional arguments:
  datapth               path to a [day] directory of compressed 15-minutes
                        twitter files (ie. /tmp/2019-01-01)

optional arguments:
  --anchor ANCHOR       path to a json file of anchor ngrams to subsample tweets 
                        (default: None)
  --score SCORE         confidence score threshold for the language identifier
                        (default: 0.25)
  -c CLASSIFIER, --classifier CLASSIFIER
                        label to use when parsing out n-grams 
                        (ft: FastText, tw: Twitter) (default: ft)
  -e, --emoji           download new codes for emojis from
                        (https://www.unicode.org/) 
                        and re-compile regex to parse out ngrams 
                        (default: False)
  -h, --help            show this help message and exit
  -i IGNORE, --ignore IGNORE
                        a list of languages to ignore (n-grams collection only) 
                        (default: ja th zh zh-cn zh-tw yue wuu jv su km my lo bo)
  -l, --languages       collect language statistics and save to `_languages.csv.gz` 
                        (default: False)
  -m MODEL, --model MODEL
                        absolute Path to FastText pre-trained model 
                        (default: ../storywrangler/opt/tlid/resources/lid.176.bin)
  -n NGRAMS, --ngrams NGRAMS
                        n-grams scheme to use (default: 1)
  -o OUTDIR, --outdir OUTDIR
                        absolute Path to save network predictions 
                        (default: ../storywrangler/ngrams)
  -p, --predictions     keep track of network predictions and save to `_predictions.csv.gz` 
                        (default: False)
  -x CODES, --codes CODES
                        path to (.json) language hashtable 
                        (default: ../storywrangler/opt/tlid/resources/language_codes.json)
```

Alternatively, you may use [collect.py](storywrangler/collect.py) to run 
both of our language classifier and ngrams-parser on a given day of tweets 
(compressed JSON batches) for `15-minute resolution` n-grams.

```shell
usage: collect.py [-h] [-o OUTDIR] [-n NGRAMS] [--codes CODES] [--langs LANGS]
                  [--model MODEL] [--predictions] [--emoji] [--score SCORE]
                  [--anchor ANCHOR] [--case_insensitive CASE_INSENSITIVE]
                  datapth

positional arguments:
  datapth               path to a [day] directory of compressed 15-minutes
                        twitter files (ie. /tmp/2019-01-01)

optional arguments:
  --anchor ANCHOR       path to a json file of anchor ngrams to subsample
                        tweets (default: None)
  --case_sensitive CASE_SENSITIVE
                        a toggle to get case sensitive ngrams 
                        (default: False)
  --codes CODES         path to (.json) language hashtable 
                        (default: ../storywrangler/opt/tlid/resources/language_codes.json)
  --emoji               download new codes for emojis from
                        (https://www.unicode.org/) 
                        and re-compile regex to parse out ngrams 
                        (default: False)
  --langs LANGS         a list of languages to parse out 
                        (default: en es pt ar ko fr id tr hi de it ru)
  --model MODEL         absolute Path to FastText pre-trained model 
                        (default: ../storywrangler/opt/tlid/resources/lid.176.bin)
  --predictions         keep track of network predictions and save to `_predictions.csv.gz` 
                        (default: False)
  --score SCORE         confidence score threshold for the language identifier
                        (default: 0.25)
  -h, --help            show this help message and exit
  -n NGRAMS, --ngrams NGRAMS
                        n-grams scheme to use 
                        (default: 1)
  -o OUTDIR, --outdir OUTDIR
                        absolute Path to save network predictions 
                        (default: ../storywrangler/ngrams)

```

To subsample tweets that contains a given set of anchor n-grams, 
you can create a simple JSON file (`anchors.json`) as shown below. 
The parser will automatically filter out tweets that do not match the given criteria.
```json
{
  "case_insensitive": "True",
  "pattern": "None",
  "partial": "False",  
  "gate": "AND",
  "anchors": [
    "pandemic",
    "unemployment"
  ]
}
```

| Flags              | Description                                            |
| ------------------ | ------------------------------------------------------ |
| `case_insensitive` | To ignore case sensitivity                             |
| `pattern`          | Custom regex pattern to use                            |
| `partial`          | Match both (pandemic and pandemics)                    |
| `gate`             | To match either or all requested anchors ('OR', 'AND') |
| `anchors`          | List of n-grams                                        |


You may also use [aggregate.py](storywrangler/aggregate.py) to aggregate
n-grams at a given time resolution.
```shell
usage: aggregate.py [-h] [-o OUTDIR] [-t TIME] indir

positional arguments:
  indir                 path to daily compressed tarballs (see `parser.py -h`)

optional arguments:
  -h, --help            show this help message and exit
  -o OUTDIR, --outdir OUTDIR
                        absolute path to save aggregated ngrams 
                        (default: ../storywrangler/ngrams)
  -t TIME, --time TIME  a time-scale to aggregate on (eg, 'W', 'M', 'Y')
                        (default: W)

```


> **Disclaimer**:
> Our current parser does **NOT** support continuous-script based languages
> (e.g., Japanese (ja), Chinese (zh), Thai (th), etc.



## Citation

See the following paper for more details, 
and please cite it if you use
our dataset:

> Alshaabi, T., Adams, J. L., Arnold, M. V., Minot, J. R., Dewhurst, D. R., Reagan, A. J., Danforth, C. M., & Dodds, P. S. (2020). 
> [Storywrangler: A massive exploratorium for sociolinguistic, cultural, socioeconomic, and political timelines using Twitter](https://arxiv.org/abs/2007).
> *arXiv preprint arXiv:2007.12988*.


For more information regarding 
our tweet's language identification and detection framework,
please see the following paper: 

> Alshaabi, T., Dewhurst, D. R., Minot, J. R., Arnold, M. V., Adams, J. L., Danforth, C. M., & Dodds, P. S. (2020). 
> [The growing amplification of social media: Measuring temporal and social contagion dynamics for over 150 languages on Twitter for 2009--2020](https://arxiv.org/abs/2003.03667).
> *arXiv preprint arXiv:2003.03667*.

## Resources

- [`lid.176.bin`](resources/lid.176.bin) 
FastText language identification model v0.9.2.
- [`emojis.bin`](resources/emojis.bin) 
A compiled regular expression that matches emojis.
- [`ngrams.bin`](resources/ngrams.bin) 
A compiled regular expression that matches ngrams in tweets.
- [`language_hashtbl.json`](resources/language_hashtbl.json) 
A hash-table to match Twitter's language labels to FastText.
- [`language_codes.json`](resources/language_codes.json) 
A hash-table of numeric codes that matches all languages found in the Decahose.

## Supported Languages

A list of all supported languages can be in [`iso-codes.csv`](resources/iso-codes.csv).

`Language`          |`FastText`|`Twitter`|`Language`           |`FastText`|`Twitter` 
:------------------ |:--------:|:-------:|:------------------- |:--------:|:-------: 
Afrikaans           | af       | -       | Lombard             | lmo      | -      |
Albanian            | sq       | -       | Lower-Sorbian       | dsb      | -      | 
Amharic             | am       | am      | Luxembourgish       | lb       | -      |
Arabic              | ar       | ar      | Macedonian          | mk       | -      |
Aragonese           | an       | -       | Maithili            | mai      | -      |
Armenian            | hy       | hy      | Malagasy            | mg       | -      |
Assamese            | as       | -       | Malayalam           | ml       | ml     |
Asturian            | ast      | -       | Malay               | ms       | msa    | 
Avaric              | av       | -       | Maltese             | mt       | -      |
Azerbaijani         | az       | -       | Manx                | gv       | -      |
Bashkir             | ba       | -       | Marathi             | mr       | mr     |
Basque              | eu       | eu      | Mazanderani         | mzn      | -      |
Bavarian            | bar      | -       | Minangkabau         | min      | -      | 
Belarusian          | be       | -       | Mingrelian          | xmf      | -      |
Bengali             | bn       | bn      | Mirandese           | mwl      | -      |
Bihari              | bh       | -       | Mongolian           | mn       | -      |
Bishnupriya         | bpy      | -       | Nahuatl             | nah      | -      |
Bosnian             | bs       | -       | Neapolitan          | nap      | -      | 
Breton              | br       | -       | Nepali              | ne       | ne     |
Bulgarian           | bg       | bg      | Newari              | new      | -      |
Burmese             | my       | my      | Northern-Frisian    | frr      | -      |
Catalan             | ca       | ca      | Northern-Luri       | lrc      | -      |
Cebuano             | ceb      | -       | Norwegian           | no       | no     |
Cherokee            | -        | chr     | Inuktitut           | -        | iu     |
Central-Bikol       | bcl      | -       | Nynorsk             | nn       | -      | 
Central-Kurdish     | ckb      | ckb     | Occitan             | oc       | -      |
Chavacano           | cbk      | -       | Oriya               | or       | or     | 
Chechen             | ce       | -       | Ossetic             | os       | -      |
Chinese-Simplified  | -        | zh-cn   | Pampanga            | pam      | -      |
Chinese-Traditional | -        | zh-tw   | Panjabi             | pa       | pa     | 
Chinese             | zh       | zh      | Persian             | fa       | fa     |
Chuvash             | cv       | -       | Pfaelzisch          | pfl      | -      |
Cornish             | kw       | -       | Piemontese          | pms      | -      |
Corsican            | co       | -       | Polish              | pl       | pl     |
Croatian            | hr       | -       | Portuguese          | pt       | pt     | 
Czech               | cs       | cs      | Pushto              | ps       | ps     |
Danish              | da       | da      | Quechua             | qu       | -      |
Dimli               | diq      | -       | Raeto-Romance       | rm       | -      |
Divehi              | dv       | dv      | Romanian            | ro       | ro     |
Dotyali             | dty      | -       | Russia-Buriat       | bxr      | -      | 
Dutch               | nl       | nl      | Russian             | ru       | ru     |
Egyptian-Arabic     | arz      | -       | Rusyn               | rue      | -      | 
Emiliano-Romagnolo  | eml      | -       | Sanskrit            | sa       | -      |
English             | en       | en      | Sardinian           | sc       | -      | 
Erzya               | myv      | -       | Saxon               | nds      | -      |
Esperanto           | eo       | -       | Scots               | sco      | -      | 
Estonian            | et       | et      | Serbian             | sr       | sr     |
Fiji-Hindi          | hif      | -       | Serbo-Croatian      | sh       | -      |
Filipino            | -        | fil     | Sicilian            | scn      | -      |
Finnish             | fi       | fi      | Sindhi              | sd       | sd     | 
French              | fr       | fr      | Sinhala             | si       | si     | 
Frisian             | fy       | -       | Slovak              | sk       | -      |
Gaelic              | gd       | -       | Slovenian           | sl       | sl     | 
Gallegan            | gl       | -       | Somali              | so       | -      | 
Georgian            | ka       | ka      | South-Azerbaijani   | azb      | -      |
German              | de       | de      | Spanish             | es       | es     | 
Goan-Konkani        | gom      | -       | Sundanese           | su       | -      | 
Greek               | el       | el      | Swahili             | sw       | -      |
Guarani             | gn       | -       | Swedish             | sv       | sv     |
Gujarati            | gu       | gu      | Tagalog             | tl       | tl     |
Haitian             | ht       | ht      | Tajik               | tg       | -      | 
Hebrew              | he       | he/iw   | Tamil               | ta       | ta     |
Hindi               | hi       | hi      | Tatar               | tt       | -      | 
Hungarian           | hu       | hu      | Telugu              | te       | te     | 
Icelandic           | is       | is      | Thai                | th       | th     |
Ido                 | io       | -       | Tibetan             | bo       | bo     | 
Iloko               | ilo      | -       | Tosk-Albanian       | als      | -      |
Indonesian          | id       | id/in   | Turkish             | tr       | tr     | 
Interlingua         | ia       | -       | Turkmen             | tk       | -      | 
Interlingue         | ie       | -       | Tuvinian            | tyv      | -      | 
Irish               | ga       | -       | Uighur              | ug       | ug     |
Italian             | it       | it      | Ukrainian           | uk       | uk     | 
Japanese            | ja       | ja      | Upper-Sorbian       | hsb      | -      | 
Javanese            | jv       | -       | Urdu                | ur       | ur     |
Kalmyk              | xal      | -       | Uzbek               | uz       | -      | 
Kannada             | kn       | kn      | Venetian            | vec      | -      | 
Karachay-Balkar     | krc      | -       | Veps                | vep      | -      |
Kazakh              | kk       | -       | Vietnamese          | vi       | vi     | 
Khmer               | km       | km      | Vlaams              | vls      | -      | 
Kirghiz             | ky       | -       | Volapük             | vo       | -      |
Komi                | kv       | -       | Walloon             | wa       | -      | 
Korean              | ko       | ko      | Waray               | war      | -      | 
Kurdish             | ku       | -       | Welsh               | cy       | cy     | 
Lao                 | lo       | lo      | Western-Mari        | mrj      | -      |
Latin               | la       | -       | Western-Panjabi     | pnb      | -      | 
Latvian             | lv       | lv      | Wu-Chinese          | wuu      | -      |
Lezghian            | lez      | -       | Yakut               | sah      | -      | 
Limburgan           | li       | -       | Yiddish             | yi       | -      | 
Lithuanian          | lt       | lt      | Yoruba              | yo       | -      | 
Lojban              | jbo      | -       | Yue-Chinese         | yue      | -      | 
