import json
from pathlib import Path

cwd = Path.cwd().parent
main = cwd / "storywrangler"
tmp = cwd / "tmp"
main.replace(tmp)
main.mkdir(exist_ok=True, parents=True)

opt = main / "opt"
opt.mkdir(exist_ok=True, parents=True)

ngrams = main / "ngrams"
logs = main / "logs"
opt.mkdir(exist_ok=True, parents=True)
logs.mkdir(exist_ok=True, parents=True)

realtime = main / "realtime"
realtime_logs = main / "realtime_logs"
realtime.mkdir(exist_ok=True, parents=True)
realtime_logs.mkdir(exist_ok=True, parents=True)

languages = main / "languages"
languages.mkdir(exist_ok=True, parents=True)

plots = main / "plots"
plots.mkdir(exist_ok=True, parents=True)

tlid = opt / "tlid"
tmp.replace(tlid)

utils = opt / "tweet_utils"

model = tlid / "resources/lid.176.bin"
eparser = tlid / "resources/emojis.bin"
nparser = tlid / "resources/ngrams.bin"
langs = tlid / "resources/iso-codes.csv"
codes = tlid / "resources/language_codes.json"
hashtbl = tlid / "resources/language_hashtbl.json"

config = {
    "twitterlid": f"{tlid}",
    "tweet_utils": f"{utils}",
    "fasttext-version": "v0.9.2",
    "emoji_parser": f"{eparser}",
    "ngrams_parser": f"{nparser}",
    "model": f"{model}",
    "supported_languages": f"{langs}",
    "language_hashtbl": f"{hashtbl}",
    "language_codes": f"{codes}",
    "model_threshold": ".25",
    "ngrams": f"{ngrams}",
    "logs": f"{logs}",
    "realtime": f"{realtime}",
    "realtime_logs": f"{realtime_logs}",
    "languages": f"{languages}",
    "plots": f"{plots}",

}

with open(tlid / 'config.json', 'w', encoding='utf-8') as file:
    json.dump(config, file, ensure_ascii=False, indent=4)
